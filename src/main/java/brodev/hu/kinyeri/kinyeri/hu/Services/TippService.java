package brodev.hu.kinyeri.kinyeri.hu.Services;

import brodev.hu.kinyeri.kinyeri.hu.Dto.TippDto;
import brodev.hu.kinyeri.kinyeri.hu.Mapper.TippMapper;
import brodev.hu.kinyeri.kinyeri.hu.Models.*;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.MatchResult;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.MatchRepository;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.TippRepository;
import lombok.AllArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TippService {


    private final SimpMessagingTemplate brokerMessagingTemplate;

    private final TippRepository tippRepository;

    private final MatchRepository matchRepository;

    private final UserService userService;

    private final TippMapper tippMapper;

    private final MatchTippStatisticsService matchTippStatisticsService;

    public DataTablesOutput<Tipp> findAll(DataTablesInput input) {

        return tippRepository.findAll(input);
    }
    public DataTablesOutput<Tipp> findAll(DataTablesInput input, String username) {
        User user = userService.FindByUserName(username);

        Specification<Tipp> additionalSpecification = new Specification<Tipp>() {
            @Override
            public Predicate toPredicate(Root<Tipp> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                return cb.and(  cb.like(root.get("tippId").get("user").get("username"), user.getUsername()));
            }
        };
        return tippRepository.findAll(input,additionalSpecification);
    }

    public DataTablesOutput<Tipp> findAllUserTips(DataTablesInput input, Principal principal) {
        User user = userService.FindByUserName(principal.getName());

        Specification<Tipp> additionalSpecification = new Specification<Tipp>() {
            @Override
            public Predicate toPredicate(Root<Tipp> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                return cb.and( cb.like(root.get("tippId").get("user").get("username"), principal.getName()));
            }
        };
        return tippRepository.findAll(input,additionalSpecification);
    }


    public Tipp get(TippId id) {
        return tippRepository.getOne(id);
    }

    public MatchResult getResultById(TippId id) {
        return tippRepository.findById(id).map(tipp -> tipp.getResultTip()).orElse(null);
    }

    public JsonResponse delete(TippId id) {
        try {
            tippRepository.deleteById(id);
            return new JsonResponse("Sikeres törlés", "success");
        } catch (DataIntegrityViolationException cve){
            return new JsonResponse("Ez a Tipp használatban van így nem törölhető!","error");
        }catch(EmptyResultDataAccessException e){
            return new JsonResponse("Ez a Tipp már törölve van!","error");
        }catch (Exception e) {
            return new JsonResponse("Sikertelen törlés" + e.getMessage(), "error");
        }
    }

    public Tipp findOneById(TippId id) {
        Optional<Tipp> tipp=tippRepository.findById(id);
        if (tipp.isPresent()){
            return tipp.get();
        }
        return null;

    }

    public List<Tipp> getAll() {
        return tippRepository.findAll();
    }


    public List<Tipp> getMatchTipps(Long id) {
        return tippRepository.getAllByTippIdMatchIdId(id);
    }

    public void save(Tipp tipp) {
        try{
            tippRepository.save(tipp);
        }catch(Exception e ){
            System.out.println("Hiba történt tipp kiértelésekor:" + tipp.getTippId());
        }
    }

    public JsonResponse saveTippDto(TippDto tippDto, Principal principal) {
        try{
            Match match;
            Tipp tipp=new Tipp();
            TippId tippId=new TippId();
            tippId.setUser(userService.FindByUserName(principal.getName()));
            Optional<Match> matchOptional=matchRepository.findById(tippDto.getMatchId());
            if (matchOptional.isPresent()){
                match=matchOptional.get();
                if (!match.getMatchTime().isAfter(LocalDateTime.now())){
                    return new JsonResponse("A mérkőzés már elkezdödött!", "error");
                }
                if(match.getResult() != null || match.getHomeGoalQty() != null || match.getVisitorGoalQty() != null){
                    return new JsonResponse("A mérkőzést már lezárták nem adhatsz le tippet!", "error");
                }
            }else{
                return new JsonResponse("A mérkőzés nem létezik!", "error");
            }


            tippId.setMatchId(match);
            tipp.setTippId(tippId);
            tipp=tippMapper.toTipp(tippDto,tipp);
            if(tipp.getHomeGoalQtyTipp() > tipp.getVisitorGoalQtyTipp()){
                if(!tipp.getResultTip().equals(MatchResult.H)){
                    return new JsonResponse("A tipp leadása sikertelen volt, mivel ellentmondásos értékeket adtál meg!", "error");
                }
            }
            if(tipp.getVisitorGoalQtyTipp() > tipp.getHomeGoalQtyTipp()){
                if(!tipp.getResultTip().equals(MatchResult.V)){
                    return new JsonResponse("A tipp leadása sikertelen volt, mivel ellentmondásos értékeket adtál meg!", "error");
                }
            }
            if(tipp.getVisitorGoalQtyTipp().equals(tipp.getHomeGoalQtyTipp())){
                if(!tipp.getResultTip().equals(MatchResult.D)){
                    return new JsonResponse("A tipp leadása sikertelen volt, mivel ellentmondásos értékeket adtál meg!", "error");
                }
            }
            User user = tippId.getUser();
            if(!tippRepository.existsById(tippId)) {
                if (user.getTipNum() != null) {
                    user.setTipNum(user.getTipNum() + 4);
                } else {
                    user.setTipNum(4);
                }
            }
            if(user.getLastTip()!=null){
                user.setLastTip(match.getMatchTime().isAfter(user.getLastTip())?match.getMatchTime():user.getLastTip());
            }else{
                user.setLastTip(match.getMatchTime());
            }
            userService.save(user);
            tippRepository.save(tipp);
            brokerMessagingTemplate.convertAndSend("/topic/live-statistics",matchTippStatisticsService.calculateHVDStatistic(match, null,tippDto.getMatchId()));
            return new JsonResponse("Sikeres tipp leadás!", "success");
        }catch (Exception e){

            return new JsonResponse("Hiba a tipp leadásnál!", "error");
        }
    }

    public DataTablesOutput<Tipp> findAllWithoutVip(DataTablesInput input, String username) {
        DataTablesOutput<Tipp> dataTablesOutput=findAll(input, username);
        List<Tipp> tippList=dataTablesOutput.getData();
        dataTablesOutput.setData(tippList.stream()
                .map(tipp -> {
                    if(tipp.getFinalPoints()==null) {
                        tipp.setVisitorGoalQtyTipp(null);
                        tipp.setHomeGoalQtyTipp(null);
                        tipp.setResultTip(null);
                        tipp.setGoalQty(null);
                    }
                    return tipp;
                })
                .collect(Collectors.toList())
        );
        return dataTablesOutput;
    }

    public List<Tipp> getUserTips(User user) {
        return tippRepository.getAllByTippIdUserAndFinalPointsIsNotNull(user);
    }
    public long countUserTipps(User user) {
        return tippRepository.countAllByTippIdUserAndFinalPointsIsNotNull(user);
    }

    public long countUserTippsForCurrentMonth(User user) {
        return tippRepository.countAllByTippIdUserAndTippIdMatchIdMatchTimeGreaterThan(user,LocalDateTime.now().withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0));
    }

    public long countUserTippDistinctDays(User user) {
        return tippRepository.countDistinctTippIdMatchIdMatchTime_DayOfMonthByTippIdUserAndTippIdMatchIdMatchTimeGreaterThan(user,LocalDateTime.now().withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0));
    }

    public List<Tipp> getCurrentMonthTipps(User user) {
        return tippRepository.findAllByTippIdUserAndTippIdMatchIdMatchTimeGreaterThan(user,LocalDateTime.now().withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0));
    }

    public List<Tipp> findAllTippByUser(User user)
    {
        return tippRepository.findAllByTippId_UserAndFinalPointsIsNotNull(user);
    }
}
