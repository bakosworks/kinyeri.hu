package brodev.hu.kinyeri.kinyeri.hu.Repositories;

import brodev.hu.kinyeri.kinyeri.hu.Models.AllTimeTopStatistic;
import org.springframework.stereotype.Repository;

@Repository
public interface AllTimeTopStatisticRepository extends BaseTopStatisticsRepository<AllTimeTopStatistic>{
}
