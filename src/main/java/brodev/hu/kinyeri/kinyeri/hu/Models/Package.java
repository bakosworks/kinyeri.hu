package brodev.hu.kinyeri.kinyeri.hu.Models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "packages")
public class Package {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private double price;

    @Column(name = "details")
    private String details;

    @Column(name = "interval_in_days")
    private Integer interval;

    @Column(name = "active")
    private boolean active;

    @Column(name = "color")
    private String color;

    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm" , timezone = "Europe/Budapest")
    @Column(name = "active_till")
    private LocalDateTime activeTill;
}
