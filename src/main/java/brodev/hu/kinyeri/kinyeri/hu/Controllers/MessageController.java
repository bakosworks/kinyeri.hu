
package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Dto.MessageDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.Comment;
import brodev.hu.kinyeri.kinyeri.hu.Models.Message;
import brodev.hu.kinyeri.kinyeri.hu.Services.CommentService;
import brodev.hu.kinyeri.kinyeri.hu.Services.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;

@Controller
@AllArgsConstructor
public class MessageController {
    private SimpMessagingTemplate brokerMessagingTemplate;

    private MessageService messageService;

    private CommentService commentService;

    @SendTo("/topic/live-chat")
    @MessageMapping("/send-message")
    public Message send(MessageDto message, Authentication authentication) {
        return messageService.saveMessage(message, authentication);
      //  brokerMessagingTemplate.convertAndSend("/topic/live-chat",message);
    }
    @SendTo("/topic/comment/{matchId}")
    @MessageMapping("/comment/{matchId}")
    public Comment connectToCommentSection(@DestinationVariable(value = "matchId") String matchId, Comment comment, Authentication authentication) {
        return commentService.newComment(comment,authentication,matchId);
      //  brokerMessagingTemplate.convertAndSend("/topic/live-chat",message);
    }

}

