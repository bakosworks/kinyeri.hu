package brodev.hu.kinyeri.kinyeri.hu.Services;

import brodev.hu.kinyeri.kinyeri.hu.Models.BankDetail;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.BankDetailRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class BankDetailService {

    private final  BankDetailRepository bankDetailRepository;

    public void save(BankDetail bankDetail) {
        bankDetailRepository.save(bankDetail);
    }

    public BankDetail getOne() {
        return bankDetailRepository.getOne(1l);
    }
}
