
package brodev.hu.kinyeri.kinyeri.hu.Models;

import brodev.hu.kinyeri.kinyeri.hu.Models.enums.PayType;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.PaymentStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "ref_payments")
@Data
public class RefPayment {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "req_user")
    private User reqUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_payed")
    private User userPayed;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_reverted")
    private User userReverted;

    @Column(name = "email")
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "pay_type")
    private PayType payType;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_status")
    private PaymentStatus paymentStatus;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "Europe/Budapest")
    @Column(name = "req_time")
    private LocalDateTime reqTime;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "Europe/Budapest")
    @Column(name = "finish_time")
    private LocalDateTime finishTime;

    @Column(name = "ref_balance")
    private Integer refBalance;
}
