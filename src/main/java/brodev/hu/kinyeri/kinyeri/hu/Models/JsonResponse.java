package brodev.hu.kinyeri.kinyeri.hu.Models;

import lombok.Data;

import java.util.Objects;

@Data
public class JsonResponse {

    private String msg;
    private String type;
    private Long id;
    private Object obj;


    public JsonResponse(String msg, String type, Long id) {
        this.msg = msg;
        this.type = type;
        this.id = id;
    }

    public JsonResponse(String msg, String type) {
        this.msg = msg;
        this.type = type;
    }

    public JsonResponse(String msg, String type, Object obj) {
        this.msg = msg;
        this.type = type;
        this.obj = obj;
    }
}