package brodev.hu.kinyeri.kinyeri.hu.Repositories;

import brodev.hu.kinyeri.kinyeri.hu.Models.Lottery;
import brodev.hu.kinyeri.kinyeri.hu.Models.UnibetOdds;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnibetOddsRepository extends JpaRepository<UnibetOdds,Long> {
}
