package brodev.hu.kinyeri.kinyeri.hu.Dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ReflinkDto {
    private Long id;
    private String text;
    private String link;
    private String streamReflink;
    private String matchReflink;

}
