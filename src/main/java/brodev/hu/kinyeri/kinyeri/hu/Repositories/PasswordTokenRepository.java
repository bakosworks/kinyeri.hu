package brodev.hu.kinyeri.kinyeri.hu.Repositories;

import brodev.hu.kinyeri.kinyeri.hu.Models.PasswordResetToken;
import brodev.hu.kinyeri.kinyeri.hu.Models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PasswordTokenRepository  extends JpaRepository<PasswordResetToken, Integer> {

    PasswordResetToken findByToken(String token);
}
