package brodev.hu.kinyeri.kinyeri.hu.Mapper;

import brodev.hu.kinyeri.kinyeri.hu.Dto.TippDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.Tipp;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.MatchResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class TippMapper {

    public Tipp toTipp(TippDto from,Tipp to){
        if (StringUtils.isNotEmpty(from.getRadioName())){
            switch (from.getRadioName()){
                case "d":
                    to.setResultTip(MatchResult.D);
                    break;
                case "h":
                    to.setResultTip(MatchResult.H);
                    break;
                case "v":
                    to.setResultTip(MatchResult.V);
                    break;
                default:break;
            }
        }
        to.setGoalQty(from.getVisitorQty()+from.getHomeQty());
        to.setHomeGoalQtyTipp(from.getHomeQty());
        to.setVisitorGoalQtyTipp(from.getVisitorQty());
        return to;
    }
}
