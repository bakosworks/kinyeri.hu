package brodev.hu.kinyeri.kinyeri.hu.Models;

import lombok.Data;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@MappedSuperclass
public class BaseTopStatistic {

    @Id
    @Column(name = "id")
    private Long id;
}
