package brodev.hu.kinyeri.kinyeri.hu.Services;

import brodev.hu.kinyeri.kinyeri.hu.Dto.MatchWithStatisticDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.Match;
import brodev.hu.kinyeri.kinyeri.hu.Services.Mail.MailContentBuilder;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@Configuration
@EnableAsync
@Service
public class MailService {

    @Autowired
    MailContentBuilder mailContentBuilder;

    @Autowired
    ReflinkService reflinkService;

    @Value("${support.mail.username}")
    String supportMailUsername;

    @Value("${support.mail.password}")
    String supportMailPassword;

    @Value("${server.name}")
    String serverName;



    public void sendForgotPw(String userName,String emailOwner,String tokenUrl) throws MessagingException {

        Properties props = new Properties();

        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "server1.kinyeri.hu");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(supportMailUsername, supportMailPassword);
            }
        });

        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress("Kinyeri.hu <"+supportMailUsername+">", false));

        String content = mailContentBuilder.sendForogtoPwHTML(userName,tokenUrl,serverName);
        String sendFor = emailOwner;
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(sendFor));
        msg.setSubject("Elfelejtett jelszó");
        msg.setContent(content, "text/html; charset=utf-8");
        msg.setSentDate(new Date());
        Transport.send(msg);
    }

    @Async
    public void sendTomorrowTips(List<Match> matches,List<String> recipients) throws MessagingException {

        Properties props = new Properties();

        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "server1.kinyeri.hu");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(supportMailUsername, supportMailPassword);
            }
        });

        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress("Kinyeri.hu <"+supportMailUsername+">", false));

        String content = mailContentBuilder.sendTomorrowTips(matches);

        for (String user : recipients) {
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(user));
            msg.setSubject("Holnapi mérkőzések");
            msg.setContent(content, "text/html; charset=utf-8");
            msg.setSentDate(new Date());
            Transport.send(msg);
        }
    }

    @Async
    public void sendMatchesWithStatistic(List<MatchWithStatisticDto> matches, List<String> recipients) throws MessagingException {

        Properties props = new Properties();

        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "server1.kinyeri.hu");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(supportMailUsername, supportMailPassword);
            }
        });

        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress("Kinyeri.hu <"+supportMailUsername+">", false));

        String content = mailContentBuilder.sendMatchWithStatistics(matches,reflinkService.findOne());

        LocalDateTime dateTime = LocalDateTime.now();

        for (String user : recipients)
        {
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(user));
            msg.setSubject("Mérkőzés értesítő - Hamarosan kezdődő mérkőzés - "+dateTime.toString("MM.dd. HH:mm"));
            msg.setContent(content, "text/html; charset=utf-8");
            msg.setSentDate(new Date());
            Transport.send(msg);
        }
    }
}
