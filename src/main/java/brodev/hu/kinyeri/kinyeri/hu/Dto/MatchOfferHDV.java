package brodev.hu.kinyeri.kinyeri.hu.Dto;

import lombok.Data;

@Data
public class MatchOfferHDV {
    private String home;
    private String draw;
    private String away;
    private String under1500;
    private String under2500;
    private String over1500;
    private String over2500;
    private String over3500;
    private String over4500;
    private String over5500;
    private String over6500;
    private String bothTeamHasGoalYes;
    private String bothTeamHasGoalNo;

}


