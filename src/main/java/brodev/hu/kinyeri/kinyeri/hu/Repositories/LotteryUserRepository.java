package brodev.hu.kinyeri.kinyeri.hu.Repositories;

import brodev.hu.kinyeri.kinyeri.hu.Models.Lottery;
import brodev.hu.kinyeri.kinyeri.hu.Models.LotteryUser;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LotteryUserRepository extends JpaRepository<LotteryUser,Long>, DataTablesRepository<LotteryUser,Long> {
}
