package brodev.hu.kinyeri.kinyeri.hu.Repositories;

import brodev.hu.kinyeri.kinyeri.hu.Models.Match;
import brodev.hu.kinyeri.kinyeri.hu.Models.Tipp;
import brodev.hu.kinyeri.kinyeri.hu.Models.TippId;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface TippRepository extends JpaRepository <Tipp, TippId>, DataTablesRepository<Tipp,TippId> {
  List<Tipp> getAllByTippIdMatchIdId(Long id);

    List<Tipp> getAllByTippIdUserAndFinalPointsIsNotNull(User user);

  long countAllByTippIdUserAndFinalPointsIsNotNull(User user);

  long countAllByTippIdUserAndTippIdMatchIdMatchTimeGreaterThan(User user, LocalDateTime localDateTime);

  List<Tipp> findAllByTippIdUserAndTippIdMatchIdMatchTimeGreaterThan(User user, LocalDateTime localDateTime);

  long countDistinctTippIdMatchIdMatchTime_DayOfMonthByTippIdUserAndTippIdMatchIdMatchTimeGreaterThan(User user,LocalDateTime localDateTime);

  List<Tipp> findAllByTippId_UserAndFinalPointsIsNotNull(User user);
}
