package brodev.hu.kinyeri.kinyeri.hu.Services;

import brodev.hu.kinyeri.kinyeri.hu.Dto.ChartDatasets;
import brodev.hu.kinyeri.kinyeri.hu.Dto.TopStatisticDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.BaseTopStatistic;
import brodev.hu.kinyeri.kinyeri.hu.Models.TempStatus;
import brodev.hu.kinyeri.kinyeri.hu.Models.Tipp;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.TopStatisticType;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.ActiveTopStatisticRepository;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.AllTimeTopStatisticRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class TopStatisticService {

    private final AllTimeTopStatisticRepository allTimeTopStatisticRepository;

    private final ActiveTopStatisticRepository activeTopStatisticRepository;

    private final UserService userService;

    private final TippService tippService;

    public List<TopStatisticDto> findAll(TopStatisticType topStatisticType) {

        List<User> users=userService.findTop10(topStatisticType);


        List<LocalDateTime> months=new ArrayList<>();
        int month= LocalDateTime.now().minusMonths(5).getMonthValue();
        int year= LocalDateTime.now().minusMonths(5).getYear();
        months.add(LocalDateTime.of(LocalDate.of(year,month,1),LocalTime.MIDNIGHT));
        while (months.size()<=5){
            if (month>11){
                month=0;
                year++;
            }
            month++;
            months.add(LocalDateTime.of(LocalDate.of(year,month,1),LocalTime.MIDNIGHT));
        }

        List<TopStatisticDto> statisticDtos=new ArrayList<>();

        for (User user:users) {
            Map<LocalDateTime,List<Tipp>> topStatistics1= user.getTippList().stream()
                    .map(tipp -> {
                        int tempYear=tipp.getTippId().getMatchId().getMatchTime().getYear();
                        int tempMonth=tipp.getTippId().getMatchId().getMatchTime().getMonthValue();
                        tipp.getTippId().getMatchId().setMatchTime(LocalDateTime.of(LocalDate.of(tempYear,tempMonth,1), LocalTime.MIDNIGHT));
                        return tipp;
                    })
                    .collect(Collectors.groupingBy(tipp -> tipp.getTippId().getMatchId().getMatchTime(),Collectors.toList()));
            int max=Arrays.asList(user.getWinFinalResultQty(),user.getWinNumberOfGolsQty(),user.getWinWichTeamGoesQty(),user.getWinWinningTeamQty()).stream()
                    .max(Integer::compareTo)
                    .get();
            List<String> best = new ArrayList<>();
            TempStatus s=new TempStatus();
            AtomicInteger pendingTipps=new AtomicInteger(0);



            List<Integer> chartDtos=months.stream()
                    .map(time -> {
                        List<Tipp> tipps=topStatistics1.get(time);
                        if (tipps!=null){
                            int sum=tipps.stream()
                                    .map(tipp -> {
                                        if (tipp.getFinalPoints()==null){
                                            pendingTipps.getAndIncrement();
                                            return null;
                                        }

                                        return tipp;
                                    })
                                    .filter(Objects::nonNull)
                                    .mapToInt(Tipp::getFinalPoints)
                                    .sum();

                            int some = tipps.size()-pendingTipps.get();

                            return some == 0 ? 0 :  (sum*100)/((some)*4);
                        }else {
                            return 0;
                        }
                    }).collect(Collectors.toList());



            long tipNum=  tippService.countUserTipps(user);

            if (max==user.getWinFinalResultQty()){
                best.add("Pontos végeredmény "+(user.getWinFinalResultQty()*100)/(tipNum)+"%");
                s.setS("Pontos végeredmény");
            } else if (max==user.getWinNumberOfGolsQty()){
                best.add("Gólszám "+(user.getWinNumberOfGolsQty()*100)/((tipNum))+"%");
                s.setS("Gólszám");
            } else if (max==user.getWinWichTeamGoesQty()){
                best.add("Mindkét csapat szerez gólt "+(user.getWinWichTeamGoesQty()*100)/((tipNum))+"%");
                s.setS("Mindkét csapat szerez gólt");
            } else if (max==user.getWinWinningTeamQty()){
                best.add("Nyertes csapat "+(user.getWinWinningTeamQty()*100)/((tipNum))+"%");
                s.setS("Nyertes csapat");
            }


            pendingTipps.set(0);

            List<Integer> bestChartDtos=months.stream()
                    .map(time -> {
                        List<Tipp> tipps=topStatistics1.get(time);
                        if (tipps!=null){
                            int bestSum=tipps.stream()
                                    .map(tipp -> {
                                        if (tipp.getFinalPoints()==null){
                                            pendingTipps.getAndIncrement();
                                            return null;
                                        }
                                        switch (s.getS()){
                                            case "Pontos végeredmény":
                                                return tipp.isWinFinalResult()?1:0;
                                            case "Gólszám":
                                                return tipp.isWinNumberOfGols()?1:0;
                                            case "Mindkét csapat szerez gólt":
                                                return tipp.isWinWichTeamGoes()?1:0;
                                            case "Nyertes csapat":
                                                return tipp.isWinWinningTeam()?1:0;
                                        }
                                        return 0;
                                    })
                                    .filter(Objects::nonNull)
                                    .mapToInt(Integer::intValue)
                                    .sum();

                            int some = tipps.size()-pendingTipps.get();

                            return some == 0 ? 0 : (bestSum*100)/(some);

                        }else {
                            return 0;
                        }
                    }).collect(Collectors.toList());


            user.setTippList(null);
            user.setPassword(null);
            user.setLastName(null);
            user.setFirstName(null);
            user.setAddress(null);
            user.setCity(null);
            user.setCountry(null);
            user.setEmail(null);
            user.setRoles(null);
            TopStatisticDto topStatisticDto=TopStatisticDto.builder()
                    .datasets(Arrays.asList(ChartDatasets.builder()
                            .data(chartDtos)
                            .label("Összesített rálátás")
                            .backgroundColor("#1e90ff")
                            .build(),ChartDatasets.builder()
                            .data(bestChartDtos)
                            .backgroundColor("#63c663")
                            .label(s.getS())
                            .build()))
                    .labels(months.stream()
                            .map(time -> time.getYear()+"."+time.getMonthValue())
                            .collect(Collectors.toList()))
                    .pending(pendingTipps.get())
                    .user(user)
                    .best(best)
                    .build();
            statisticDtos.add(topStatisticDto);
        }
        return statisticDtos ;
    }


}
