package brodev.hu.kinyeri.kinyeri.hu.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserModDto {
    private boolean mod;
    private Long userId;
}
