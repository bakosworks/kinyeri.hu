package brodev.hu.kinyeri.kinyeri.hu.Dto;

import brodev.hu.kinyeri.kinyeri.hu.Models.enums.BestSkill;
import lombok.Data;

import javax.persistence.Column;
import java.util.List;

@Data
public class UserDto {
    private Long id;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private Integer yearsOld;
    private Boolean defaultPassword;
    private Boolean active;
    private String password;
    private String password2;
    private List<RoleDto> roles;
    private int daysLeftVip;
    private String country;
    private String zipCode;
    private String city;
    private String address;
    private boolean emailNotification;
    private String skrillAddress;
    private String paypalAddress;

}
