package brodev.hu.kinyeri.kinyeri.hu.Models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "reflink")
public class Reflink {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reflink_id")
    private Long id;

    @Column(name ="text")
    private String text;

    @Column(name ="link")
    private String link;

    @Column(name ="stream_reflink")
    private String streamReflink;

    @Column(name = "match_reflink")
    private String matchReflink;

}

