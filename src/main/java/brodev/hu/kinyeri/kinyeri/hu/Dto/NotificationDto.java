package brodev.hu.kinyeri.kinyeri.hu.Dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class NotificationDto {
    private Long id;
    private LocalDateTime sendTime;
    private String title;
    private String message;
    private UserDto user;

}
