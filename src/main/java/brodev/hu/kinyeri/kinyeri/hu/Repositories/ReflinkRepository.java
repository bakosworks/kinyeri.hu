package brodev.hu.kinyeri.kinyeri.hu.Repositories;


import brodev.hu.kinyeri.kinyeri.hu.Models.Reflink;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface ReflinkRepository extends JpaRepository<Reflink, Long>  , DataTablesRepository<Reflink,Long > {

}
