package brodev.hu.kinyeri.kinyeri.hu.Services;

import brodev.hu.kinyeri.kinyeri.hu.Dto.PaymentDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Models.Package;
import brodev.hu.kinyeri.kinyeri.hu.Models.Payment;
import brodev.hu.kinyeri.kinyeri.hu.Models.Role;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.PaymentStatus;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.PaymentRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Service
@AllArgsConstructor
public class PaymentService {

    private final PaymentRepository paymentRepository;

    private final UserService userService;

    private final RoleService roleService;

    private final PackageService packageService;


    public DataTablesOutput<Payment> findAll(DataTablesInput input) {
        return paymentRepository.findAll(input);
    }

    public JsonResponse paid(Long id) {
        try{
            Payment payment = paymentRepository.getOne(id);
            User user = payment.getUserId();
            if(user.getDaysLeftVip() == null){
                user.setDaysLeftVip(0);
            }
            if(user.getDaysLeftVip() == 0){
                Role role = roleService.getVip();
                user.getRoles().add(role);
            }
            user.setDaysLeftVip(user.getDaysLeftVip()+ payment.getPackageId().getInterval());
            payment.setStatus(PaymentStatus.DONE);
            payment.setActivateTime(LocalDateTime.now());
            if (user.getRefUser()!=null) {
                Integer bonus = 0;
                if (payment.getDiscount() != null) {
                    double discount = (double) payment.getDiscount() / 100;
                    bonus = (int) ((payment.getPackageId().getPrice() * (1.0-discount)) / 2);
                } else {
                    bonus = (int) (payment.getPackageId().getPrice() / 2);
                }
                user.getRefUser().setBalance(user.getRefUser().getBalance() == null ? 0 : user.getRefUser().getBalance() + bonus);
                userService.save(user.getRefUser());
            }
            userService.save(user);
            paymentRepository.save(payment);
            return new JsonResponse("Sikeres csomag aktíválás! Felhasználó: "+user.getUsername() +" Csomag: "+ payment.getPackageId().getName(), "success");
        }catch(Exception e){
            System.out.println("Hiba történt befizetés véglegesítése közben: "+e.getMessage());
            return new JsonResponse("Hiba történt!", "error");
        }
    }

    public JsonResponse delete(Long id) {
        try{
            Payment payment = paymentRepository.getOne(id);
            paymentRepository.deleteById(id);
            return  new JsonResponse("Sikeressen törölve lett a rendelés! Felhasználó: "+payment.getUserId().getUsername() + "Csomag: "+ payment.getPackageId().getName(), "success");

        }catch(Exception e){
            System.out.println("Hiba történt törlés közben:" +e.getMessage());
            return new JsonResponse("Hiba történt! Kérem keresse fel az üzemeltetőt!", "error");
        }
    }

    public JsonResponse add(PaymentDto paymentDto, Principal principal) {

        try {
            if (!userService.existByUsername(principal.getName())) {
                return new JsonResponse("Hiba történt! A felhasználó nem található!", "error");
            }
            User user = userService.findByUsername(principal.getName());

            if (!packageService.exists(paymentDto.getPackageId())) {
                return new JsonResponse("Hiba történt! A Csomag nem található!", "error");
            }
            Package ePackage = packageService.getOne(paymentDto.getPackageId());
            List<Payment> paymentList = paymentRepository.findAllByUserIdAndStatus(user, PaymentStatus.IN_PROCESS);
            for (Payment payment : paymentList){
                if(payment.getPackageId().equals(ePackage)){
                    return new JsonResponse("Ezt a csomagot már megrendelted, a profilodnál rendezheted rendelésed!", "error");
                }
            }
            Payment payment = new Payment();
            payment.setCreatedTime(LocalDateTime.now());
            payment.setUserId(user);
            payment.setPackageId(ePackage);
            payment.setStatus(PaymentStatus.IN_PROCESS);
            if (isFirstCustomerWithRefLink(principal)){
                payment.setDiscount(30);
            }
            paymentRepository.save(payment);
            return new JsonResponse("Sikeresen megrendelted az alábbi csomagot: '"+payment.getPackageId().getName()+"' !", "success",payment);
        }catch (Exception e) {
            System.out.println("Hiba történt mentlés közben: "+e.getMessage());
            return new JsonResponse("Hiba történt! Kérem keresse fel az üzemeltetőt!", "error");
        }
    }

    public JsonResponse undo(Long id) {
        try {
            Payment payment = paymentRepository.getOne(id);
            payment.setStatus(PaymentStatus.IN_PROCESS);
            User user = payment.getUserId();
            user.setDaysLeftVip(user.getDaysLeftVip()- payment.getPackageId().getInterval());
            if(user.getDaysLeftVip()< 0){
                user.setDaysLeftVip(0);
            }
            if( user.getDaysLeftVip() <= 0){
                Role role = roleService.getVip();
                user.getRoles().remove(role);
            }

            if (user.getRefUser()!=null) {
                Integer bonus = 0;
                if (payment.getDiscount() != null) {
                    double discount = (double) payment.getDiscount() / 100;
                    bonus = (int) ((payment.getPackageId().getPrice() * (1.0-discount)) / 2);
                } else {
                    bonus = (int) (payment.getPackageId().getPrice() / 2);
                }
                user.getRefUser().setBalance(user.getRefUser().getBalance() == null ? 0 : user.getRefUser().getBalance() - bonus);
                userService.save(user.getRefUser());
            }
            payment.setStatus(PaymentStatus.IN_PROCESS);
            payment.setActivateTime(null);
            userService.save(user);
            paymentRepository.save(payment);
            return new JsonResponse("Sikeres csomag visszaállítás! Felhasználó: "+payment.getUserId().getUsername() + " Csomag: "+ payment.getPackageId().getName(), "success");
        }catch (Exception e) {
            System.out.println("Hiba történt mentlés közben: "+e.getMessage());
            return new JsonResponse("Hiba történt! Kérem keresse fel az üzemeltetőt!", "error");
        }
    }

    public Payment getOne(Long id) {
        return  paymentRepository.findById(id).get();
    }

    public DataTablesOutput<Payment> findListUserPayment(DataTablesInput input, Principal principal) {

        Specification<Payment> additionalSpecification = new Specification<Payment>() {
            @Override
            public Predicate toPredicate(Root<Payment> root, CriteriaQuery<?> query, CriteriaBuilder cb) {


                List<Predicate> predicates = new ArrayList<>();
                predicates.add(
                        cb.like(root.join("userId").get("username"),principal.getName())
                );

                return cb.and(predicates.toArray(new Predicate[0]));

            };
        };

        return paymentRepository.findAll(input, additionalSpecification);
    }

    public void dayMinusz() {
        List<User> userList = userService.findAllList();
        for (User user : userList){
            if(user.getDaysLeftVip() > 0){
                user.setDaysLeftVip(user.getDaysLeftVip()-1);
                Role roleVip = roleService.getVip();
                if(user.getDaysLeftVip() == 0){
                    user.getRoles().remove(roleVip);
                }
                userService.save(user);
            }
        }
    }

    public JsonResponse addGift(PaymentDto paymentDto) {
        try {
            if (!userService.existById(paymentDto.getUserId())) {
                return new JsonResponse("Hiba történt! A felhasználó nem található!", "error");
            }
            User user = userService.get(paymentDto.getUserId());

            if (!packageService.exists(paymentDto.getPackageId())) {
                return new JsonResponse("Hiba történt! A Csomag nem található!", "error");
            }
            Package ePackage = packageService.getOne(paymentDto.getPackageId());
            Payment payment = new Payment();
            payment.setCreatedTime(LocalDateTime.now());
            payment.setUserId(user);
            payment.setPackageId(ePackage);
            payment.setStatus(PaymentStatus.DONE);
            paymentRepository.save(payment);
            paid(payment.getId());

            return new JsonResponse("Sikeres csomag aktíválás! Felhasználó: "+ user.getUsername() +" Csomag: " +ePackage.getName() , "success");
        }catch (Exception e) {
            System.out.println("Hiba történt mentlés közben: "+e.getMessage());
            return new JsonResponse("Hiba történt! Kérem keresse fel az üzemeltetőt!", "error");
        }
    }

    public boolean isFirstCustomerWithRefLink(Principal principal) {
        User user=userService.findByUsername(principal.getName());
        return !paymentRepository.existsByUserId(user)&& user.getRefUser()!=null;
    }

    public Long countOfSuccesPaymentByRefUser(Long id)
    {
        return paymentRepository.countOfSuccesPaymentByRefUser(id);
    }
    public Long countOfPaidUser(Long id)
    {
        return paymentRepository.countOfPaidUser(id);
    }
}
