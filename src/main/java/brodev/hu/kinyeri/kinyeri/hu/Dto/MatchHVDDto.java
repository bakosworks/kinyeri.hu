package brodev.hu.kinyeri.kinyeri.hu.Dto;

import brodev.hu.kinyeri.kinyeri.hu.Models.enums.MatchResult;
import lombok.Data;

import javax.persistence.Column;

@Data
public class MatchHVDDto {
    private Long matchId;
    private Long home = 0l;
    private Double homePoints = 0.;
    private Long visitor = 0l;
    private Double visitorPoints = 0.;
    private Long draw = 0l;
    private Double drawPoints = 0.;
    private Double goalOver;
    private Long goalTippHome;
    private Long goalTippVisitor;
    private Long maxWinRate;
    private MatchResult voted;
    private MatchResult matchResultWon;
    private Boolean goalOverWon;
    private Boolean bothTeamGoalWon;
    private String over2500;
    private String over3500;
    private String over4500;
    private String over5500;
    private String over6500;
    private String over1500;
    private String under1500;
    private String under2500;
    private String bothTeamYes;
    private String bothTeamNo;
    private String oddsH;
    private String oddsD;
    private String oddsV;
}
