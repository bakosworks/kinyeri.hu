package brodev.hu.kinyeri.kinyeri.hu.Dto;

import brodev.hu.kinyeri.kinyeri.hu.Models.Comment;
import lombok.Data;

import java.util.List;

@Data
public class CommentDto {
    private Long commentQty;
    private List<Comment> comments;
}
