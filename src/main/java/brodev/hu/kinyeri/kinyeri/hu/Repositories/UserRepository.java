package brodev.hu.kinyeri.kinyeri.hu.Repositories;


import brodev.hu.kinyeri.kinyeri.hu.Models.Match;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public interface UserRepository extends JpaRepository<User, Long>  , DataTablesRepository<User,Long >, CrudRepository<User,Long> {
    User findFirstByUsername(String username);


    Boolean existsByUsername(String Username);


    @Query(nativeQuery = true,value = "SELECT count(id) FROM users where email = :mail")
    Integer countOfEmail(@Param("mail")String mail);

    User findByEmail(String email);
    Optional<User> findByEmailOrderById(String email);

    Boolean existsByEmailAndPasswordIsNull(String email);

    boolean existsByEmail(String email);

    Boolean existsByEmailAndPassword(String mail,String password);


    List<User> findTop10ByLastTipGreaterThanEqualAndClosedTipsGreaterThanEqualOrderByBoostedWinRateDesc(LocalDateTime time,Integer integer);
    List<User> findTop10ByLastTipGreaterThanEqualAndClosedTipsGreaterThanEqualOrderByWinRateDesc(LocalDateTime time,Integer integer);
    List<User> findTop10ByLastTipGreaterThanEqualAndClosedTipsGreaterThanEqualOrderByWinFinalResultWinRateDesc(LocalDateTime time, Integer integer);
    List<User> findTop10ByLastTipGreaterThanEqualAndClosedTipsGreaterThanEqualOrderByWinNumberOfGolsWinRateDesc(LocalDateTime time, Integer integer);
    List<User> findTop10ByLastTipGreaterThanEqualAndClosedTipsGreaterThanEqualOrderByWinWichTeamGoesWinRateDesc(LocalDateTime time, Integer integer);
    List<User> findTop10ByLastTipGreaterThanEqualAndClosedTipsGreaterThanEqualOrderByWinWinningTeamWinRateDesc(LocalDateTime time, Integer integer);

    User findByUsername(String name);

    List<User> findAllByActive(Boolean b);

    List<User> findAllByRoles_Role(String role);

    List<User> findAllByDaysLeftVipGreaterThan(Integer integer);

    List<User> findAllByActiveAndEmailNotification(boolean b, boolean b1);

    List<User> findAllByEmailNotification(boolean b);

    long countDistinctByTippList_TippId_MatchId_MatchTimeIsGreaterThanAndTippList_TippId_MatchId_MatchTimeIsLessThan(LocalDateTime from,LocalDateTime to);

    Optional<User> findFirstByUsernameOrRefLink(String reflink, String reflink1);

    Integer countAllByRefUserAndPassword(User u,String password);

    List<User> findAllByRefLinkIsNull();
    boolean existsByRefLink(String s);

    Integer countAllByRefUser(User u);

    Integer countAllByRefUserAndPasswordIsNot(User u,String password);


    @Query(nativeQuery = true, value = "select users.id from \n" +
            "            (SELECT c.user as userId ,count(*) as numberofTips,us.boosted_win_rate from \n" +
            "            (SELECT  Distinct t.user, date(match_time) FROM  tipps as t inner join matches as m where m.id =  t.match_id and YEAR(match_time) = :yearm and MONTH(match_time) = :monthm ) as c \n" +
            "            inner join users as us where  us.id = c.user and us.tip_num > 400\n" +
            "            group by c.user \n" +
            "            order by us.boosted_win_rate desc\n" +
            "            ) as maybe\n" +
            "            inner join users \n" +
            "            where maybe.userId = users.id and\n" +
            "            (SELECT count(distinct(date(match_time))) FROM matches where YEAR(match_time) = :yearm and MONTH(match_time) = :monthm ) - maybe.numberofTips < 10 and maybe.numberofTips > 0 and users.active = 1 and users.username != 'admin'\n" +
            "            ORDER BY `maybe`.`boosted_win_rate` DESC, users.tip_num DESC limit 10")
    List<Long> findTop10ForCurrentMonthRemoveUserNumberOfTipsLassThan3AndOrderByWinrateDescNumberOfTipsDesc(@Param("monthm")Integer month,@Param("yearm")Integer year);




    @Query(nativeQuery = true, value = "select kaki.id from \n" +
            "            (SELECT c.user as userId ,count(*) as numberofTips,us.boosted_win_rate from \n" +
            "            (\n" +
            "            SELECT * from \n" +
            "            ( \n" +
            "            SELECT cc.user,count(*) as numberofTips,us.boosted_win_rate from \n" +
            "\t\t\t\t(SELECT  user, date(match_time) FROM  tipps as t inner join matches as m where m.id =  t.match_id and YEAR(match_time) = :yearm and MONTH(match_time) = :monthm and DAY(match_time) < '20' ) as cc \n" +
            "                  inner join users as us where  us.id = cc.user and us.tip_num > 400  \n" +
            "            group by cc.user \n" +
            "            order by us.boosted_win_rate desc) as ex where numberofTips > 30) as c \n" +
            "            inner join users as us where  us.id = c.user  \n" +
            "            group by c.user \n" +
            "            order by us.boosted_win_rate desc\n" +
            "            ) as maybe\n" +
            "            inner join users  as kaki\n" +
            "            where maybe.userId = kaki.id  and maybe.numberofTips > 0 and kaki.active = 1 and kaki.username != 'admin'\n" +
            "            ORDER BY `maybe`.`boosted_win_rate` DESC, kaki.tip_num DESC limit 10")
    List<Long> findTop10ForCurrentMonthRemoveUserNumberOfTipsLassThan3AndOrderByWinrateDescNumberOfTipsDescAfterDay20(@Param("monthm")Integer month,@Param("yearm")Integer year);

    List<User> findAllByIdIn(Collection<Long> coll);
}
