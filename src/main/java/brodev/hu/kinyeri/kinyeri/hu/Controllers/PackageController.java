package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Models.Package;
import brodev.hu.kinyeri.kinyeri.hu.Models.Team;
import brodev.hu.kinyeri.kinyeri.hu.Services.PackageService;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/package")
public class PackageController {

    private final PackageService packageService;

    @GetMapping("/find-all-active")
    public List<Package> findAllActive(){
        return packageService.findAllActive();
    }

    @GetMapping("/get")
    public Package getPackage(@RequestParam("id") Long id){
        return packageService.getOne(id);
    }

    @PostMapping(value = "/list", name = "Get all Packages to DataTable")
    public DataTablesOutput<Package> getAllTeams(@Valid @RequestBody DataTablesInput input) {
        return packageService.findAll(input);
    }

    @PostMapping("/add")
    public JsonResponse addNewPackage(@RequestBody Package aPackage){
        return packageService.addNewPackage(aPackage);
    }
    @PutMapping("/edit")
    public JsonResponse editPackage(@RequestBody Package aPackage){
        if (packageService.exists(aPackage.getId())) {
            return packageService.edit(aPackage);
        }else{
            return new JsonResponse("Nem létezik csomag ilyen id-val!","error");
        }
    }
}