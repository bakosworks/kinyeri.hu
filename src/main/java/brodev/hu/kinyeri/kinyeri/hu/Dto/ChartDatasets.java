package brodev.hu.kinyeri.kinyeri.hu.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChartDatasets {
    private String label;
    private String backgroundColor;
    private List<Integer> data;
}
