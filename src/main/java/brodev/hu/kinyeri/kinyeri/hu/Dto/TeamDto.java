package brodev.hu.kinyeri.kinyeri.hu.Dto;

import lombok.Data;

@Data
public class TeamDto {

    private Long id;
    private String name;
}
