package brodev.hu.kinyeri.kinyeri.hu.Dto;

import lombok.Data;

@Data
public class TippDto {
    private Long matchId;
    private String radioName;
    private Integer goalQty;
    private Integer homeQty;
    private Integer visitorQty;
}
