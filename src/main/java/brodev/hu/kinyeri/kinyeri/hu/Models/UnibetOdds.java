
package brodev.hu.kinyeri.kinyeri.hu.Models;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "unibet_odds")
public class UnibetOdds {

    @Id
    @Column(name = "line_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "over_2500")
    private Float over2500;

    @Column(name = "over_3500")
    private Float over3500;

    @Column(name = "over_4500")
    private Float over4500;

    @Column(name = "over_5500")
    private Float over5500;

    @Column(name = "over_6500")
    private Float over6500;

    @Column(name = "over_1500")
    private Float over1500;

    @Column(name = "under_1500")
    private Float under1500;

    @Column(name = "under_2500")
    private Float under2500;

    @Column(name = "both_team_yes")
    private Float bothTeamYes;

    @Column(name = "both_team_no")
    private Float bothTeamNo;

}

