package brodev.hu.kinyeri.kinyeri.hu.Services;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import java.io.Serializable;

public class BaseService<T,ID extends Serializable,R extends DataTablesRepository> {

    protected R repository;

    public BaseService(R repository) {
        this.repository = repository;
    }

    public DataTablesOutput<T> list(DataTablesInput dataTablesInput)
    {
       return repository.findAll(dataTablesInput);
    }



}
