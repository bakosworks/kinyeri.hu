package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Dto.CommentDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.Comment;
import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Services.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/comments")
public class CommentController {

    private final CommentService commentService;

    @GetMapping("/get-comments/{matchId}/{commentId}")
    public CommentDto getComments(@PathVariable(name = "matchId") Long matchId,
                                  @PathVariable(name = "commentId") Long commentId){
        return commentService.get10Comment(matchId,commentId);
    }
    @PutMapping("/delete/{id}")
    public void deleteComment(@PathVariable Long id, Principal principal){
        commentService.deleteComment(id,principal);
    }
}