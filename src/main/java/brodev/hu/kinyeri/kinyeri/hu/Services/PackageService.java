package brodev.hu.kinyeri.kinyeri.hu.Services;

import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Models.Package;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.PackageRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PackageService {

    private final PackageRepository packageRepository;

    public List<Package> findAllActive(){
        return packageRepository.findAllByActiveOrderById(true);
    }

    public JsonResponse addNewPackage(Package pckg){
        packageRepository.save(pckg);
        return new JsonResponse("Sikeres csomag létrehozás","success");
    }

    public JsonResponse edit(Package pckg){
        packageRepository.save(pckg);
        return new JsonResponse("Sikeres csomag módosítás","success");
    }

    public boolean exists(Long id) {
        return packageRepository.existsById(id);
    }

    public DataTablesOutput<Package> findAll(DataTablesInput input) {
        return packageRepository.findAll(input);
    }

    public Package getOne(Long id) {
        return packageRepository.getOne(id);
    }


    public List<Package> findAllList() {
        return packageRepository.findAll();
    }
}
