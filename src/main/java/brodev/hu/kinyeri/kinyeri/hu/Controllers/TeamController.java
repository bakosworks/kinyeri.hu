package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Dto.InputDto;
import brodev.hu.kinyeri.kinyeri.hu.Dto.TeamDto;
import brodev.hu.kinyeri.kinyeri.hu.Dto.UserDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Models.Team;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import brodev.hu.kinyeri.kinyeri.hu.Services.TeamService;
import brodev.hu.kinyeri.kinyeri.hu.Services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/team")
public class TeamController {

    @Autowired
    private TeamService teamService;

    @Autowired
    private ModelMapper modelMapper;


    @PostMapping(value = "/list", name = "Get all Teams to DataTable")
    public DataTablesOutput<Team> getAllTeams(@Valid @RequestBody DataTablesInput input) {
        return teamService.findAll(input);
    }

    @PostMapping(value = "/add", name ="Add a Team from DTO" )
    public JsonResponse addTeamByDTO(@RequestBody TeamDto teamDto){
        try {

            Team team = modelMapper.map(teamDto, Team.class);
            if(!teamService.existByName(team.getName()))
            {
                teamService.save(team);
                return new JsonResponse("Sikeres csapat mentés!","success");
            }
            else
            {
                return new JsonResponse("Csapatnév már foglalt!","error");
            }
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"error");
        }
    }

    @PostMapping(value = "/edit", name ="Add a User from DTO" )
    public JsonResponse editTeamByDTO(@RequestBody TeamDto teamDto){
        try {
            Team team = modelMapper.map(teamDto, Team.class);
            if(!teamService.existByName(team.getName()))
            {
                teamService.save(team);
                return new JsonResponse("Sikeres csapat mentés!","success");
            }
            else
            {
                return new JsonResponse("Csapatnév már foglalt!","error");
            }
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"error");
        }
    }

    @PostMapping(value = "/add-by-select", name ="Add a Team from DTO" )
    public List<InputDto> addByTeam(@RequestBody TeamDto teamDto){

        Team team = modelMapper.map(teamDto, Team.class);

        if(!teamService.existByName(team.getName()))
        {
            teamService.save(team);
        }

         return teamService.findAllList().stream()
                 .map(team1 -> InputDto.builder().
                         text(team1.getName())
                         .value(team1.getName())
                         .build())
                 .collect(Collectors.toList());
    }

    @GetMapping(value = "/get", name ="Get a Team by Id")
    public TeamDto getTeamById(@RequestParam("id") Long id){
        return modelMapper.map(teamService.get(id),TeamDto.class);
    }

    @DeleteMapping(value = "/delete/{id}", name="Delete a position by Id")
    public JsonResponse deleteTeamById(@PathVariable("id") Long id) {
        return teamService.delete(id);
    }
}
