package brodev.hu.kinyeri.kinyeri.hu.Models;

import lombok.Data;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@Table(name = "all_time_top_statistics")
@Immutable
public class AllTimeTopStatistic extends BaseTopStatistic{

}
