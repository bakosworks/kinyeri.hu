
package brodev.hu.kinyeri.kinyeri.hu.Models;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "teams")
public class Team {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;
}

