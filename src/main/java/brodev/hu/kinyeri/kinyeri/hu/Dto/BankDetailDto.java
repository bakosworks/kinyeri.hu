package brodev.hu.kinyeri.kinyeri.hu.Dto;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

@Data
public class BankDetailDto {

    private Long id;


    private String name;


    private String iban;

    private String bankNumber;

    private String swiftCode;

    private String bankName;

    private String phone;
}
