package brodev.hu.kinyeri.kinyeri.hu.Models;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;


@Table(name = "lottery_user")
@Entity
@Data
public class LotteryUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @OneToOne
    @JoinColumn(name = "lottery")
    private Lottery lotteryId;

    @ManyToOne
    @JoinColumn(name = "user")
    private User user;

    @Column(name = "position")
    private Integer position;

    @Column(name = "money")
    private Double money;

}



