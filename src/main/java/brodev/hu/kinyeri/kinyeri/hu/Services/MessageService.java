
package brodev.hu.kinyeri.kinyeri.hu.Services;

import brodev.hu.kinyeri.kinyeri.hu.Dto.MessageDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.Message;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class MessageService {


    private final SimpMessagingTemplate brokerMessagingTemplate;

    private final MessageRepository messageRepository;

    private final UserService userService;

    public Message saveMessage(MessageDto message, Authentication authentication){
        User user=userService.FindByUserName(authentication.getName());
        if (user.getBanned().equals(true)){
            return null;
        }
        Message msg=new Message();
        msg.setInsertTime(LocalDateTime.now());
        msg.setText(message.getText());
        msg.setUser(user);
        return messageRepository.save(msg);
    }

    public List<Message> get50MessagesFromId(Long id) {
        if (id==-111){
            return messageRepository.findTop50ByUser_BannedAndDeletedByIsNullOrderByIdDesc(false);
        }else {
            return messageRepository.findTop50ByIdLessThanAndUser_BannedAndDeletedByIsNullOrderByIdDesc(id,false);
        }
    }

    public void deleteComment(Long id, Principal principal) {
        messageRepository.findById(id).ifPresent(message -> {
            User user=userService.FindByUserName(principal.getName());
            if (message.getUser().equals(user) || user.getRoles().stream().anyMatch(role -> role.getRole().equals("MOD"))) {
                message.setDeletedBy(user);
                message.setDeleteTime(LocalDateTime.now());
                messageRepository.save(message);
                brokerMessagingTemplate.convertAndSend("/topic/chat-delete", message.getId());
            }
        });
    }
}

