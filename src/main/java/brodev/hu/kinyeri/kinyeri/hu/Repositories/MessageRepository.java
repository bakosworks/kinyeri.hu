
package brodev.hu.kinyeri.kinyeri.hu.Repositories;

import brodev.hu.kinyeri.kinyeri.hu.Models.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message,Long> {
    List<Message> findTop50ByIdLessThanAndUser_BannedAndDeletedByIsNullOrderByIdDesc(Long id,Boolean bool);
    List<Message> findTop50ByUser_BannedAndDeletedByIsNullOrderByIdDesc(Boolean aBoolean);
}

