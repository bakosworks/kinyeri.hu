package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Dto.TippDto;
import brodev.hu.kinyeri.kinyeri.hu.Dto.UserDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Models.Tipp;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import brodev.hu.kinyeri.kinyeri.hu.Services.TippService;
import brodev.hu.kinyeri.kinyeri.hu.Services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/tipp")
@AllArgsConstructor
public class TippController {
    private final TippService tippService;
    private final UserService userService;



    @PostMapping(value = "/list", name = "Get all Matches to DataTable")
    public DataTablesOutput<Tipp> getAllUser(@Valid @RequestBody DataTablesInput input) {
        return tippService.findAll(input);
    }

    @PostMapping(value = "/list-own-tipps", name = "Get all Matches to DataTable")
    public DataTablesOutput<Tipp> getAllOwnTips(@Valid @RequestBody DataTablesInput input, Principal principal) {

        return tippService.findAllUserTips(input,principal);
    }

    @PostMapping(value = "/list-tipps/{username}", name = "Get all Matches to DataTable")
    public DataTablesOutput<Tipp> getAllUserTips(@Valid @RequestBody DataTablesInput input,@PathVariable String username) {

        return tippService.findAll(input,username);
    }

    @PostMapping(value = "/list-tipps-hidden/{username}", name = "Get all Matches to DataTable")
    public DataTablesOutput<Tipp> getAllTippsWithoutVip(@Valid @RequestBody DataTablesInput input,@PathVariable String username) {

        return tippService.findAllWithoutVip(input,username);
    }

    @PostMapping(value = "/tipp-submit")
    public JsonResponse saveTipp(@RequestBody TippDto tippDto, Principal principal){
        return tippService.saveTippDto(tippDto, principal);
    }

//    @PostMapping(value = "/active-top10")
//    public List<User> userDto(){
//        List<User> users = userService.userfindUsersTodayTomorrowTipsAndTippsOver50AndWinrateOver50();
//        List<UserDto> userDtos = new ArrayList<>();
//        for(User user : users){
//           UserDto userDto = userService.userWinRateCalculate(user);
//           userDtos.add(userDto);
//        }
//        return users;
//    }


}
