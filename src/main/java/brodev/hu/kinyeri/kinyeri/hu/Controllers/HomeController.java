
package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Dto.UserModDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.Match;
import brodev.hu.kinyeri.kinyeri.hu.Models.Role;
import brodev.hu.kinyeri.kinyeri.hu.Models.Team;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import brodev.hu.kinyeri.kinyeri.hu.Services.MatchService;
import brodev.hu.kinyeri.kinyeri.hu.Services.ReflinkService;
import brodev.hu.kinyeri.kinyeri.hu.Services.UserService;
import lombok.AllArgsConstructor;
import org.apache.tomcat.jni.Local;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@AllArgsConstructor
public class HomeController {

    private final MatchService matchService;

    private final UserService userService;

    private final ReflinkService reflinkService;

    @RequestMapping(value = {"/", "/index"}, method =RequestMethod.GET)
    public String index(Model model, Principal principal) {

        model.addAttribute("user",isMod(principal));
        model.addAttribute("todayMatches",matchService.getAllWithStatisticsToday(principal));
        model.addAttribute("reflink",reflinkService.findOne());
        model.addAttribute("now", LocalDateTime.now());
        return "/index";

    }
    @GetMapping(value = "/tegnapi")
    public ModelAndView yesterdayPage(Principal principal)
    {

        return new ModelAndView("page/yesterday").
                addObject("reflink",reflinkService.findOne()).
                addObject("todayMatches",matchService.getAllWithStatisticsYesterday(principal)).
                addObject("user",isMod(principal));
    }
    @GetMapping(value = "/holnapi")
    public ModelAndView tomorrowPage(Principal principal)
    {
        return new ModelAndView("page/tomorrow").addObject("reflink",reflinkService.findOne()).addObject("todayMatches",matchService.getAllWithStatisticsTomorrow(principal)).addObject("user",isMod(principal));
    }

    private UserModDto isMod(Principal principal) {
        boolean mod=false;
        Long userId=null;
        if (principal!=null){
            User user=userService.FindByUserName(principal.getName());
            userId=user.getId();
            Optional<Role> role= user.getRoles().stream()
                    .filter(role1 -> role1.getRole().equals("MOD"))
                    .findFirst();
            if (role.isPresent()){
                mod=true;
            }
        }
        return new UserModDto(mod,userId);
    }

    @GetMapping(value = "/aszf")
    public ModelAndView aszf()
    {
        return new ModelAndView("docs");
    }
}

