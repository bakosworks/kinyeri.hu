
package brodev.hu.kinyeri.kinyeri.hu.Models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TippId implements Serializable {

    @ManyToOne
    @JoinColumn(name = "user")
    private User user;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "match_id")
    private Match matchId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TippId that = (TippId) o;
        return user.equals(that.user) &&
                matchId.equals(that.matchId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, matchId);
    }
}

