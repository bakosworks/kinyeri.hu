package brodev.hu.kinyeri.kinyeri.hu.Services;

import brodev.hu.kinyeri.kinyeri.hu.Dto.*;
import brodev.hu.kinyeri.kinyeri.hu.Mapper.MatchMapper;
import brodev.hu.kinyeri.kinyeri.hu.Models.*;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.MatchRepository;
import com.google.common.util.concurrent.AtomicDouble;
import org.joda.time.DateTime;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


import javax.mail.MessagingException;
import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class MatchService {

    private final MailService mailService;

    private final MatchTippStatisticsService matchTippStatisticsService;

    private final MatchMapper matchMapper;

    private final MatchRepository matchRepository;

    private final TippService tippService;

    private final UserService userService;

    private final NotificationService notificationService;

    public List<Match> findClosed() {
        return matchRepository.findAllByResultIsNotNull();
    }

    public HistoryDto findAllByResultIsNotNull(Pageable pageable) {

        Page<Match> returnValue = matchRepository.findAllByResultIsNotNull(pageable);

        List<MatchWithStatisticDto> finalList = returnValue.getContent().stream()
                .map(match -> MatchWithStatisticDto.builder()
                        .match(match)
                        .matchHVDDto(matchTippStatisticsService.getMatchStatistics(match, null, match.getId(), tippService.getMatchTipps(match.getId())))
                        .build())
                .collect(Collectors.toList());

        return HistoryDto.builder().list(finalList).pageNumber(returnValue.getPageable().getPageNumber()).build();

    }

    public MatchService(MailService mailService, MatchTippStatisticsService matchTippStatisticsService, MatchMapper matchMapper, MatchRepository matchRepository, TippService tippService, UserService userService, NotificationService notificationService) {
        this.mailService = mailService;
        this.matchTippStatisticsService = matchTippStatisticsService;
        this.matchMapper = matchMapper;
        this.matchRepository = matchRepository;
        this.tippService = tippService;
        this.userService = userService;
        this.notificationService = notificationService;
    }

    private int points;
    private List<Tipp> matchTipps = new ArrayList<>();
    private int winCount;
    private MatchHVDDto matchHVDDto;
    private int goolOverCounter;
    private int goolOverSum;
    private int goolHomeCounter;
    private int goolHomeSum;
    private int goolVisitorCounter;
    private int goolVisitorSum;

    public DataTablesOutput<Match> findAll(DataTablesInput input) {
        return matchRepository.findAll(input);
    }

    public Match save(Match match) {
        return matchRepository.save(match);
    }

    public Match get(Long id) {
        return matchRepository.getOne(id);
    }

    public Match findById(Long id) {
        return matchRepository.findById(id).orElse(null);
    }


    public JsonResponse delete(Long id) {
        try {
            DeleteALLTipps(id);
            matchRepository.deleteById(id);
            return new JsonResponse("Sikeres törlés", "success");
        } catch (DataIntegrityViolationException cve) {
            return new JsonResponse("Ez a mérkőzés használatban van így nem törölhető!", "error");
        } catch (EmptyResultDataAccessException e) {
            return new JsonResponse("Ez a mérkőzés  már törölve van!", "error");
        } catch (Exception e) {
            return new JsonResponse("Sikertelen törlés" + e.getMessage(), "error");
        }
    }

    private void DeleteALLTipps(Long id) {

        for (Tipp tipp : tippService.getMatchTipps(id)) {
            User user = tipp.getTippId().getUser();
            if (user.getTipNum() >= 4) {
                user.setTipNum(user.getTipNum() - 4);
            }

            if (tipp.getFinalPoints() != null) {
                user.setWinNum(user.getWinNum() - tipp.getFinalPoints());
            }

            if (tipp.isWinWinningTeam()) {
                user.setWinWinningTeamQty(user.getWinWinningTeamQty() - 1);
            }

            if (tipp.isWinFinalResult()) {
                user.setWinFinalResultQty(user.getWinFinalResultQty() - 1);
            }

            if (tipp.isWinNumberOfGols()) {
                user.setWinNumberOfGolsQty(user.getWinNumberOfGolsQty() - 1);
            }

            if (tipp.isWinWichTeamGoes()) {
                user.setWinWichTeamGoesQty(user.getWinWichTeamGoesQty() - 1);
            }

            if (user.getWinNum() == 0 && user.getTipNum() == 0) {
                user.setWinRate(0);
            } else {
                user.setWinRate((user.getWinNum() * 100) / user.getTipNum());
            }

            userService.save(user);
            tippService.delete(tipp.getTippId());
        }
    }

    public Match findOneById(Long id) {
        Optional<Match> match = matchRepository.findById(id);
        if (match.isPresent()) {
            return match.get();
        }
        return null;

    }

    public void calculatePoints(Match match) {
        matchTipps = tippService.getMatchTipps(match.getId());
        for (Tipp tipp : matchTipps) {
            boolean f = false;
            User user = pointsWinCalculate(tipp.getTippId().getUser(), tipp, match);
            user.setWinRate((user.getWinNum() * 100) / (user.getTipNum()));
             if (user.getTipNum() > 199) {
                user.setBoostedWinRate(user.getWinRate() + 2);
            } else if (user.getTipNum() > 499) {
                user.setBoostedWinRate(user.getWinRate() + 4);
            } else if (user.getTipNum() > 999) {
                user.setBoostedWinRate(user.getWinRate() + 6);
            } else {
                user.setBoostedWinRate(user.getWinRate());
            }

            int winrateBoost = user.getClosedTips() == 0 ? 0 : (user.getWinWichTeamGoesQty() * 2 + user.getWinNumberOfGolsQty() + user.getWinFinalResultQty() * 20 + user.getWinWinningTeamQty() * 10) / user.getClosedTips();

            user.setBoostedWinRate(user.getBoostedWinRate() + winrateBoost);

            if (tippService.countUserTippsForCurrentMonth(user) >= 60) {
                user.setBoostedWinRate(user.getBoostedWinRate() + 1);
                f = true;
            }
            if (tippService.countUserTippDistinctDays(user) >= 20) {
                user.setBoostedWinRate(user.getBoostedWinRate() + 1);
                if (f) {

                    List<Tipp> listTipp = tippService.getCurrentMonthTipps(tipp.getTippId().getUser());

                    AtomicDouble sum = new AtomicDouble(0);

                    AtomicInteger winCounter = new AtomicInteger(0);

                    listTipp.forEach(tipp1 -> {

                        if (tipp.isWinWinningTeam()) {

                            winCounter.incrementAndGet();
                            switch (tipp.getResultTip()) {
                                case H:
                                    sum.addAndGet(tipp.getTippId().getMatchId().getOddsH());
                                    break;
                                case D:
                                    sum.addAndGet(tipp.getTippId().getMatchId().getOddsD());
                                    break;
                                case V:
                                    sum.addAndGet(tipp.getTippId().getMatchId().getOddsV());
                                    break;
                            }
                        }
                    });

                    user.setBoostedWinRate(user.getBoostedWinRate() + (int) Math.round(sum.get() / winCounter.get()));
                }
            }

            userService.save(user);
        }
    }

    private User pointsWinCalculate(User user, Tipp tipp, Match match) {
        if (tipp.getFinalPoints() != null) {
            user.setWinNum(user.getWinNum() - tipp.getFinalPoints());
        }

        winCount = 0;
        //hazai vendég Döntetlen
        if (tipp.getResultTip() != null) {
            if (tipp.getResultTip().equals(match.getResult())) {
                winCount++;
                if (!tipp.isWinWinningTeam()) {
                    user.setWinWinningTeamQty(user.getWinWinningTeamQty() + 1);
                }
                tipp.setWinWinningTeam(true);

            } else {
                if (tipp.isWinWinningTeam()) {
                    user.setWinWinningTeamQty(user.getWinWinningTeamQty() - 1);
                }
                tipp.setWinWinningTeam(false);
            }
        }
        //hazai/vendég gól
        if (tipp.getHomeGoalQtyTipp().equals(match.getHomeGoalQty()) && tipp.getVisitorGoalQtyTipp().equals(match.getVisitorGoalQty())) {
            winCount++;
            if (!tipp.isWinFinalResult()) {
                user.setWinFinalResultQty(user.getWinFinalResultQty() + 1);
            }
            tipp.setWinFinalResult(true);
        } else {
            if (tipp.isWinFinalResult()) {
                user.setWinFinalResultQty(user.getWinFinalResultQty() - 1);
            }
            tipp.setWinFinalResult(false);
        }
        //gólok száma
        if (tipp.getGoalQty() == (match.getVisitorGoalQty() + match.getHomeGoalQty())) {
            winCount++;
            if (!tipp.isWinNumberOfGols()) {
                user.setWinNumberOfGolsQty(user.getWinNumberOfGolsQty() + 1);
            }
            tipp.setWinNumberOfGols(true);
        } else {
            if (tipp.isWinNumberOfGols()) {
                user.setWinNumberOfGolsQty(user.getWinNumberOfGolsQty() - 1);
            }
            tipp.setWinNumberOfGols(false);
        }


        if ((match.getHomeGoalQty() > 0 && match.getVisitorGoalQty() > 0) == (tipp.getVisitorGoalQtyTipp() > 0 && tipp.getHomeGoalQtyTipp() > 0)) {
            if (!tipp.isWinWichTeamGoes()) {
                user.setWinWichTeamGoesQty(user.getWinWichTeamGoesQty() + 1);
            }
            tipp.setWinWichTeamGoes(true);
            winCount++;
        } else {
            if (tipp.isWinWichTeamGoes()) {
                user.setWinWichTeamGoesQty(user.getWinWichTeamGoesQty() - 1);
            }
            tipp.setWinWichTeamGoes(false);
        }

        tipp.setFinalPoints(winCount);
        tippService.save(tipp);
        if (user.getWinNum() != null) {
            user.setWinNum(user.getWinNum() + winCount);
        } else {
            user.setWinNum(winCount);
        }
        return user;
    }


    private User pointsWinCalculateSEC(User user, Tipp tipp, Match match) {
        if (tipp.getFinalPoints() != null) {
            user.setWinNum(user.getWinNum() - tipp.getFinalPoints());
        }

        winCount = 0;
        //hazai vendég Döntetlen
        if (tipp.getResultTip() != null) {
            if (tipp.getResultTip().equals(match.getResult())) {
                winCount++;
                if (!tipp.isWinWinningTeam()) {
                    user.setWinWinningTeamQty(user.getWinWinningTeamQty() + 1);
                }
                tipp.setWinWinningTeam(true);

            } else {
                if (tipp.isWinWinningTeam()) {
                    user.setWinWinningTeamQty(user.getWinWinningTeamQty() - 1);
                }
                tipp.setWinWinningTeam(false);
            }
        }
        //hazai/vendég gól
        if (tipp.getHomeGoalQtyTipp().equals(match.getHomeGoalQty()) && tipp.getVisitorGoalQtyTipp().equals(match.getVisitorGoalQty())) {
            winCount++;
            if (!tipp.isWinFinalResult()) {
                user.setWinFinalResultQty(user.getWinFinalResultQty() + 1);
            }
            tipp.setWinFinalResult(true);
        } else {
            if (tipp.isWinFinalResult()) {
                user.setWinFinalResultQty(user.getWinFinalResultQty() - 1);
            }
            tipp.setWinFinalResult(false);
        }
        //gólok száma
        if (tipp.getGoalQty() == (match.getVisitorGoalQty() + match.getHomeGoalQty())) {
            winCount++;
            if (!tipp.isWinNumberOfGols()) {
                user.setWinNumberOfGolsQty(user.getWinNumberOfGolsQty() + 1);
            }
            tipp.setWinNumberOfGols(true);
        } else {
            if (tipp.isWinNumberOfGols()) {
                user.setWinNumberOfGolsQty(user.getWinNumberOfGolsQty() - 1);
            }
            tipp.setWinNumberOfGols(false);
        }


        if ((match.getHomeGoalQty() > 0 && match.getVisitorGoalQty() > 0) == (tipp.getVisitorGoalQtyTipp() > 0 && tipp.getHomeGoalQtyTipp() > 0)) {
            if (!tipp.isWinWichTeamGoes()) {
                user.setWinWichTeamGoesQty(user.getWinWichTeamGoesQty() + 1);
            }
            tipp.setWinWichTeamGoes(true);
            winCount++;
        } else {
            if (tipp.isWinWichTeamGoes()) {
                user.setWinWichTeamGoesQty(user.getWinWichTeamGoesQty() - 1);
            }
            tipp.setWinWichTeamGoes(false);
        }

        tipp.setFinalPoints(winCount);

        if (user.getWinNum() != null) {
            user.setWinNum(user.getWinNum() + winCount);
        } else {
            user.setWinNum(winCount);
        }
        return user;
    }


    public List<Match> getAll() {
        return matchRepository.findAll();
    }


    public List<MatchDto> getAllWithStatisticsToday(Principal principal) {

        //Sima sorrend idő szerint
        //Ha meccs már elkezdődött akkor külön
        //Majd a végére a lezártak

        User user = null;
        if (principal != null) {
            user = userService.findByUsername(principal.getName());
        }
        List<MatchDto> matchDtos = new ArrayList<>();
        for (Match match : matchRepository.TodayMatches(new DateTime().toString("YYYY-MM-dd HH:mm:ss"))) {
            MatchDto matchDto = new MatchDto();
            matchDto = matchMapper.toMatchDto(match, matchDto);
            matchDto.setMatchReflink(match.getMatchReflink());
            matchDto.setCloseTime(matchDto.getMatchTime().plusMinutes(135));
            matchDto.setMatchHVDDto(matchTippStatisticsService.getMatchStatistics(match, user, match.getId(), tippService.getMatchTipps(match.getId())));
            if (user != null)
                matchDto.getMatchHVDDto().setVoted(tippService.getResultById(new TippId(user, match)));
            matchDtos.add(matchDto);
        }

        List<MatchDto> listSum = new ArrayList<>();

        LocalDateTime now = LocalDateTime.now();

        //Aktivok
        listSum.addAll(
                matchDtos.stream().filter(matchDto ->
                        !now.isAfter(matchDto.getMatchTime())
                ).collect(Collectors.toList())
        );

        //Elkezdődött
        listSum.addAll(
                matchDtos.stream().filter(matchDto ->
                        now.isAfter(matchDto.getMatchTime()) && now.isBefore(matchDto.getMatchTime().plusMinutes(135))
                ).collect(Collectors.toList())
        );

        //Végetért
        listSum.addAll(
                matchDtos.stream().filter(matchDto ->

                        now.isAfter(matchDto.getMatchTime().plusMinutes(135))

                ).collect(Collectors.toList())
        );


        return listSum;
    }

    public List<MatchDto> getAllWithStatisticsYesterday(Principal principal) {
        List<MatchDto> matchDtos = new ArrayList<>();
        for (Match match : matchRepository.YesterdayMatches(new DateTime().minusDays(1).toString("YYYY-MM-dd HH:mm:ss"))) {
            MatchDto matchDto = new MatchDto();
            User user = null;
            if (principal != null) {
                user = userService.findByUsername(principal.getName());
            }
            matchDto = matchMapper.toMatchDto(match, matchDto);
            matchDto.setMatchHVDDto(matchTippStatisticsService.getMatchStatistics(match, user, match.getId(), tippService.getMatchTipps(match.getId())));
            matchDtos.add(matchDto);
        }
        return matchDtos;
    }

    public List<MatchDto> getAllWithStatisticsTomorrow(Principal principal) {
        List<MatchDto> matchDtos = new ArrayList<>();
        for (Match match : matchRepository.TomorrowMatches(new DateTime().plusDays(1).toString("YYYY-MM-dd HH:mm:ss"))) {
            MatchDto matchDto = new MatchDto();
            User user = null;
            if (principal != null) {
                user = userService.findByUsername(principal.getName());
            }
            matchDto = matchMapper.toMatchDto(match, matchDto);
            matchDto.setMatchReflink(match.getMatchReflink());
            matchDto.setMatchHVDDto(matchTippStatisticsService.getMatchStatistics(match, user, match.getId(), tippService.getMatchTipps(match.getId())));
            matchDtos.add(matchDto);
        }
        return matchDtos;
    }


    //    @Scheduled(cron = "0 0 8 * * *")
    public void sendTomorrowTips() {
        List<User> list = userService.findAllByActiveAndEmailNotification(true, true);

        try {
            List<Match> matches = matchRepository.TomorrowMatches(new DateTime().plusDays(1).toString("YYYY-MM-dd HH:mm:ss"));
            if (list.size() > 0 && matches.size() > 0) {
                mailService.sendTomorrowTips(matches,
                        list.stream()
                                .map(user -> user.getEmail())
                                .collect(Collectors.toList())
                );
            }
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Scheduled(fixedRate = 300000)
    public void sendTippsWithStatisticForVip() {
        List<User> list = userService.findAllByEmailNotification(true);
        List<Match> matches = matchRepository.findAllBySentAndMatchTimeGreaterThanEqualAndMatchTimeLessThanEqual(false, LocalDateTime.now().plusHours(1), LocalDateTime.now().plusHours(1).plusMinutes(15));
        try {
            if (list.size() > 0 && matches.size() > 0) {
                mailService.sendMatchesWithStatistic(
                        matches.stream()
                                .map(match -> MatchWithStatisticDto.builder()
                                        .match(match)
                                        .matchHVDDto(matchTippStatisticsService.getMatchStatistics(match, null, match.getId(), tippService.getMatchTipps(match.getId())))
                                        .build())
                                .collect(Collectors.toList()),
                        list.stream()
                                .map(user -> user.getEmail()).collect(Collectors.toList())
                );
                matches.stream()
                        .forEach(match -> match.setSent(true));
                matchRepository.saveAll(matches);
            }
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Scheduled(fixedRate = 60000)
    public void sendNotification() {
        List<Match> matches = matchRepository.findAllBySentNotificationAndMatchTimeGreaterThanEqualAndMatchTimeLessThanEqual(false, LocalDateTime.now().plusMinutes(9), LocalDateTime.now().plusMinutes(13));

        if (matches.size() > 0) {

            matches.stream()
                    .forEach(match -> {
                        notificationService.save(Notification.builder()
                                .sendTime(LocalDateTime.now())
                                .user(userService.FindByUserName("admin"))
                                .title("Hamarosan kezdődik a mérkőzés")
                                .message(match.getHomeTeam().getName() + " - " + match.getVisitorTeam().getName())
                                .build(), match.getId());
                        match.setSentNotification(true);
                    });
            matchRepository.saveAll(matches);
        }

    }

    public void calculatePointsVegyebe(Match match) {
        matchTipps = tippService.getMatchTipps(match.getId());
        for (Tipp tipp : matchTipps) {
            boolean f = false;
            User user = pointsWinCalculateVegyebe(tipp.getTippId().getUser(), tipp, match);
            user.setWinRate((user.getWinNum() * 100) / (user.getTipNum()));
            if (user.getTipNum() > 199) {
                user.setBoostedWinRate(user.getWinRate() + 2);
            } else if (user.getTipNum() > 499) {
                user.setBoostedWinRate(user.getWinRate() + 4);
            } else if (user.getTipNum() > 999) {
                user.setBoostedWinRate(user.getWinRate() + 6);
            } else {
                user.setBoostedWinRate(user.getWinRate());
            }

            int winrateBoost = user.getClosedTips() == 0 ? 0 : (user.getWinWichTeamGoesQty() * 2 + user.getWinNumberOfGolsQty() + user.getWinFinalResultQty() * 20 + user.getWinWinningTeamQty() * 10) / user.getClosedTips();

            user.setBoostedWinRate(user.getBoostedWinRate() + winrateBoost);

            if (tippService.countUserTippsForCurrentMonth(user) >= 60) {
                user.setBoostedWinRate(user.getBoostedWinRate() + 1);
                f = true;
            }
            if (tippService.countUserTippDistinctDays(user) >= 20) {
                user.setBoostedWinRate(user.getBoostedWinRate() + 1);
                if (f) {

                    List<Tipp> listTipp = tippService.getCurrentMonthTipps(tipp.getTippId().getUser());

                    AtomicDouble sum = new AtomicDouble(0);

                    AtomicInteger winCounter = new AtomicInteger(0);

                    listTipp.forEach(tipp1 -> {

                        if (tipp.isWinWinningTeam()) {

                            winCounter.incrementAndGet();
                            switch (tipp.getResultTip()) {
                                case H:
                                    sum.addAndGet(tipp.getTippId().getMatchId().getOddsH());
                                    break;
                                case D:
                                    sum.addAndGet(tipp.getTippId().getMatchId().getOddsD());
                                    break;
                                case V:
                                    sum.addAndGet(tipp.getTippId().getMatchId().getOddsV());
                                    break;
                            }
                        }
                    });

                    user.setBoostedWinRate(user.getBoostedWinRate() + (int) Math.round(sum.get() / winCounter.get()));
                }
            }

            userService.save(user);
        }
    }

    private User pointsWinCalculateVegyebe(User user, Tipp tipp, Match match) {
        if (tipp.getFinalPoints() != null) {
            user.setWinNum(user.getWinNum() - tipp.getFinalPoints());
        }

        winCount = 0;
        //hazai vendég Döntetlen
        if (tipp.getResultTip() != null) {
            if (tipp.getResultTip().equals(match.getResult())) {
                winCount++;
                if (!tipp.isWinWinningTeam()) {
                    user.setWinWinningTeamQty(user.getWinWinningTeamQty() + 1);
                }
                tipp.setWinWinningTeam(true);

            } else {
                if (tipp.isWinWinningTeam()) {
                    user.setWinWinningTeamQty(user.getWinWinningTeamQty() - 1);
                }
                tipp.setWinWinningTeam(false);
            }
        }
        //hazai/vendég gól
        if (tipp.getHomeGoalQtyTipp().equals(match.getHomeGoalQty()) && tipp.getVisitorGoalQtyTipp().equals(match.getVisitorGoalQty())) {
            winCount++;
            if (!tipp.isWinFinalResult()) {
                user.setWinFinalResultQty(user.getWinFinalResultQty() + 1);
            }
            tipp.setWinFinalResult(true);
        } else {
            if (tipp.isWinFinalResult()) {
                user.setWinFinalResultQty(user.getWinFinalResultQty() - 1);
            }
            tipp.setWinFinalResult(false);
        }
        //gólok száma
        if (tipp.getGoalQty() == (match.getVisitorGoalQty() + match.getHomeGoalQty())) {
            winCount++;
            if (!tipp.isWinNumberOfGols()) {
                user.setWinNumberOfGolsQty(user.getWinNumberOfGolsQty() + 1);
            }
            tipp.setWinNumberOfGols(true);
        } else {
            if (tipp.isWinNumberOfGols()) {
                user.setWinNumberOfGolsQty(user.getWinNumberOfGolsQty() - 1);
            }
            tipp.setWinNumberOfGols(false);
        }


        if ((match.getHomeGoalQty() > 0 && match.getVisitorGoalQty() > 0) == (tipp.getVisitorGoalQtyTipp() > 0 && tipp.getHomeGoalQtyTipp() > 0)) {
            user.setWinWichTeamGoesQty(user.getWinWichTeamGoesQty() + 1);

            tipp.setWinWichTeamGoes(true);
            winCount++;
        } else {
            tipp.setWinWichTeamGoes(false);
        }

        tipp.setFinalPoints(winCount);
        tippService.save(tipp);
        if (user.getWinNum() != null) {
            user.setWinNum(user.getWinNum() + winCount);
        } else {
            user.setWinNum(winCount);
        }
        return user;
    }

    public Integer countByResultIsNotNull() {
        return matchRepository.countByResultIsNotNull();

    }

    public Integer countWonMatches() {
        return matchRepository.countWonMatches();
    }

    public Integer countFinalGoalForHomeAndVisitor()
    {
        return matchRepository.countFinalGoalForHomeAndVisitor();
    }
    public Integer countWonGoalSum()
    {
        return   matchRepository.countWonGoalSum();
    }
    public Integer countWonGG()
    {
        return  matchRepository.countWonGG();
    }

}

