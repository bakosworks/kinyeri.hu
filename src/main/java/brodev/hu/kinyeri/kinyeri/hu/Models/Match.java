
package brodev.hu.kinyeri.kinyeri.hu.Models;

import brodev.hu.kinyeri.kinyeri.hu.Dto.MatchHVDDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.MatchResult;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "matches")
@Data
public class Match {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "home_team")
    private Team homeTeam;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "visitor_team")
    private Team visitorTeam;

    @Column(name = "odds_h")
    private Float oddsH;

    @Column(name = "odds_v")
    private Float oddsV;

    @Column(name = "odds_d")
    private Float oddsD;

    @Column(name = "home_goal_qty")
    private Integer homeGoalQty;

    @Column(name = "visitor_goal_qty")
    private Integer visitorGoalQty;

    @Column(name = "sent")
    private Boolean sent;

    @Column(name = "sent_notification")
    private Boolean sentNotification;

    @Enumerated(EnumType.STRING)
    @Column(name = "result")
    private MatchResult result;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "Europe/Budapest")
    @Column(name = "match_time")
    private LocalDateTime matchTime;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "Europe/Budapest")
    @Column(name = "insert_time")
    private LocalDateTime insertTime;

    @Column(name = "match_reflink")
    private String matchReflink;

    @Column(name = "goal_over")
    private Double goalOver = 0.;
    @Column(name = "goal_tipp_home")
    private Long goalTippHome = 0l;
    @Column(name = "goal_tipp_visitor")
    private Long goalTippVisitor = 0l;
    @Column(name = "max_win_rate")
    private Long maxWinRate = 0l;
    @Column(name = "draw")
    private Long draw = 0l;
    @Column(name = "visitor")
    private Long visitor = 0l;
    @Column(name = "home")
    private Long home = 0l;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "unibet_odds")
    private UnibetOdds unibetOdds;
}

