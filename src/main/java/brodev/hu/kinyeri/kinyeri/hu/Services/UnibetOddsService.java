package brodev.hu.kinyeri.kinyeri.hu.Services;

import brodev.hu.kinyeri.kinyeri.hu.Models.UnibetOdds;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.UnibetOddsRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UnibetOddsService {

    private final UnibetOddsRepository unibetOddsRepository;

    public UnibetOdds save(UnibetOdds unibetOdds) {
        return unibetOddsRepository.save(unibetOdds);
    }
}
