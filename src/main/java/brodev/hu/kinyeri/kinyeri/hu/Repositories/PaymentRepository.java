package brodev.hu.kinyeri.kinyeri.hu.Repositories;

import brodev.hu.kinyeri.kinyeri.hu.Models.Match;
import brodev.hu.kinyeri.kinyeri.hu.Models.Payment;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.PaymentStatus;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentRepository extends JpaRepository<Payment,Long>, DataTablesRepository<Payment,Long > {
    List<Payment> findAllByUserId(User user);

    List<Payment> findAllByUserIdAndStatus(User user, PaymentStatus inProcess);

    boolean existsByUserId(User user);

    @Query("SELECT count(p.id) FROM Payment p WHERE p.userId.refUser.id = ?1 and p.activateTime != null")
    Long countOfSuccesPaymentByRefUser(Long userId);

    @Query("SELECT count(DISTINCT p.userId) FROM Payment p WHERE p.userId.refUser.id = ?1 and p.activateTime != null")
    Long countOfPaidUser(Long userId);
}
