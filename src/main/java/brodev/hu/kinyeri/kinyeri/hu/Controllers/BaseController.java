package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Services.BaseService;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.PostMapping;
import javax.validation.Valid;


public class BaseController<E,S extends BaseService>  {

    protected S service;

    public BaseController(S s) {
        this.service = s;
    }

    @PostMapping(value = "/list")
    public DataTablesOutput<E> list(@Valid DataTablesInput dataTablesInput)
    {
       return service.list(dataTablesInput);
    }

}
