package brodev.hu.kinyeri.kinyeri.hu.Services;

import brodev.hu.kinyeri.kinyeri.hu.Dto.MatchGoolCalculator;
import brodev.hu.kinyeri.kinyeri.hu.Dto.MatchHVDDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.Match;
import brodev.hu.kinyeri.kinyeri.hu.Models.Tipp;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.MatchResult;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class MatchTippStatisticsService {

    @Autowired
    @Lazy
    private  TippService tippService;

    @Autowired
    @Lazy
    private MatchRepository matchRepository;


    public MatchHVDDto calculateHVDStatistic(Match match, User user, Long id) {


        MatchHVDDto matchHVDDto=new MatchHVDDto();
        List<Long> list = setDatas(user, id, tippService.getMatchTipps(id), matchHVDDto,match);

        return setResultDatas(match, matchHVDDto, list);
    }
    public MatchHVDDto getMatchStatistics(Match match, User user, Long id, List<Tipp> tippList) {


        MatchHVDDto matchHVDDto=new MatchHVDDto();
        matchHVDDto.setMatchId(match.getId());
        List<Long> list=Arrays.asList(match.getHome(), match.getDraw(), match.getVisitor());
        matchHVDDto.setGoalOver(match.getGoalTippVisitor().doubleValue()+match.getGoalTippHome().doubleValue());
        matchHVDDto.setGoalTippVisitor(match.getGoalTippVisitor());
        matchHVDDto.setGoalTippHome(match.getGoalTippHome());
        matchHVDDto.setMaxWinRate(match.getMaxWinRate());
        matchHVDDto.setHome(match.getHome());
        matchHVDDto.setDraw(match.getDraw());
        matchHVDDto.setVisitor(match.getVisitor());
        return setResultDatas(match, matchHVDDto, list);
    }

    private MatchHVDDto setResultDatas(Match match, MatchHVDDto matchHVDDto, List<Long> list) {

        if(match.getUnibetOdds()!=null)
        {

            matchHVDDto.setBothTeamNo(match.getUnibetOdds().getBothTeamNo() == null? "" : match.getUnibetOdds().getBothTeamNo().toString().length()== 3 ? match.getUnibetOdds().getBothTeamNo()+"0": match.getUnibetOdds().getBothTeamNo()+"");
            matchHVDDto.setBothTeamYes(match.getUnibetOdds().getBothTeamYes() == null? "" : match.getUnibetOdds().getBothTeamYes().toString().length()== 3 ? match.getUnibetOdds().getBothTeamYes()+"0": match.getUnibetOdds().getBothTeamYes()+"");
            matchHVDDto.setOver1500(match.getUnibetOdds().getOver1500() == null? "" : match.getUnibetOdds().getOver1500().toString().length()== 3 ? match.getUnibetOdds().getOver1500()+"0": match.getUnibetOdds().getOver1500()+"");
            matchHVDDto.setOver2500(match.getUnibetOdds().getOver2500() == null? "": match.getUnibetOdds().getOver2500().toString().length()== 3 ? match.getUnibetOdds().getOver2500()+"0": match.getUnibetOdds().getOver2500()+"");
            matchHVDDto.setOver3500(match.getUnibetOdds().getOver3500() == null? "": match.getUnibetOdds().getOver3500().toString().length()== 3 ? match.getUnibetOdds().getOver3500()+"0": match.getUnibetOdds().getOver3500()+"");
            matchHVDDto.setOver4500(match.getUnibetOdds().getOver4500() == null? "": match.getUnibetOdds().getOver4500().toString().length()== 3 ? match.getUnibetOdds().getOver4500()+"0": match.getUnibetOdds().getOver4500()+"");
            matchHVDDto.setOver5500(match.getUnibetOdds().getOver5500() == null? "": match.getUnibetOdds().getOver5500().toString().length()== 3 ? match.getUnibetOdds().getOver5500()+"0": match.getUnibetOdds().getOver5500()+"");
            matchHVDDto.setOver6500(match.getUnibetOdds().getOver6500() == null? "": match.getUnibetOdds().getOver6500().toString().length()== 3 ? match.getUnibetOdds().getOver6500()+"0": match.getUnibetOdds().getOver6500()+"");
            matchHVDDto.setUnder1500(match.getUnibetOdds().getUnder1500() == null? "": match.getUnibetOdds().getUnder1500().toString().length()== 3 ? match.getUnibetOdds().getUnder1500()+"0": match.getUnibetOdds().getUnder1500()+"");
            matchHVDDto.setUnder2500(match.getUnibetOdds().getUnder2500() == null? "": match.getUnibetOdds().getUnder2500().toString().length()== 3 ? match.getUnibetOdds().getUnder2500()+"0": match.getUnibetOdds().getUnder2500()+"");

        }

        if (match.getResult()!=null){

            List<Long> finalList = list;
            list=list.stream()
                    .filter(e -> Collections.frequency(finalList,e)>1)
                    .distinct()
                    .collect(Collectors.toList());

            //3
            int goalSum = matchHVDDto.getGoalOver().intValue();
            int goalResult = match.getHomeGoalQty()+match.getVisitorGoalQty();

            if(goalSum==0 && goalResult<1.5)
            {
                matchHVDDto.setGoalOverWon(true);
            }
            else if(goalSum==1 && goalResult<2.5)
            {
                matchHVDDto.setGoalOverWon(true);
            }
            else if(goalSum==2 && goalResult<2.5)
            {
                matchHVDDto.setGoalOverWon(true);
            }
            else if(goalSum==3 && goalResult>2.5)
            {
                matchHVDDto.setGoalOverWon(true);
            }
            else if(goalSum==4 && goalResult>3.5)
            {
                matchHVDDto.setGoalOverWon(true);
            }
            else if(goalSum==5 && goalResult>3.5)
            {
                matchHVDDto.setGoalOverWon(true);
            }
            else if(goalSum==6 && goalResult>4.5)
            {
                matchHVDDto.setGoalOverWon(true);
            }
            else if(goalSum==7 && goalResult>5.5)
            {
                matchHVDDto.setGoalOverWon(true);
            }
            else if(goalSum>=8 && goalResult>6.5) {
                matchHVDDto.setGoalOverWon(true);
            }
            else
            {
                matchHVDDto.setGoalOverWon(false);
            }

          //  matchHVDDto.setGoalOverWon(matchHVDDto.getGoalOver()<match.getHomeGoalQty()+match.getVisitorGoalQty());

            if (list.size()>0){
                matchHVDDto.setMatchResultWon(match.getResult());
            }else {
                if (matchHVDDto.getMaxWinRate().equals(matchHVDDto.getHome())) {
                    matchHVDDto.setMatchResultWon(MatchResult.H);
                } else if (matchHVDDto.getMaxWinRate().equals(matchHVDDto.getVisitor())) {
                    matchHVDDto.setMatchResultWon(MatchResult.V);
                } else {
                    matchHVDDto.setMatchResultWon(MatchResult.D);
                }
            }

            if (!(matchHVDDto.getGoalTippHome()== 0 || matchHVDDto.getGoalTippVisitor()==0)){
                matchHVDDto.setBothTeamGoalWon(match.getHomeGoalQty()>0 && match.getVisitorGoalQty()>0);
            }else{
                matchHVDDto.setBothTeamGoalWon(match.getHomeGoalQty()==0 || match.getVisitorGoalQty()==0);
            }
        }
        return matchHVDDto;
    }

    private List<Long> setDatas(User user, Long id, List<Tipp> tippList, MatchHVDDto matchHVDDto, Match match) {

        MatchGoolCalculator matchGoolCalculator = new MatchGoolCalculator();

        matchHVDDto.setMatchId(id);

        for(Tipp tipp : tippList){
            if (tipp.getTippId().getUser().equals(user)){
                matchHVDDto.setVoted(tipp.getResultTip());
            }
            if(tipp.getResultTip().equals(MatchResult.H)){
                calculateHPoints(tipp, matchHVDDto);
            }else if(tipp.getResultTip().equals(MatchResult.D)){
                calculateDPoints(tipp, matchHVDDto);
            }else if(tipp.getResultTip().equals(MatchResult.V)){
                calculateVPoints(tipp, matchHVDDto);
            }
            calculateGoolOver(tipp,matchGoolCalculator);
        }

        if (matchGoolCalculator.getGoolHomeCounter()!=0) {
            long d=Math.round(matchGoolCalculator.getGoolHomeSum() / matchGoolCalculator.getGoolHomeCounter());
            matchHVDDto.setGoalTippHome(d);
            match.setGoalTippHome(d);

        }else{
            matchHVDDto.setGoalTippHome(0l);
            match.setGoalTippHome(0l);
        }
        if (matchGoolCalculator.getGoolVisitorCounter()!=0) {
            long d=Math.round(matchGoolCalculator.getGoolVisitorSum() / matchGoolCalculator.getGoolVisitorCounter());
            matchHVDDto.setGoalTippVisitor(d);
            match.setGoalTippVisitor(d);

        }else{
            matchHVDDto.setGoalTippVisitor(0l);
            match.setGoalTippVisitor(0l);
        }

        Double sum=(double) matchHVDDto.getHome()+(double) matchHVDDto.getVisitor()+(double) matchHVDDto.getDraw();
        if (sum!=0){

            Double wrH= matchHVDDto.getHome()/sum;
            Double wrD= matchHVDDto.getDraw()/sum;
            Double wrV= matchHVDDto.getVisitor()/sum;
            long hWr=Math.round(wrH*100);
            long vWr=Math.round(wrV*100);
            long dWr=Math.round(wrD*100);
            matchHVDDto.setHome(hWr);
            match.setHome(hWr);
            matchHVDDto.setVisitor(vWr);
            match.setVisitor(vWr);
            matchHVDDto.setDraw(dWr);
            match.setDraw(dWr);

        }

        matchHVDDto.setOddsD(match.getOddsD().toString().length() == 3 ? match.getOddsD()+"0" : match.getOddsD()+"");
        matchHVDDto.setOddsV(match.getOddsV().toString().length() == 3 ? match.getOddsV()+"0" : match.getOddsV()+"");
        matchHVDDto.setOddsH(match.getOddsH().toString().length() == 3 ? match.getOddsH()+"0" : match.getOddsH()+"");

        List<Long> list=Arrays.asList(matchHVDDto.getHome(), matchHVDDto.getDraw(), matchHVDDto.getVisitor());
        matchHVDDto.setMaxWinRate(Collections.max(list));
        match.setMaxWinRate(Collections.max(list));

        if (matchHVDDto.getGoalTippVisitor().equals(matchHVDDto.getGoalTippHome()) && !matchHVDDto.getMaxWinRate().equals(matchHVDDto.getDraw())){
            if (matchHVDDto.getHome()> matchHVDDto.getVisitor()){
                matchHVDDto.setGoalTippHome(matchHVDDto.getGoalTippHome()+1);
                match.setGoalTippHome(matchHVDDto.getGoalTippHome()+1);
            }else{
                matchHVDDto.setGoalTippVisitor(matchHVDDto.getGoalTippVisitor()+1);
                match.setGoalTippVisitor(matchHVDDto.getGoalTippVisitor()+1);
            }
        }

        if(matchHVDDto.getMaxWinRate().equals(matchHVDDto.getDraw()) && !(matchHVDDto.getGoalTippVisitor().equals(matchHVDDto.getGoalTippHome())))
        {
            if(matchHVDDto.getGoalTippVisitor()> matchHVDDto.getGoalTippHome())
            {
                long d=matchHVDDto.getGoalTippHome()+(matchHVDDto.getGoalTippVisitor()- matchHVDDto.getGoalTippHome());
                matchHVDDto.setGoalTippHome(d);
                match.setGoalTippHome(d);
            }
            else if(matchHVDDto.getGoalTippVisitor()< matchHVDDto.getGoalTippHome())
            {
                long d=matchHVDDto.getGoalTippVisitor()+(matchHVDDto.getGoalTippHome()- matchHVDDto.getGoalTippVisitor());
                matchHVDDto.setGoalTippVisitor(d);
                match.setGoalTippVisitor(d);
            }

            if(matchHVDDto.getGoalTippVisitor()>1 && matchHVDDto.getGoalTippHome()>1 && (matchHVDDto.getGoalTippHome().equals(matchHVDDto.getGoalTippVisitor())))
            {
                matchHVDDto.setGoalTippHome(matchHVDDto.getGoalTippHome()-1);
                match.setGoalTippHome(matchHVDDto.getGoalTippHome()-1);
                matchHVDDto.setGoalTippVisitor(matchHVDDto.getGoalTippVisitor()-1);
                match.setGoalTippVisitor(matchHVDDto.getGoalTippVisitor()-1);
            }
        }
        double d=(double) matchHVDDto.getGoalTippHome()+ matchHVDDto.getGoalTippVisitor();
        matchHVDDto.setGoalOver(d);
        match.setGoalOver(d);
        matchRepository.save(match);
        return list;
    }


    private void calculateGoolOver(Tipp tipp,MatchGoolCalculator matchGoolCalculator) {

        double winRate =tipp.getTippId().getUser().getWinNum() == 0?0: (double)tipp.getTippId().getUser().getBoostedWinRate()/100;
        int tippNumber=tipp.getTippId().getUser().getTipNum();
        if(tippNumber<=40){
            setNewStatistics(tipp, matchGoolCalculator,1);
        }else if(tippNumber<= 400) {

            if (winRate <= 0.2) {

            } else if (winRate <= 0.25) {
                setNewStatistics(tipp, matchGoolCalculator,0.05);
            } else if (winRate <= 0.3) {
                setNewStatistics(tipp, matchGoolCalculator,0.5);
            } else if (winRate <= 0.4) {
                setNewStatistics(tipp, matchGoolCalculator,5);
            } else if (winRate <= 0.6) {
                setNewStatistics(tipp, matchGoolCalculator,10);
            } else if (winRate > 0.6) {
                setNewStatistics(tipp, matchGoolCalculator,20);
            }
        }else if((tippNumber)<= 2000) {

            if (winRate <= 0.2) {

            } else if (winRate <= 0.25) {
                setNewStatistics(tipp, matchGoolCalculator,0.5);
            } else if (winRate <= 0.3) {
                setNewStatistics(tipp, matchGoolCalculator,1);
            } else if (winRate <= 0.35) {
                setNewStatistics(tipp, matchGoolCalculator,5);
            }else if (winRate <= 0.4) {
                setNewStatistics(tipp, matchGoolCalculator,20);
            }else if (winRate <= 0.45) {
                setNewStatistics(tipp, matchGoolCalculator,100+((Math.round((winRate-0.4)*100))*5));
            }else if (winRate <= 50) {
                setNewStatistics(tipp, matchGoolCalculator,300+((Math.round((winRate-0.45)*100))*10));
            }else if (winRate <= 55) {
                setNewStatistics(tipp, matchGoolCalculator,500+((Math.round((winRate-0.5)*100))*25));
            } else if (winRate <= 0.6) {
                setNewStatistics(tipp, matchGoolCalculator,1000+((Math.round((winRate-0.55)*100))*50));
            } else if (winRate > 0.6) {
                setNewStatistics(tipp, matchGoolCalculator,2000+((Math.round((winRate-0.6)*100))*100));
            }
        }else{

            if (winRate <= 0.4 && winRate>= 0.35) {
                setNewStatistics(tipp, matchGoolCalculator,40);
            }else if (winRate <= 0.45) {
                setNewStatistics(tipp, matchGoolCalculator,200+((Math.round((winRate-0.4)*100))*5));
            }else if (winRate <= 50) {
                setNewStatistics(tipp, matchGoolCalculator,450+((Math.round((winRate-0.45)*100))*10));
            }else if (winRate <= 55) {
                setNewStatistics(tipp, matchGoolCalculator,750+((Math.round((winRate-0.5)*100))*25));
            } else if (winRate <= 0.6) {
                setNewStatistics(tipp, matchGoolCalculator,1500+((Math.round((winRate-0.55)*100))*50));
            } else if (winRate > 0.6) {
                setNewStatistics(tipp, matchGoolCalculator,3000+((Math.round((winRate-0.6)*100))*100));
            }
        }

    }

    private void setNewStatistics(Tipp tipp, MatchGoolCalculator matchGoolCalculator, double d) {
        matchGoolCalculator.setGoolOverSum(matchGoolCalculator.getGoolOverSum() + (d * tipp.getGoalQty()*100));
        matchGoolCalculator.setGoolOverCounter(matchGoolCalculator.getGoolOverCounter() + d*100);
        matchGoolCalculator.setGoolHomeSum(matchGoolCalculator.getGoolHomeSum()+ (d * tipp.getHomeGoalQtyTipp()*100));
        matchGoolCalculator.setGoolHomeCounter(matchGoolCalculator.getGoolHomeCounter() + d*100);
        matchGoolCalculator.setGoolVisitorSum( matchGoolCalculator.getGoolVisitorSum()  + (d * tipp.getVisitorGoalQtyTipp()*100));
        matchGoolCalculator.setGoolVisitorCounter( matchGoolCalculator.getGoolVisitorCounter()+ d*100);
    }

    private void calculateVPoints(Tipp tipp,MatchHVDDto matchHVDDto) {
        matchHVDDto.setVisitor(matchHVDDto.getVisitor() + calculateHVDPoints(tipp).longValue());
    }


    private void calculateDPoints(Tipp tipp,MatchHVDDto matchHVDDto) {
        matchHVDDto.setDraw(matchHVDDto.getDraw() + calculateHVDPoints(tipp).longValue());
    }

    private void calculateHPoints(Tipp tipp,MatchHVDDto matchHVDDto) {
        matchHVDDto.setHome(matchHVDDto.getHome() + calculateHVDPoints(tipp).longValue());
    }

    private Double calculateHVDPoints(Tipp tipp) {
        Double count = 0.;
        double winRate =tipp.getTippId().getUser().getWinNum() == 0?0: (double)tipp.getTippId().getUser().getBoostedWinRate()/100;
        int tippNumber=tipp.getTippId().getUser().getTipNum();
        if(tippNumber<=40){
            count=count+1*100;
        }else if(tippNumber<= 400) {
            if (winRate <= 0.2) {
                count = 0.;
            } else if (winRate <= 0.25) {
                count = count+0.05*100;
            } else if (winRate <= 0.3) {
                count = count+0.5*100;
            } else if (winRate <= 0.4) {
                count = count+5*100;
            } else if (winRate <= 0.6) {
                count = count+10*100;
            } else if (winRate > 0.6) {
                count = count+20*100;
            }
        }else if((tippNumber)<= 2000) {
            if (winRate <= 0.2) {
                count = 0.;
            } else if (winRate <= 0.25) {
                count = count+0.5*100;
            } else if (winRate <= 0.3) {
                count = count+1*100;
            } else if (winRate <= 0.35) {
                count = count+5*100;
            }else if (winRate <= 0.4) {
                count = count+20*100;
            }else if (winRate <= 0.45) {
                count = count+(100+((Math.round((winRate-0.4)*100))*5))*100;
            }else if (winRate <= 50) {
                count = count+(300+((Math.round((winRate-0.45)*100))*10))*100;
            }else if (winRate <= 55) {
                count = count+(500+((Math.round((winRate-0.5)*100))*25))*100;
            } else if (winRate <= 0.6) {
                count = count+(1000+((Math.round((winRate-0.55)*100))*50))*100;
            } else if (winRate > 0.6) {
                count = count+(2000+((Math.round((winRate-0.6)*100))*100))*100;
            }
        }else{
            
            if (winRate <= 0.4 && winRate>= 0.35) {
                count = count+(40)*100;
            }else if (winRate <= 0.45) {
                count = count+(200+((Math.round((winRate-0.4)*100))*5))*100;
            }else if (winRate <= 50) {
                count = count+(450+((Math.round((winRate-0.45)*100))*10))*100;
            }else if (winRate <= 55) {
                count = count+(750+((Math.round((winRate-0.5)*100))*25))*100;
            } else if (winRate <= 0.6) {
                count = count+(1500+((Math.round((winRate-0.55)*100))*50))*100;
            } else if (winRate > 0.6) {
                count = count+(3000+((Math.round((winRate-0.6)*100))*100))*100;
            }
        }
        return count;

    }
}