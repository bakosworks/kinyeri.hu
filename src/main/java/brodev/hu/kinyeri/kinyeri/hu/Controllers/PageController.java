
package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Dto.UserDto;

import brodev.hu.kinyeri.kinyeri.hu.Models.PasswordResetToken;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.TopStatisticType;
import brodev.hu.kinyeri.kinyeri.hu.Services.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.Currency;
import java.util.Locale;

@Controller

public class PageController {

    @Value("${server.name}")
    private String serverName;

    private final UserService userService;
    private final RoleService roleService;
    private final PackageService packageService;
    private final PaymentService paymentService;
    private final TopStatisticService topStatisticService;
    private final BankDetailService bankDetailService;
    private final MatchService matchService;
    private final TippService tippService;
    private final PasswordResetTokenService passwordResetTokenService;

    public PageController(UserService userService, RoleService roleService, PackageService packageService, PaymentService paymentService, TopStatisticService topStatisticService, BankDetailService bankDetailService, MatchService matchService, TippService tippService, PasswordResetTokenService passwordResetTokenService) {
        this.userService = userService;
        this.roleService = roleService;
        this.packageService = packageService;
        this.paymentService = paymentService;
        this.topStatisticService = topStatisticService;
        this.bankDetailService = bankDetailService;
        this.matchService = matchService;
        this.tippService = tippService;
        this.passwordResetTokenService = passwordResetTokenService;
    }


    @RequestMapping(value={ "/login"}, method = RequestMethod.GET)
    public ModelAndView login(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }


    @RequestMapping(value="/registration", method = RequestMethod.GET)
    public ModelAndView registration(){
        ModelAndView modelAndView = new ModelAndView();
        UserDto userDto = new UserDto();
        modelAndView.addObject("userDto", userDto);
        modelAndView.setViewName("registration");
        return modelAndView;
    }

    @RequestMapping(value="/ranglista", method = RequestMethod.GET)
    public ModelAndView topRankings(){
        ModelAndView modelAndView = new ModelAndView();
        LocalDate convertedDate = LocalDate.now();
        convertedDate = convertedDate.withDayOfMonth(
                convertedDate.getMonth().length(convertedDate.isLeapYear()));
        modelAndView.setViewName("/page/rankings");

        Long userCounter = userService.getCurrentMonthActiveUsers();
        modelAndView.addObject("userCounter",userCounter);

        Double number =  (1000*userCounter)*0.7;

        modelAndView.addObject("lastDay",convertedDate);

        modelAndView.addObject("helpMoney",(NumberFormat.getNumberInstance(new Locale("hu", "HU")).format(number)).replaceAll(" ","."));

        return modelAndView;
    }

    @RequestMapping(value = {"/forgot-password"}, method =RequestMethod.GET)
    public String forgotPassword(HttpServletRequest request, Model model) {

        return "/page/forgot-password";
    }
    @RequestMapping(value = {"/premium"}, method =RequestMethod.GET)
    public String pricing(HttpServletRequest request, Model model,Principal principal) {
        model.addAttribute("currentUser",userService.findUserByUsername(principal.getName()));
        model.addAttribute("packages",packageService.findAllActive());
        model.addAttribute("bankDetail",bankDetailService.getOne());
        model.addAttribute("firstCustomer",paymentService.isFirstCustomerWithRefLink(principal));
        return "/page/pricing";
    }

    @GetMapping("/changePassword")
    public String showChangePasswordPage(Locale locale, Model model, @RequestParam("token") String token) {

        String result = passwordResetTokenService.validatePasswordResetToken(token);
        if(result == null)
        {
            PasswordResetToken passwordResetToken = passwordResetTokenService.getPasswordResetTokenByToken(token);
            if (passwordResetToken.getStatus()==0)
            {
                model.addAttribute("status",1);
                model.addAttribute("token",token);
            }
            else
            {
                model.addAttribute("status",2);
            }
        }
        else if (result.equals("invalidToken"))
        {
            model.addAttribute("status",3);
        }
        else if(result.equals("expired"))
        {
            model.addAttribute("status",4);
        }
        return "/page/change-password";
    }
    @GetMapping("/page/beallitasok")
    public String settings(Model model,Principal principal) {
        model.addAttribute("currentUser",userService.findUserByUsername(principal.getName()));
        model.addAttribute("bankDetail",bankDetailService.getOne());
        return "/page/settings";
    }
    @GetMapping("/szabalyzat")
    public String rules(Model model,Principal principal)
    {
        return "/page/rules";
    }

    @GetMapping("/meghivo/{reflink}")
    public String meghivo(Model model,Principal principal,@PathVariable("reflink") String reflink)
    {
        model.addAttribute("userReflink",userService.findByReflink(reflink));
        return "/page/reflink";
    }

    @GetMapping("/meghivo")
    public String meghivoDefault()
    {
        return  "redirect:/";
    }

    @GetMapping("/elozmenyek")
    public String elozmenyek(Model model)
    {
        model.addAttribute("last10Match",matchService.findAllByResultIsNotNull(PageRequest.of(0,20, Sort.Direction.DESC,"matchTime")));
        return  "/page/elozmenyek";
    }

    @GetMapping(value = "/affiliate" , name = "Page for affiliate information")
    public String affilate(Principal principal,Model model)
    {
        if(principal!=null)
        {
            try {
                User u = userService.findUserByUsername(principal.getName());

                model.addAttribute("tmpUsers",userService.countAllByRefUser(u));
                model.addAttribute("fullRegUser",userService.findAllUserByReflinkUserAndPasswordIsNot(u,"TMP"));

                model.addAttribute("user",u);
                model.addAttribute("countOfSuccesPayments",paymentService.countOfSuccesPaymentByRefUser(u.getId()));
                model.addAttribute("countOfSuccessPaidUser",paymentService.countOfPaidUser(u.getId()));
                model.addAttribute("serverName",serverName);
            }
            catch (Exception e)
            {
                return "redirdect:/index";
            }

        }

        return "/page/affiliate";


    }

    @GetMapping(value = "/statisztika",name = "Statistic page for Top1 user vs Kinyeri.hu")
    public String statisztika(Principal principal,Model model)
    {
        User user = userService.findTop10(TopStatisticType.ALL_TIME).get(0);
        long closedTippNumber = tippService.countUserTipps(user);

        model.addAttribute("merkozesnyertese",(user.getWinWinningTeamQty()*100)/((closedTippNumber)));
        model.addAttribute("gg", (user.getWinWichTeamGoesQty()*100)/((closedTippNumber)));
        model.addAttribute("golszam",(user.getWinNumberOfGolsQty()*100)/((closedTippNumber)));
        model.addAttribute("pontoskimenetel",(user.getWinFinalResultQty()*100)/((closedTippNumber)));
        model.addAttribute("user",user);

        Double pageTipNum = matchService.countByResultIsNotNull().doubleValue();
        Double pageMerkozesNyertese = ( matchService.countWonMatches() / pageTipNum) * 100;
        Double pagePontosKimenetel = (matchService.countFinalGoalForHomeAndVisitor() / pageTipNum ) * 100;
        Double pagePontosGolSzam = (matchService.countWonGoalSum() / pageTipNum) * 100;
        Double pageGG = (matchService.countWonGG() / pageTipNum) * 100;

        Integer x = matchService.countWonMatches();
        Integer y = matchService.countFinalGoalForHomeAndVisitor();
        Integer z = matchService.countWonGoalSum();
        Integer c = matchService.countWonGG();
        Double pageWinrate = ((x+y+z+c) / (pageTipNum*4)) * 100;

        model.addAttribute("pageTipNum",pageTipNum.intValue());
        model.addAttribute("pageMerkozesNyertese",pageMerkozesNyertese.intValue());
        model.addAttribute("pagePontosKimenetel",pagePontosKimenetel.intValue());
        model.addAttribute("pagePontosGolSzam",pagePontosGolSzam.intValue());
        model.addAttribute("pageGG",pageGG.intValue());
        model.addAttribute("winrate",pageWinrate.intValue());

        return "/page/statisztika";
    }


}

