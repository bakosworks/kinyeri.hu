package brodev.hu.kinyeri.kinyeri.hu.Services.Mail;

import brodev.hu.kinyeri.kinyeri.hu.Dto.MatchHVDDto;
import brodev.hu.kinyeri.kinyeri.hu.Dto.MatchWithStatisticDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.Match;
import brodev.hu.kinyeri.kinyeri.hu.Models.Reflink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.List;
import java.util.Map;

@Service
public class MailContentBuilder {

    private TemplateEngine templateEngine;

    @Autowired
    public MailContentBuilder(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public String sendForogtoPwHTML(String userName,String token,String serverName) {
        Context context = new Context();
        context.setVariable("token",token);
        context.setVariable("serverName", serverName);
        context.setVariable("userName", userName);
        return templateEngine.process("/mail/forgot-password-mail", context);
    }

    public String sendMatchWithStatistics(List<MatchWithStatisticDto> matchTipMap, Reflink reflink) {
        Context context = new Context();
        context.setVariable("matches", matchTipMap);
        context.setVariable("reflink", reflink);
        return templateEngine.process("/mail/match-with-statistics", context);
    }

    public String sendTomorrowTips(List<Match> matches) {
        Context context = new Context();
        context.setVariable("matches", matches);
        return templateEngine.process("/mail/tomorrow-matches", context);
    }
}
