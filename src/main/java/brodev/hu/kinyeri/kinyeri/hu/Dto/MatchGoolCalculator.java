package brodev.hu.kinyeri.kinyeri.hu.Dto;

import lombok.Data;

@Data
public class MatchGoolCalculator {
    Double goolOverCounter= 0.;
    Double goolOverSum = 0.;
    Double goolHomeCounter= 0.;
    Double goolHomeSum = 0.;
    Double goolVisitorCounter = 0.;
    Double goolVisitorSum = 0.;
}
