package brodev.hu.kinyeri.kinyeri.hu.Repositories;

import brodev.hu.kinyeri.kinyeri.hu.Models.AllTimeTopStatistic;
import brodev.hu.kinyeri.kinyeri.hu.Models.BaseTopStatistic;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

@NoRepositoryBean
public interface BaseTopStatisticsRepository<E extends BaseTopStatistic> extends JpaRepository<E,Long>, DataTablesRepository<E,Long> {

}
