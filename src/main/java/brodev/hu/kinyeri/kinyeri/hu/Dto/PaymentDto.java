package brodev.hu.kinyeri.kinyeri.hu.Dto;

import brodev.hu.kinyeri.kinyeri.hu.Models.Package;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.PaymentStatus;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Data
public class PaymentDto {

    private Long id;

    private LocalDateTime createdTime;

    private LocalDateTime activateTime;

    private String status;

    private Long packageId;

    private Long userId;
}
