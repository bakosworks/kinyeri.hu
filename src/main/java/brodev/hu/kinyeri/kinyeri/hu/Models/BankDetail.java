package brodev.hu.kinyeri.kinyeri.hu.Models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "bank_details")
@Data
public class BankDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "iban")
    private String iban;

    @Column(name = "bank_number")
    private String bankNumber;

    @Column(name = "swift_code")
    private String swiftCode;

    @Column(name = "bank_name")
    private String bankName;

    @Column(name = "phone")
    private String phone;
}

