package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Dto.TopStatisticDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.TopStatisticType;
import brodev.hu.kinyeri.kinyeri.hu.Services.TopStatisticService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/top-statistic")
public class TopStatisticController {

    private final TopStatisticService topStatisticService;

    @GetMapping("/get-statistic/{type}")
    public List<TopStatisticDto> topStatisticDtos(@PathVariable TopStatisticType type){
        return topStatisticService.findAll(type);
    }
}