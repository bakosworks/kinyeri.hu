
package brodev.hu.kinyeri.kinyeri.hu.Services;

import brodev.hu.kinyeri.kinyeri.hu.Dto.CommentDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.Comment;
import brodev.hu.kinyeri.kinyeri.hu.Models.Match;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.CommentRepository;
import lombok.AllArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CommentService {


    private final SimpMessagingTemplate brokerMessagingTemplate;

    private final CommentRepository commentRepository;

    private final UserService userService;

    private final MatchService matchService;

    public Comment newComment(Comment comment, Authentication authentication,String matchId) {
        User user=userService.FindByUserName(authentication.getName());
        if (user.getBanned().equals(true)){
            return null;
        }
        comment.setMatchId(matchService.findOneById(Long.parseLong(matchId)));
        comment.setUser(user);
        comment.setInsertTime(LocalDateTime.now());
        Comment cmnt=commentRepository.save(comment);
        return cmnt;
    }

    public CommentDto get10Comment(Long matchId, Long commentId) {
        Match match= matchService.get(matchId);
        long qty=commentRepository.countByMatchIdAndUser_BannedAndDeletedByIsNull(match,false);
        CommentDto commentDto=new CommentDto();
        commentDto.setCommentQty(qty);
        if (commentId==-111){
            commentDto.setComments(commentRepository.findTop10ByMatchIdAndUser_BannedAndDeletedByIsNullOrderByIdDesc(match,false));
        }else {
            commentDto.setComments(commentRepository.findTop10ByMatchIdAndIdLessThanAndUser_BannedAndDeletedByIsNullOrderByIdDesc(match, commentId,false));
        }
        return commentDto;
    }

    public void deleteComment(Long id, Principal principal) {
        commentRepository.findById(id).ifPresent(comment -> {
            User user=userService.FindByUserName(principal.getName());
            if (comment.getUser().equals(user) || user.getRoles().stream().anyMatch(role -> role.getRole().equals("MOD"))) {
                comment.setDeletedBy(user);
                comment.setDeleteTime(LocalDateTime.now());
                commentRepository.save(comment);
                brokerMessagingTemplate.convertAndSend("/topic/comment-delete", comment.getId() + ";" + comment.getMatchId().getId());
            }
        });
    }
}

