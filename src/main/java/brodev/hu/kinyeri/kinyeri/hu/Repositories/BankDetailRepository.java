package brodev.hu.kinyeri.kinyeri.hu.Repositories;

import brodev.hu.kinyeri.kinyeri.hu.Models.BankDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankDetailRepository extends JpaRepository<BankDetail,Long> {
}
