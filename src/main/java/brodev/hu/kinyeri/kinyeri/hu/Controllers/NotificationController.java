package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Dto.NotificationDto;
import brodev.hu.kinyeri.kinyeri.hu.Dto.TippDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Models.Notification;
import brodev.hu.kinyeri.kinyeri.hu.Models.Tipp;
import brodev.hu.kinyeri.kinyeri.hu.Services.NotificationService;
import brodev.hu.kinyeri.kinyeri.hu.Services.TippService;
import brodev.hu.kinyeri.kinyeri.hu.Services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/notification")
@AllArgsConstructor
public class NotificationController {

    private final NotificationService notificationService;

    private final UserService userService;

    @PostMapping(value = "/list", name = "Get all Notification to DataTable")
    public DataTablesOutput<Notification> getNotifications(@Valid @RequestBody DataTablesInput input) {

        return notificationService.findAll(input);
    }

    @PostMapping(value = "/add")
    public JsonResponse addNotification(@RequestBody NotificationDto notificationDTO, Principal principal)
    {
        Notification notification = new Notification();
        notification.setUser(userService.findUserByUsername(principal.getName()));
        notification.setMessage(notificationDTO.getMessage());
        notification.setTitle(notificationDTO.getTitle());
        notification.setSendTime(LocalDateTime.now());
        return notificationService.save(notification,null);
    }

}
