package brodev.hu.kinyeri.kinyeri.hu.Dto;

import lombok.Data;

@Data
public class RoleDto {
    private Long role_id;
    private String role;
}
