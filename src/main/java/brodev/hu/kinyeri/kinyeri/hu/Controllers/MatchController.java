package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Dto.*;
import brodev.hu.kinyeri.kinyeri.hu.Mapper.MatchMapper;
import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Models.Match;
import brodev.hu.kinyeri.kinyeri.hu.Models.Team;
import brodev.hu.kinyeri.kinyeri.hu.Models.UnibetOdds;
import brodev.hu.kinyeri.kinyeri.hu.Services.*;
import lombok.AllArgsConstructor;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/match")
@AllArgsConstructor
public class MatchController {
    private final MatchService matchService;

    private final MatchMapper matchMapper;

    private MatchTippStatisticsService matchTippStatisticsService;

    private final ReflinkService reflinkService;
    private final TeamService teamService;
    private final UnibetOddsService unibetOddsService;


    @PostMapping(value = "/list", name = "Get all Matches to DataTable")
    public DataTablesOutput<Match> getAllUser(@Valid @RequestBody DataTablesInput input) {
        return matchService.findAll(input);
    }

    @PostMapping(value = "/add", name ="Add a Match from DTO" )
    public JsonResponse addMatchByDTO(@RequestBody MatchDto matchDto){
        try {
            Match match = new Match();
            match = matchMapper.toMatch(matchDto, match);
            match.setMatchReflink(matchDto.getMatchReflink().trim().equals("") ? reflinkService.findOne().getMatchReflink() : matchDto.getMatchReflink());
            match.setSent(false);
            match.setSentNotification(false);
            matchService.save(match);
            return new JsonResponse("Sikeres mérkőzés mentés!","success");
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"error");
        }
    }

    @PostMapping(value = "/edit", name ="Edit a Match from DTO" )
    public JsonResponse editMatchByDTO(
            @RequestParam("id") Long id,
            @RequestParam("hteam") String editHTeam,
            @RequestParam("vteam") String editVTeam,
            @RequestParam("reflink") String reflink,
            @RequestParam("matchDate") String matchDate,
            @RequestParam("oddsH") Float editHOdds,
            @RequestParam("oddsD") Float editDOdds,
            @RequestParam("oddsV") Float editVOdds)
    {

        try {

            Match match = matchService.findById(id);

            Team foundHteam = teamService.findOne(editHTeam);
            Team foundVteam = teamService.findOne(editVTeam);

            match.setHomeTeam(foundHteam);
            match.setVisitorTeam(foundVteam);
            match.setOddsH(editHOdds);
            match.setOddsD(editDOdds);
            match.setOddsV(editVOdds);
            match.setMatchTime(LocalDateTime.parse(matchDate));

            match.setMatchReflink(reflink == "" ? reflinkService.findOne().getMatchReflink() : reflink);
            //match.setSent(matchService.get(match.getId()).getSent());
           // match.setSentNotification(matchService.get(match.getId()).getSentNotification());

           matchService.save(match);
           return new JsonResponse("Sikeres módosítás!","success");
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"error");
        }
    }

    @PostMapping(value = "/editResult", name ="Edit a MatchResult from DTO" )
    public JsonResponse editMatchResultByDTO(@RequestBody MatchDto matchDto){
        try {
            Match match = new Match();
            match= matchMapper.toMatch(matchDto, match);
            match=matchService.findById(match.getId());
            match.setMatchReflink(matchDto.getMatchReflink().trim().equals("") ? reflinkService.findOne().getMatchReflink() : matchDto.getMatchReflink());
            match.setResult(matchDto.getResult());
            match.setVisitorGoalQty(matchDto.getGoalVQty());
            match.setHomeGoalQty(matchDto.getGoalHQty());
            match=matchService.save(match);
            matchService.calculatePoints(match);
            return new JsonResponse("Sikeres számolás!","success");
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"error");
        }
    }



    @GetMapping(value = "/get", name ="Get a Team by Id")
    public MatchDto getMatchById(@RequestParam("id") Long id){
        MatchDto to = new MatchDto();
        Match from = matchService.get(id);
        to = matchMapper.toMatchDto(from,to);
        to.setMatchReflink(from.getMatchReflink());
        return to;


    }


    @GetMapping(value = "/calculate-matches")
    public String calcMatches(){
        matchService.getAll().forEach(match -> {
            matchTippStatisticsService.calculateHVDStatistic(match,null, match.getId());
        });
        return "fasza";


    }

    @GetMapping(value = "/findAllByResultIsNotNull")
    public HistoryDto findAll(@RequestParam("pageNumber") Integer pageNumber)
    {
        return matchService.findAllByResultIsNotNull(PageRequest.of(pageNumber+1,20, Sort.Direction.DESC,"matchTime"));
    }

    @DeleteMapping(value = "/delete/{id}", name="Delete a Match by Id")
    public JsonResponse deleteMatchById(@PathVariable("id") Long id) {
        return matchService.delete(id);
    }

    @PostMapping(value = "/close", name ="Close a Match from DTO" )
    public JsonResponse closeMatchByDTO(@RequestBody MatchDto matchDto){

            return new JsonResponse("számolási logikát megírni","error");
    }

    public Float fromOddsStringToFloat(String odds)
    {
        String[] numbers = odds.split("/");

        if(numbers.length==2)
        {
            Integer no1 = Integer.parseInt(numbers[0]);
            Integer no2 = Integer.parseInt(numbers[1]);

            Float actFloat = new Float((float) no1 / no2)+1;


            return (float) Math.round(actFloat * 100) / 100;

        }
        else
        {
            return null;
        }

    }

    @PostMapping(value = "/v2/add", name ="Edit a MatchResult from DTO" )
    public JsonResponse addv2(@RequestBody List<MatchV2> match) {

        DateTimeZone timeZone = DateTimeZone.forID( "Europe/Paris" );

        for(MatchV2 matchV2 : match)
        {
            Float homeOdds = fromOddsStringToFloat(matchV2.getMatchOffer().getHome());
            Float drawOdds = fromOddsStringToFloat(matchV2.getMatchOffer().getDraw());
            Float awayOdds = fromOddsStringToFloat(matchV2.getMatchOffer().getAway());

            UnibetOdds unibetOdds = new UnibetOdds();

            if(matchV2.getMatchOffer().getOver1500()!=null && matchV2.getMatchOffer().getOver1500()!="")
            {
                unibetOdds.setOver1500(fromOddsStringToFloat(matchV2.getMatchOffer().getOver1500()));
            }
            if(matchV2.getMatchOffer().getOver2500()!=null && matchV2.getMatchOffer().getOver2500()!="")
            {
                unibetOdds.setOver2500(fromOddsStringToFloat(matchV2.getMatchOffer().getOver2500()));
            }
            if(matchV2.getMatchOffer().getOver3500()!=null && matchV2.getMatchOffer().getOver3500()!="")
            {
                unibetOdds.setOver3500(fromOddsStringToFloat(matchV2.getMatchOffer().getOver3500()));
            }
            if(matchV2.getMatchOffer().getOver4500()!=null && matchV2.getMatchOffer().getOver4500()!="")
            {
                unibetOdds.setOver4500(fromOddsStringToFloat(matchV2.getMatchOffer().getOver4500()));
            }
            if(matchV2.getMatchOffer().getOver5500()!=null && matchV2.getMatchOffer().getOver5500()!="")
            {
                unibetOdds.setOver5500(fromOddsStringToFloat(matchV2.getMatchOffer().getOver5500()));
            }
            if(matchV2.getMatchOffer().getOver6500()!=null && matchV2.getMatchOffer().getOver6500()!="")
            {
                unibetOdds.setOver6500(fromOddsStringToFloat(matchV2.getMatchOffer().getOver6500()));
            }
            if(matchV2.getMatchOffer().getUnder1500()!=null && matchV2.getMatchOffer().getUnder1500()!="")
            {
                unibetOdds.setUnder1500(fromOddsStringToFloat(matchV2.getMatchOffer().getUnder1500()));
            }
            if(matchV2.getMatchOffer().getUnder2500()!=null && matchV2.getMatchOffer().getUnder2500()!="")
            {
                unibetOdds.setUnder2500(fromOddsStringToFloat(matchV2.getMatchOffer().getUnder2500()));
            }
            if(matchV2.getMatchOffer().getBothTeamHasGoalNo()!=null && matchV2.getMatchOffer().getBothTeamHasGoalNo()!="")
            {
                unibetOdds.setBothTeamNo(fromOddsStringToFloat(matchV2.getMatchOffer().getBothTeamHasGoalNo()));
            }
            if(matchV2.getMatchOffer().getBothTeamHasGoalYes()!=null && matchV2.getMatchOffer().getBothTeamHasGoalYes()!="")
            {
                unibetOdds.setBothTeamYes(fromOddsStringToFloat(matchV2.getMatchOffer().getBothTeamHasGoalYes()));
            }

            unibetOddsService.save(unibetOdds);

            if(homeOdds > 0 && drawOdds > 0 && awayOdds > 0)
            {

                Match genMatch = new Match();

                Team homeTeam = teamService.findOneOrCreate(matchV2.getHome());
                Team awayTeam = teamService.findOneOrCreate(matchV2.getAway());

                DateTime dateTime = new DateTime( matchV2.getStart(), timeZone );

                org.joda.time.LocalDateTime dtl = dateTime.toLocalDateTime();

                LocalDateTime newDateTime =  LocalDateTime.of(dtl.getYear(),dtl.getMonthOfYear(),dtl.getDayOfMonth(),dtl.getHourOfDay(),dtl.getMinuteOfHour(),dtl.getSecondOfMinute());

                genMatch.setMatchTime(newDateTime);
                genMatch.setHomeTeam(homeTeam);
                genMatch.setVisitorTeam(awayTeam);
                genMatch.setInsertTime(LocalDateTime.now());
                genMatch.setOddsH(homeOdds);
                genMatch.setOddsD(drawOdds);
                genMatch.setOddsV(awayOdds);
                genMatch.setSent(false);
                genMatch.setSentNotification(false);
                genMatch.setUnibetOdds(unibetOdds);

                matchService.save(genMatch);
            }

        }

        return new JsonResponse("success","success",match.size());
    }

}
