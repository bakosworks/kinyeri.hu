package brodev.hu.kinyeri.kinyeri.hu.Repositories;


import brodev.hu.kinyeri.kinyeri.hu.Models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByRole(String role);


}