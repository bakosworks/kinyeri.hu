package brodev.hu.kinyeri.kinyeri.hu.Services;

import brodev.hu.kinyeri.kinyeri.hu.Models.Role;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.RoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class RoleService {

    private final RoleRepository roleRepository;


    public List<Role> findAll() {
       return roleRepository.findAll();
    }

    public Role getVip() {
        return roleRepository.findByRole("VIP");
    }
}
