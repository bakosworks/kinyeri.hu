
package brodev.hu.kinyeri.kinyeri.hu.Services;


import brodev.hu.kinyeri.kinyeri.hu.Dto.UserDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.*;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.TopStatisticType;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.PasswordTokenRepository;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.RoleRepository;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.UserRepository;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Transactional
@Service
@Slf4j
public class UserService {


    private final SimpMessagingTemplate brokerMessagingTemplate;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private PasswordTokenRepository passwordTokenRepository;
    private TippService tippService;


    @Autowired
    public UserService(SimpMessagingTemplate brokerMessagingTemplate, UserRepository userRepository,
                       RoleRepository roleRepository,
                       BCryptPasswordEncoder bCryptPasswordEncoder,
                       PasswordTokenRepository passwordTokenRepository, @Lazy TippService tippService) {
        this.brokerMessagingTemplate = brokerMessagingTemplate;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.passwordTokenRepository = passwordTokenRepository;
        this.tippService=tippService;

    }


    public List<User> getUsersByListId(List<Long> list){
        return userRepository.findAllById(list);
    }

    public User FindByUserName(String userName) {
        return userRepository.findFirstByUsername(userName);
    }

    public User saveUser(UserDto userDto) {
        User user = new User();
        user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        user.setActive(true);
        user.setUsername(userDto.getUsername());
        user.setEmail(userDto.getEmail());
        user.setYearsOld(userDto.getYearsOld());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        Role userRole = roleRepository.findByRole("USER");
        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        user.setCountry(userDto.getCountry());
        user.setBanned(false);
        user.setCity(userDto.getCity());
        user.setZipCode(userDto.getZipCode());
        user.setAddress(userDto.getAddress());
        user.setEmailNotification(userDto.isEmailNotification());
        setRefCodeForUser(user);
        return userRepository.save(user);
    }

    public User saveUserFromReflink(UserDto userDto) {

        User user = findUserByEmail(userDto.getEmail());
        user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        user.setActive(true);
        user.setUsername(userDto.getUsername());
        user.setEmail(userDto.getEmail());
        user.setYearsOld(userDto.getYearsOld());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        Role userRole = roleRepository.findByRole("USER");
        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        user.setCountry(userDto.getCountry());
        user.setBanned(false);
        user.setCity(userDto.getCity());
        user.setZipCode(userDto.getZipCode());
        user.setAddress(userDto.getAddress());
        user.setEmailNotification(userDto.isEmailNotification());
        setRefCodeForUser(user);
        return userRepository.save(user);
    }

    public void save(User user) {
        userRepository.save(user);
    }

    public void saveUserWithRoles(User user) {
        if(!user.getPassword().equals("")) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        }
        else{
            user.setPassword(get(user.getId()).getPassword());
        }
        user.setActive(true);
        userRepository.save(user);
    }



    public User get(Long id) {
        if(userRepository.existsById(id)){
            return userRepository.getOne(id);
        }else{
            return new User();
        }

    }

    public ModelAndView registerValid(UserDto userDto) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("registration");
            modelAndView.addObject("userDto", userDto);
            if(userRepository.existsByUsername(userDto.getUsername())){
                modelAndView.addObject("username", "A felhasználónév foglalt.");
                return  modelAndView;
            }
            if(userDto.getUsername().contains(" "))
            {
                modelAndView.addObject("username", "Nem tartalmazhat szóközt.");
                return  modelAndView;
            }
            if(!userDto.getPassword().equals(userDto.getPassword2())){
                modelAndView.addObject("password", "A jelszavak nem egyeznek.");
                return  modelAndView;
            }
            if(userRepository.existsByEmail(userDto.getEmail())){
                modelAndView.addObject("email", "Az Email foglalt.");
                return  modelAndView;
            }

            try{
                saveUser(userDto);
            }catch(Exception e){

            }
            modelAndView.setViewName("login");
            return modelAndView;
        }

    public JsonResponse delete(Long id) {

        try{
            userRepository.deleteById(id);
            return new JsonResponse("Sikeres törlés", "success");
        }catch (DataIntegrityViolationException cve){
            return new JsonResponse("Ez a Felhasználó használatban van így nem törölhető!","error");
        }catch(EmptyResultDataAccessException e){
            return new JsonResponse("Ez a Felhasználó  már törölve van!","error");
        }catch(Exception e){
            return new JsonResponse("Sikertelen törlés"+e.getMessage(), "error");
        }

    }

    @JsonView(DataTablesOutput.View.class)
    public DataTablesOutput<User> findAll(DataTablesInput input)
    {
        return userRepository.findAll(input);
    }


    public boolean existByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    public boolean mailCountIsZero(String mail)
    {
        if(userRepository.countOfEmail(mail)==0)
        {
            return true;
        }
        else return false;
    }

    public Optional<User> findByEmail(String mail)
    {
       return userRepository.findByEmailOrderById(mail);
    }

    public JsonResponse existsByEmailAndPasswordIsNull(String email,Long id)
    {
        if (id==null){
            return new JsonResponse("Hiba mentés során!","error");
        }
        Optional<User> optional=userRepository.findById(id);

        if (!optional.isPresent()){
            return new JsonResponse("Hiba mentés során!","error");
        }


        Optional<User> foundUser = findByEmail(email);

        if(foundUser.isPresent())
        {
            if(foundUser.get().getPassword().equals("TMP"))
            {
                return new JsonResponse("success","success");
            }
            else
            {
                return new JsonResponse("Foglalt email cím!","error");
            }
        }
        else
        {
           User u = new User();
           u.setRefUser(optional.get());
           u.setEmail(email);
           u.setEmailNotification(false);
           u.setBanned(false);
           u.setPassword("TMP");
           u.setUsername(System.currentTimeMillis()+"TMP");
           u.setActive(false);
           save(u);
          return new JsonResponse("success","success");
        }


    }

    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void createPasswordResetTokenForUser(User user, String token) {

        PasswordResetToken myToken = new PasswordResetToken();
        myToken.setToken(token);
        myToken.setUser(user);
        myToken.setStatus(0);
        DateTime now = new DateTime();
        now = now.plusDays(2);
        Date killDate = now.toDate();
        myToken.setExpiryDate(killDate);
        passwordTokenRepository.save(myToken);
    }


    public void saveUserWithPassword(User user)
    {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    public Double getStrongPoints(User user) {
        double winRate =(double)user.getBoostedWinRate()/100;
        int tippNumber=user.getTipNum();
        if(tippNumber<=40){
            return 1.;
        }else if(tippNumber<= 400) {
            if (winRate <= 0.2) {
                return 0.;
            } else if (winRate <= 0.25) {
                return 0.05;
            } else if (winRate <= 0.3) {
                return 0.5;
            } else if (winRate <= 0.4) {
                return 5.;
            } else if (winRate <= 0.6) {
                return 10.;
            } else if (winRate > 0.6) {
                return 20.;
            }
        }else if((tippNumber)<= 2000) {
            if (winRate <= 0.2) {
                return 0.;
            } else if (winRate <= 0.25) {
                return 0.5;
            } else if (winRate <= 0.3) {
                return 1.;
            } else if (winRate <= 0.35) {
                return 5.;
            }else if (winRate <= 0.4) {
                return 20.;
            }else if (winRate <= 0.45) {
                return (100.+((Math.round((winRate-0.4)*100))*5));
            }else if (winRate <= 50) {
                return (300.+((Math.round((winRate-0.45)*100))*10));
            }else if (winRate <= 55) {
                return (500.+((Math.round((winRate-0.50)*100))*25));
            } else if (winRate <= 0.6) {
                return (1000.+((Math.round((winRate-0.55)*100))*50));
            } else if (winRate > 0.6) {
                return (2000.+((Math.round((winRate-0.6)*100))*100));
            }
        }else{
            if (winRate <= 0.4 && winRate>= 0.35) {
                return (40.);
            }else if (winRate <= 0.45) {
                return (200.+((Math.round((winRate-0.4)*100))*5));
            }else if (winRate <= 50) {
                return (450.+((Math.round((winRate-0.45)*100))*10));
            }else if (winRate <= 55) {
                return (750.+((Math.round((winRate-0.50)*100))*25));
            } else if (winRate <= 0.6) {
                return (1500.+((Math.round((winRate-0.55)*100))*50));
            } else if (winRate > 0.6) {
                return (3000.+((Math.round((winRate-0.6)*100))*100));
            }
        }
        return 0.;
    }




    public User findByUsername(String name) {
        return userRepository.findByUsername(name);
    }
    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

//    public UserDto userWinRateCalculate(User user) {
//        UserDto userDto= new UserDto();
//        userDto= modelMapper.map(user,UserDto.class);
//        userDto.setVisibleWirate((int) Math.floor(((double) user.getWinNum() / user.getTipNum())*100));
//        if(user.getTipNum()> 200){
//           userDto.setVisibleWirate(userDto.getVisibleWirate()+2);
//        }else if(user.getTipNum() > 500){
//            userDto.setVisibleWirate(userDto.getVisibleWirate()+4);
//
//        }else if(user.getTipNum()> 1000){
//            userDto.setVisibleWirate(userDto.getVisibleWirate()+6);
//        }
//        userDto.setBestSkill(userBestSkill(user));
//        return
//    }
//
//    private BestSkill userBestSkill(User user) {
//        WINNING_TEAM, FINAL_RESULT, NUMBER_OF_GOLS, WHICH_TEAM_GOES
//
//    }

    public User findUserByUsername(String name) {
        return userRepository.findByUsername(name);
    }


    public List<User> findAllList() {
        return userRepository.findAll();
    }

    public boolean existById(Long userId) {
        return userRepository.existsById(userId);
    }

    @Transactional
    public List<User> findTop10(TopStatisticType topStatisticType) {
        switch (topStatisticType) {
            case ACTIVE:
                return userRepository.findTop10ByLastTipGreaterThanEqualAndClosedTipsGreaterThanEqualOrderByBoostedWinRateDesc(LocalDate.now().atStartOfDay(), 50);
            case ALL_TIME:

                List<Long> nativeUsers = new ArrayList<>();

                LocalDate currrentDate = LocalDate.now();

                if(currrentDate.getDayOfMonth()<20)
                {
                    nativeUsers = userRepository.findTop10ForCurrentMonthRemoveUserNumberOfTipsLassThan3AndOrderByWinrateDescNumberOfTipsDesc(currrentDate.getMonthValue(),currrentDate.getYear());
                }
                else
                {
                    nativeUsers = userRepository.findTop10ForCurrentMonthRemoveUserNumberOfTipsLassThan3AndOrderByWinrateDescNumberOfTipsDescAfterDay20(currrentDate.getMonthValue(),currrentDate.getYear());
                }

                List<User> us = new ArrayList<>();

                for (Long i: nativeUsers) {
                    us.add(userRepository.findById(i).get());
                }

                return us;

                //return userRepository.findTop10ByLastTipGreaterThanEqualAndClosedTipsGreaterThanEqualOrderByBoostedWinRateDesc(LocalDate.now().atStartOfDay().minusDays(7),50);
            case WINNING_TEAM_ACTIVE:
                return userRepository.findTop10ByLastTipGreaterThanEqualAndClosedTipsGreaterThanEqualOrderByWinWinningTeamWinRateDesc(LocalDate.now().atStartOfDay(),50);
            case WINNING_TEAM_ALL_TIME:
                return userRepository.findTop10ByLastTipGreaterThanEqualAndClosedTipsGreaterThanEqualOrderByWinWinningTeamWinRateDesc(LocalDate.now().atStartOfDay().minusDays(7),50);
            case WHICH_TEAM_ACTIVE:
                return userRepository.findTop10ByLastTipGreaterThanEqualAndClosedTipsGreaterThanEqualOrderByWinWichTeamGoesWinRateDesc(LocalDate.now().atStartOfDay(),50);
            case WHICH_TEAM_ALL_TIME:
                return userRepository.findTop10ByLastTipGreaterThanEqualAndClosedTipsGreaterThanEqualOrderByWinWichTeamGoesWinRateDesc(LocalDate.now().atStartOfDay().minusDays(7),50);
            case FINAL_RESULT_ACTIVE:
                return userRepository.findTop10ByLastTipGreaterThanEqualAndClosedTipsGreaterThanEqualOrderByWinFinalResultWinRateDesc(LocalDate.now().atStartOfDay(),50);
            case FINAL_RESULT_ALL_TIME:
                return userRepository.findTop10ByLastTipGreaterThanEqualAndClosedTipsGreaterThanEqualOrderByWinFinalResultWinRateDesc(LocalDate.now().atStartOfDay().minusDays(7),50);
            case NUMBER_OF_GOALS_ACTIVE:
                return userRepository.findTop10ByLastTipGreaterThanEqualAndClosedTipsGreaterThanEqualOrderByWinNumberOfGolsWinRateDesc(LocalDate.now().atStartOfDay(),50);
            case NUMBER_OF_GOALS_ALL_TIME:
                return userRepository.findTop10ByLastTipGreaterThanEqualAndClosedTipsGreaterThanEqualOrderByWinNumberOfGolsWinRateDesc(LocalDate.now().atStartOfDay().minusDays(7),50);
            default:
                return new ArrayList<>();
        }

    }
    /*
    public List<User> listUserForStatistic(Integer type)
    {
        if (type==0)
        {

        }
        else if (type==90)
        {

        }
        else
        {

        }
    }*/

    public List<User> findAllByActive(boolean b) {
        return userRepository.findAllByActive(b);
    }

    public List<User> findAllVip() {
        return userRepository.findAllByDaysLeftVipGreaterThan(0);
    }


    public List<User> findAll() {
        return userRepository.findAll();
    }


    public JsonResponse subscribe(String id) {
        User user = userRepository.findById(Long.parseLong(id)).get();
        user.setEmailNotification(true);
        userRepository.save(user);
        return new JsonResponse("Sikeresen feliratkoztál az email értesítésre!", "success");
    }

    public JsonResponse unSubscribe(String id) {
        User user = userRepository.findById(Long.parseLong(id)).get();
        user.setEmailNotification(false);
        userRepository.save(user);
        return new JsonResponse("Sikeresen leiratkoztál az email értesítésről!", "success");
    }

    public List<User> findAllByActiveAndEmailNotification(boolean b, boolean b1) {
        return userRepository.findAllByActiveAndEmailNotification(b,b1);
    }

    public List<User> findAllByEmailNotification(boolean b) {
        return userRepository.findAllByEmailNotification(b);
    }
    public JsonResponse unBanUser(Long userId) {
        Optional<User> userOptional=userRepository.findById(userId);
        if (userOptional.isPresent()){
            User user=userOptional.get();
            user.setBanned(false);
            userRepository.save(user);
            return new JsonResponse("Felhasználó ban visszavonva!","success");
        }else{
            return new JsonResponse("Nincs ilyen id-val user","error");
        }

    }
    public JsonResponse banUser(Long userId) {
        Optional<User> userOptional=userRepository.findById(userId);
        if (userOptional.isPresent()){
            User user=userOptional.get();
            user.setBanned(true);
            userRepository.save(user);
            brokerMessagingTemplate.convertAndSend("/topic/user-ban",user.getId());
            return new JsonResponse("Felhasználó bannolva!","success");
        }else{
            return new JsonResponse("Nincs ilyen id-val user","error");
        }
    }

    public long getCurrentMonthActiveUsers() {
        return userRepository.countDistinctByTippList_TippId_MatchId_MatchTimeIsGreaterThanAndTippList_TippId_MatchId_MatchTimeIsLessThan(
                LocalDateTime.now().withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0),
                LocalDateTime.now().withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0).plusMonths(1)
        );
    }
    public JsonResponse saveByReflinkReg(UserDto userDto,HttpServletRequest request)
    {
        if(!existByUsername(userDto.getUsername()))
        {
            if(userDto.getPassword().equals(userDto.getPassword2()))
            {
                if (userRepository.existsByEmailAndPassword(userDto.getEmail(),"TMP"))
                {
                    try {
                        User savedUser =  saveUserFromReflink(userDto);
                        request.login(savedUser.getUsername(), userDto.getPassword());
                        return new JsonResponse("success","success");
                    }
                    catch (Exception e)
                    {
                        log.error(e.getMessage());
                        return new JsonResponse("Hiba mentés során!","error");
                    }
                }
                else
                {
                    return new JsonResponse("Nincs regisztráció megkezdve!","error");
                }
            }
            else
            {
                return new JsonResponse("Két jelszó nem egyezik!","error");
            }

        }
        else
        {
            return new JsonResponse("Felhasználónév foglalt!","error");
        }
    }

    public Optional<User> findByReflink(String reflink) {
        return userRepository.findFirstByUsernameOrRefLink(reflink,reflink);
    }


    public Integer countAllByRefUser(User u)
    {
        return userRepository.countAllByRefUser(u);
    }
    public Integer findAllUserByReflinkUserAndPasswordIsNot(User u,String password)
    {
        return userRepository.countAllByRefUserAndPasswordIsNot(u,password);
    }

    public JsonResponse savePaymentAdresses(Principal principal, String skrill, String paypal) {
        User user=findByUsername(principal.getName());
        user.setSkrillAddress(skrill);
        user.setPaypalAddress(paypal);
        userRepository.save(user);
        return new JsonResponse("Sikeres mentés!","success");
    }

    public String generateRefLinkCodes() {
        List<User> users=userRepository.findAllByRefLinkIsNull();
        users.forEach(this::setRefCodeForUser);
        userRepository.saveAll(users);
        return "Sikeres";
    }

    private void setRefCodeForUser(User user) {
        String refLink=generateRefCode();
        while (userRepository.existsByRefLink(refLink)){
            refLink=generateRefCode();
        }
        user.setRefLink(refLink);
    }

    private String generateRefCode() {
        return rndChar()+""+rndChar()+""+rndChar()+""+rndChar()+""+rndNumber()+rndNumber()+rndNumber()+rndNumber();
    }

    private static char rndChar () {
        int rnd = (int) (Math.random() * 52);
        char base = (rnd < 26) ? 'A' : 'a';
        return (char) (base + rnd % 26);

    }
    private static int rndNumber () {
        return (int) (Math.random() * 9);

    }
}