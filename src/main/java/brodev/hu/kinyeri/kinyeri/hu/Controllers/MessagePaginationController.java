package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Models.Message;
import brodev.hu.kinyeri.kinyeri.hu.Services.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/messages")
public class MessagePaginationController {

    private final MessageService messageService;

    @GetMapping("/get-messages/{id}")
    public List<Message> getMessages(@PathVariable(name = "id") Long id){
        List<Message> messages=messageService.get50MessagesFromId(id);
        if (messages.size()<1){
            return null;
        }
        return messages;
    }
    @PutMapping("/delete/{id}")
    public void deleteComment(@PathVariable Long id, Principal principal){
        messageService.deleteComment(id,principal);
    }
}
