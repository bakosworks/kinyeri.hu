package brodev.hu.kinyeri.kinyeri.hu.Dto;

import brodev.hu.kinyeri.kinyeri.hu.Models.enums.MatchResult;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FaszDto {
    private MatchResult result;
    private Long wr;
}
