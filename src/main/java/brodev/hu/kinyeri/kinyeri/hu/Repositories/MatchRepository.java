
package brodev.hu.kinyeri.kinyeri.hu.Repositories;


import brodev.hu.kinyeri.kinyeri.hu.Models.Match;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.thymeleaf.processor.element.MatchingAttributeName;

import java.time.LocalDateTime;
import java.util.List;

public interface MatchRepository extends JpaRepository<Match, Long>,  DataTablesRepository<Match,Long > {


    @Query(nativeQuery = true, value = "SELECT * FROM matches where date(match_time) = date(?1) ORDER BY match_time ASC")
    List<Match> TodayMatches(String dateTime);

    @Query(nativeQuery = true, value = "SELECT * FROM matches where date(match_time) = date(?1) ORDER BY match_time ASC")
    List<Match> YesterdayMatches(String dateTime);

    @Query(nativeQuery = true, value = "SELECT * FROM matches where date(match_time) = date(?1) ORDER BY match_time ASC")
    List<Match> TomorrowMatches(String dateTime);

    List<Match> findAllByResultIsNotNull();

    List<Match> findAllBySentAndMatchTimeGreaterThanEqualAndMatchTimeLessThanEqual(boolean b, LocalDateTime from,LocalDateTime to);
    List<Match> findAllBySentNotificationAndMatchTimeGreaterThanEqualAndMatchTimeLessThanEqual(boolean b, LocalDateTime from,LocalDateTime to);

    Page<Match> findAllByResultIsNotNull (Pageable pageable);

    Integer countByResultIsNotNull();

    @Query(nativeQuery = true,value = "SELECT COUNT(id)  FROM `matches` where (goal_tipp_home>goal_tipp_visitor and result = 'H') or (goal_tipp_visitor>goal_tipp_home and result = 'V' ) or  (goal_tipp_visitor=goal_tipp_home and result = 'D')  and result is not null")
    Integer countWonMatches();

    @Query(nativeQuery = true,value = "SELECT COUNT(id) FROM `matches` where home_goal_qty = goal_tipp_home and visitor_goal_qty = goal_tipp_visitor and result is not null")
    Integer countFinalGoalForHomeAndVisitor();

    @Query(nativeQuery = true,value = "SELECT COUNT(id) FROM `matches` where (visitor_goal_qty+home_goal_qty) = (goal_tipp_visitor+goal_tipp_home)")
    Integer countWonGoalSum();

    @Query(nativeQuery = true,value = "SELECT Count(id) FROM `matches` where result is not null and \n" +
            "(goal_tipp_home>0 and goal_tipp_visitor>0 and home_goal_qty>0 and visitor_goal_qty>0) or\n" +
            "(goal_tipp_home=0 and goal_tipp_visitor=0 and home_goal_qty=0 and visitor_goal_qty=0) OR \n" +
            "(goal_tipp_home>0 and goal_tipp_visitor=0 and ((home_goal_qty>0 and visitor_goal_qty=0) or (visitor_goal_qty>0 and home_goal_qty=0))) OR\n" +
            "(goal_tipp_home=0 and goal_tipp_visitor>0 and ((home_goal_qty>0 and visitor_goal_qty=0) or (visitor_goal_qty>0 and home_goal_qty=0)))")
    Integer countWonGG();
}

