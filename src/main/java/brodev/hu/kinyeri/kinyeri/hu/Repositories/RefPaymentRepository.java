package brodev.hu.kinyeri.kinyeri.hu.Repositories;

import brodev.hu.kinyeri.kinyeri.hu.Models.RefPayment;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RefPaymentRepository  extends JpaRepository<RefPayment, Long>, DataTablesRepository<RefPayment,Long > {
    boolean existsByFinishTimeIsNullAndReqUser(User user);
}
