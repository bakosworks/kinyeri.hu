package brodev.hu.kinyeri.kinyeri.hu.Mapper;

import brodev.hu.kinyeri.kinyeri.hu.Dto.MatchDto;
import brodev.hu.kinyeri.kinyeri.hu.Dto.MatchHVDDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.Match;
import brodev.hu.kinyeri.kinyeri.hu.Models.Team;
import brodev.hu.kinyeri.kinyeri.hu.Services.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@Component
public class MatchMapper {

    @Autowired
    private TeamService teamService;

    private static final String DATE_FORMATTER= "yyyy-MM-dd HH:mm:ss";

    public MatchDto toMatchDto(Match from, MatchDto to){
        to.setId(from.getId());
        to.setHteam(from.getHomeTeam().getName());
        to.setVteam(from.getVisitorTeam().getName());
        to.setMatch(from.getMatchTime());
        to.setOddsH(from.getOddsH());
        to.setOddsD(from.getOddsD());
        to.setOddsV(from.getOddsV());
        to.setGoalHQty(from.getHomeGoalQty());
        to.setGoalVQty(from.getVisitorGoalQty());
        to.setResult(from.getResult());
        to.setHomeTeam(from.getHomeTeam());
        to.setVisitorTeam(from.getVisitorTeam());
        to.setHomeGoalQty(from.getHomeGoalQty());
        to.setVisitorGoalQty(from.getVisitorGoalQty());
        to.setMatchTime(from.getMatchTime());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
        to.setInsertTime(from.getInsertTime());
        to.setInsertTimeString(from.getInsertTime().format(formatter));


        if(from.getUnibetOdds()!=null)
        {
            to.setOver1500(from.getUnibetOdds().getOver1500());
            to.setOver2500(from.getUnibetOdds().getOver2500());
            to.setOver3500(from.getUnibetOdds().getOver3500());
            to.setOver4500(from.getUnibetOdds().getOver4500());
            to.setOver5500(from.getUnibetOdds().getOver5500());
            to.setOver6500(from.getUnibetOdds().getOver6500());

            to.setUnder1500(from.getUnibetOdds().getUnder1500());
            to.setUnder2500(from.getUnibetOdds().getUnder2500());

            to.setBothTeamGolNo(from.getUnibetOdds().getBothTeamNo());
            to.setBothTeamGolYes(from.getUnibetOdds().getBothTeamYes());
        }


        return to;
    }

    public Match toMatch (MatchDto from, Match to){
        to.setId(from.getId());
        try{
                to.setHomeTeam(teamService.findOne(from.getHteam()));
                to.setVisitorTeam(teamService.findOne(from.getVteam()));
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        to.setMatchTime(from.getMatch());
        to.setInsertTime(LocalDateTime.now());
        to.setOddsH(from.getOddsH());
        to.setOddsD(from.getOddsD());
        to.setOddsV(from.getOddsV());
        to.setHomeGoalQty(from.getGoalHQty());
        to.setVisitorGoalQty(from.getGoalVQty());
        to.setResult(from.getResult());

        return to;
    }
}
