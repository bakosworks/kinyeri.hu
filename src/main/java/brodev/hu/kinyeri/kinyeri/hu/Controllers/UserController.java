package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Dto.PersonalInfoDto;
import brodev.hu.kinyeri.kinyeri.hu.Dto.UserDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.*;
import brodev.hu.kinyeri.kinyeri.hu.Services.MailService;
import brodev.hu.kinyeri.kinyeri.hu.Services.MatchService;
import brodev.hu.kinyeri.kinyeri.hu.Services.PasswordResetTokenService;
import brodev.hu.kinyeri.kinyeri.hu.Services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private MatchService matchService;

    @Autowired
    MailService mailService;

    @Autowired
    PasswordResetTokenService passwordResetTokenService;

    @PostMapping("/registration-valid")
    public ModelAndView registratiodn(@ModelAttribute("user") UserDto userDto) {

        return  userService.registerValid(userDto);
    }

    @PostMapping(value = "/list", name = "Get all Users to DataTable")
    public DataTablesOutput<User> getAllUser(@Valid @RequestBody DataTablesInput input) {
        return userService.findAll(input);
    }
    @DeleteMapping(value = "/delete/{id}", name="Delete a position by Id")
    public JsonResponse deleteUserById(@PathVariable("id") Long id) {
        return userService.delete(id);
    }

    @PostMapping(value = "/activate/{id}", name="Activate change by Id")
    public JsonResponse changeActivateById(@PathVariable("id") Long id) {

        try {
            User user = userService.get(id);
            user.setActive(!user.getActive());
            userService.save(user);
            return new JsonResponse("Sikeres státusz módosítás!","success");
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"error");
        }
    }

    @PostMapping(value = "/add", name ="Add a User from DTO" )
    public JsonResponse addUserByDTO(@RequestBody UserDto userDto){
        try {
            User user = modelMapper.map(userDto,User.class);
            if(!userService.existByUsername(user.getUsername()))
            {
                if(!userDto.getPassword().equals(userDto.getPassword2())) {
                    return new JsonResponse("Jelszavak nem egyeznek","error");
                }
                userService.saveUserWithRoles(user);
                return new JsonResponse("Sikeres","success");
            }
            else
            {
                return new JsonResponse("Felhasználónév foglalt!","error");
            }
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"error");
        }
    }

    @PostMapping(value = "/edit", name ="Add a User from DTO" )
    public JsonResponse editUserByDTO(@RequestBody UserDto userDto){


        try {


            if(userService.existByUsername(userDto.getUsername()))
            {
                User foundUser = userService.findUserByUsername(userDto.getUsername());

                Set<Role> newRoles = new HashSet<Role>();

                userDto.getRoles().stream().forEach(roleDto ->
                        {

                            newRoles.add(new Role(roleDto.getRole_id(),roleDto.getRole()));

                        });

                foundUser.setRoles(newRoles);

                if (userDto.getEmail()!="" && userDto.getEmail()!=null )
                {
                    foundUser.setEmail(userDto.getEmail());
                }

                if(!userDto.getPassword().equals("") && !userDto.getPassword2().equals("")) {
                    if (!userDto.getPassword().equals(userDto.getPassword2())) {
                        return new JsonResponse("Jelszavak nem egyeznek", "error");
                    }else{
                        foundUser.setPassword(userDto.getPassword());
                    }
                }
                userService.saveUserWithRoles(foundUser);
                return new JsonResponse("Sikeres","success");
            }
            else
            {
                return new JsonResponse("Hiba történt, felhasználó nem található!","error");
            }
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"error");
        }
    }


    @GetMapping(value = "/get", name ="Get a User by Id")
    public UserDto getUserById(@RequestParam("id") Long id){
        return modelMapper.map(userService.get(id),UserDto.class);
    }

    @GetMapping(value = "/generate-ref-link")
    public String generateRefLinkCodes(){
        return userService.generateRefLinkCodes();
    }

    @PostMapping(value = "/get-by-name", name ="Get a User by name")
    public UserDto getUserById(Principal principal){
        return modelMapper.map(userService.findByUsername(principal.getName()),UserDto.class);
    }

    @RequestMapping(value = {"/forgotPassword"}, method =RequestMethod.POST)
    public JsonResponse forgotPassword(@RequestParam String input_email) throws MessagingException {

        if(!userService.mailCountIsZero(input_email))
        {

            User user = userService.findUserByEmail(input_email);

            String token = UUID.randomUUID().toString();
            userService.createPasswordResetTokenForUser(user, token);
            mailService.sendForgotPw(user.getUsername(),user.getEmail(),token);
            return new JsonResponse("A jelszó módosító emailt kiküldtük!","success");

        }
        else
        {
            return new JsonResponse("Az email cím nem található!","error");
        }
    };


    @RequestMapping(value = {"/change-pw-by-token"}, method = RequestMethod.POST)
    public JsonResponse passwordChangeByToken(@RequestParam String input_password, @RequestParam String token)
    {
        try {

            PasswordResetToken passwordResetToken = passwordResetTokenService.getPasswordResetTokenByToken(token);
            if (passwordResetToken!=null)
            {
                if (passwordResetToken.getStatus()==0)
                {
                    User user = passwordResetToken.getUser();
                    if (user!=null)
                    {
                        passwordResetToken.setStatus(1);
                        passwordResetTokenService.savePasswordResetToken(passwordResetToken);
                        user.setPassword(input_password);
                        userService.saveUserWithPassword(user);
                    }
                    else
                    {
                        return new JsonResponse("A felhasználható nem található!","error");
                    }
                }
                else
                {
                    return new JsonResponse("A token már használva lett!","error");
                }
            }
            else
            {
                return new JsonResponse("A token nem található!","error");
            }

            return new JsonResponse("Sikeres jelszó módosítás!","success");
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"error");
        }

//        if(!userService.mailCountIsZero(input_email))
//        {
//
//            User user = userService.findUserByEmail(input_email);
//
//            String token = UUID.randomUUID().toString();
//            userService.createPasswordResetTokenForUser(user, token);
//            mailService.sendForgotPw(user.getUsername(),user.getEmail(),token);
//            return new JsonResponse("Siker","success");
//
//        }
//        else
//        {
//            return new JsonResponse("Az email cím nem található!","error");
//        }
    };
    @PostMapping(value = "/change-info", name ="Edit profile details from DTO" )
    public JsonResponse addMatchByDTO(@RequestBody PersonalInfoDto personalInfoDto, Principal principal){
        try {
            String[] fullNameSplit = personalInfoDto.getName().split(" ");
            User actualUser = userService.FindByUserName(principal.getName());
            actualUser.setFirstName(fullNameSplit[0]);
            actualUser.setLastName(fullNameSplit[1]);
            actualUser.setAddress(personalInfoDto.getStreet());
            actualUser.setCity(personalInfoDto.getCity());
            actualUser.setEmail(personalInfoDto.getMail());
            userService.save(actualUser);
            return new JsonResponse("Sikeres módosítás!","success");
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"error");
        }
    }
    @PostMapping(value = "/send-mail", name ="Send daily mails" )
    public JsonResponse addMatchByDTO(Principal principal){
        try {
            matchService.sendTomorrowTips();
            return new JsonResponse("Sikeres küldés!","success");
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"error");
        }
    }
    @PostMapping("/subscribe")
    public JsonResponse subscribe(@RequestParam("id") String id){
        return userService.subscribe(id);
    }

    @PostMapping("/unSubscribe")
    public JsonResponse unSubscribe(@RequestParam("id") String id){
        return userService.unSubscribe(id);
    }

    @PostMapping(value = "/ban/{userId}")
    public JsonResponse banUser(@PathVariable Long userId){
        return userService.banUser(userId);
    }

    @PostMapping(value = "/unban/{userId}")
    public JsonResponse unBanUser(@PathVariable Long userId){
        return userService.unBanUser(userId);
    }

    @PostMapping(value = "/reflinks/check")
    public JsonResponse checkMailAddress(@RequestParam("mail") String mail)
    {
        if(mail!=null)
        {
            if(userService.mailCountIsZero(mail)){

                return new JsonResponse("success","Az email cím nem foglalt!");
            }
            else
            {
                return new JsonResponse("error","Az email cím foglalt!");
            }
        }
        return new JsonResponse("error","Hibás mail cím!");
    }


    @PostMapping(value = "/reflink-reg")
    public JsonResponse registrationByReflinkPage(@ModelAttribute("user") UserDto userDto, HttpServletRequest request)
    {
        return userService.saveByReflinkReg(userDto,request);
    }

    @PostMapping("/check-mail")
    public JsonResponse checkMailForAffiliate(@RequestParam("mail") String mail,@RequestParam("") Long id)
    {
        return userService.existsByEmailAndPasswordIsNull(mail,id);
    }

    @PostMapping("/payment-addresses")
    public JsonResponse checkMailForAffiliate(Principal principal,@RequestParam(value = "skrill",required = false) String skrill,@RequestParam(value = "paypal",required = false) String paypal)
    {
        return userService.savePaymentAdresses(principal,skrill,paypal);
    }

}
