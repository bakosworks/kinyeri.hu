package brodev.hu.kinyeri.kinyeri.hu.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HistoryDto {
    private List<MatchWithStatisticDto> list;
    private Integer pageNumber;
    private Boolean endOfPages;
}
