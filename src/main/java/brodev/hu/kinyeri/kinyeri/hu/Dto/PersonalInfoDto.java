package brodev.hu.kinyeri.kinyeri.hu.Dto;

import brodev.hu.kinyeri.kinyeri.hu.Models.Team;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.MatchResult;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PersonalInfoDto {

    private String mail;

    private String city;

    private String street;

    private String name;



}
