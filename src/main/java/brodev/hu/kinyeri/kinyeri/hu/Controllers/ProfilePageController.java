package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Models.Match;
import brodev.hu.kinyeri.kinyeri.hu.Models.Role;
import brodev.hu.kinyeri.kinyeri.hu.Models.Tipp;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.MatchResult;
import brodev.hu.kinyeri.kinyeri.hu.Services.BankDetailService;
import brodev.hu.kinyeri.kinyeri.hu.Services.TeamService;
import brodev.hu.kinyeri.kinyeri.hu.Services.TippService;
import brodev.hu.kinyeri.kinyeri.hu.Services.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = "/profil")
@AllArgsConstructor
public class ProfilePageController {

    private final UserService userService;

    private final TeamService teamService;

    private final BankDetailService bankDetailService;

    private final TippService tippService;


    @GetMapping(value={ "/{name}"})
    public String matchControl(@PathVariable String name, Model model, Principal principal){

        try {
            User user = userService.FindByUserName(name);
            User loggedInUser = userService.FindByUserName(principal.getName());
            boolean mod=false;
            Optional<Role> role= loggedInUser.getRoles().stream()
                    .filter(role1 -> role1.getRole().equals("MOD"))
                    .findFirst();
            if (role.isPresent()){
                mod=true;
            }

            int pendingCount = user.getTipNum()-user.getClosedTips()*4;

            model.addAttribute("user",user);

            long closedTippNumber = tippService.countUserTipps(user);

            List<Tipp> tippList = tippService.findAllTippByUser(user);
            Integer countOfHdv = 0;
            Integer countOfGoal = 0;
            Integer countOfGG = 0;

            Float oddsOfGG = 0.00F;
            Float oddsOfGoal = 0.00F;
            Float oddsOfHdv = 0.00F;


            for (Tipp tipp : tippList)
            {
                Match match = tipp.getTippId().getMatchId();

                if(tipp.getResultTip().equals(match.getResult()))
                {
                    if(match.getResult()==MatchResult.H)
                    {
                        oddsOfHdv+=match.getOddsH();
                    }
                    else if(match.getResult()==MatchResult.D)
                    {
                        oddsOfHdv+=match.getOddsD();
                    }
                    else
                    {
                        oddsOfHdv+=match.getOddsV();
                    }
                    countOfHdv++;
                }

                if(match.getUnibetOdds() != null) {
                    /*
                    if(tipp.getGoalQty()==(match.getHomeGoalQty()+match.getVisitorGoalQty()))
                    {

                        Integer goalOver = tipp.getGoalQty();
                        if (goalOver==0)
                        {
                            if(match.getUnibetOdds().getUnder1500()!=null)
                            {
                                oddsOfGoal+=match.getUnibetOdds().getUnder1500();
                                countOfGoal++;
                            }

                        }
                        else if (goalOver==1)
                        {
                            if(match.getUnibetOdds().getUnder2500()!=null)
                            {
                                oddsOfGoal+=match.getUnibetOdds().getUnder2500();
                                countOfGoal++;
                            }
                        }
                        else if (goalOver==2 || goalOver == 3)
                        {
                            if(match.getUnibetOdds().getOver2500()!=null)
                            {
                                oddsOfGoal+=match.getUnibetOdds().getOver2500();
                                countOfGoal++;
                            }
                        }
                        else if (goalOver==4 || goalOver == 5)
                        {
                            if(match.getUnibetOdds().getOver3500()!=null)
                            {
                                oddsOfGoal+=match.getUnibetOdds().getOver3500();
                                countOfGoal++;
                            }
                        }
                        else if (goalOver==6)
                        {
                            if(match.getUnibetOdds().getOver4500()!=null)
                            {
                                oddsOfGoal+=match.getUnibetOdds().getOver4500();
                                countOfGoal++;
                            }
                        }
                        else if (goalOver==7)
                        {
                            if(match.getUnibetOdds().getOver5500()!=null)
                            {
                                oddsOfGoal+=match.getUnibetOdds().getOver5500();
                                countOfGoal++;
                            }
                        }
                        else if (goalOver==8)
                        {
                            if(match.getUnibetOdds().getOver6500()!=null)
                            {
                                oddsOfGoal+=match.getUnibetOdds().getOver6500();
                                countOfGoal++;
                            }
                        }


                    }
                     */
                    if (match.getUnibetOdds().getBothTeamYes() != null && tipp.getVisitorGoalQtyTipp()>0 && tipp.getHomeGoalQtyTipp()>0)
                    {
                        if (match.getVisitorGoalQty()>0 && match.getHomeGoalQty()>0)
                        {
                            oddsOfGG+=match.getUnibetOdds().getBothTeamYes();
                            countOfGG++;
                        }

                    }
                    else
                    {
                        if(match.getUnibetOdds().getBothTeamNo()!=null)
                        {
                            if(!(match.getVisitorGoalQty()>0 && match.getHomeGoalQty() >0))
                            {
                                oddsOfGG+=match.getUnibetOdds().getBothTeamNo();
                                countOfGG++;
                            }
                        }

                    }
                }


            }


            Float actFloat = new Float((float) oddsOfHdv / countOfHdv);
            Float realHDV = (float) Math.round(actFloat * 100) / 100;

            Float actGoal = new Float((float) oddsOfGoal / countOfGoal);
            Float realGoal = (float) Math.round(actGoal * 100) / 100;

            Float actGG = new Float((float) oddsOfGG / countOfGG);
            Float realGG = (float) Math.round(actGG * 100) / 100;


            if(closedTippNumber != 0)
            {
                model.addAttribute("merkozesnyertese",(user.getWinWinningTeamQty()*100)/((closedTippNumber)));
                model.addAttribute("gg", (user.getWinWichTeamGoesQty()*100)/((closedTippNumber)));
                model.addAttribute("golszam",(user.getWinNumberOfGolsQty()*100)/((closedTippNumber)));
                model.addAttribute("pontoskimenetel",(user.getWinFinalResultQty()*100)/((closedTippNumber)));
            }
            else
            {
                model.addAttribute("merkozesnyertese",0);
                model.addAttribute("gg", 0);
                model.addAttribute("golszam",0);
                model.addAttribute("pontoskimenetel",0);
            }

            model.addAttribute("realHdv",realHDV == 0.00F ? "" : realHDV.toString().length()==3 ? realHDV+"0" :  realHDV);
            model.addAttribute("realGoal",realGG == 0.00F? "" : realGG.toString().length()==3 ? realGG+"0" :  realGG);
            model.addAttribute("mod",mod);
            model.addAttribute("bankDetail",bankDetailService.getOne());
            model.addAttribute("loggedInUser",loggedInUser);
            model.addAttribute("loseTipp",user.getTipNum()-pendingCount-user.getWinNum());
            model.addAttribute("closedTippNumber", closedTippNumber*4);
            model.addAttribute("strongPoint", userService.getStrongPoints(user));


            return "page/profile";
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "redirect:/login";
        }

    }




}
