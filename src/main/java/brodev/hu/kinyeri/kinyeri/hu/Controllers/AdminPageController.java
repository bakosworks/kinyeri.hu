package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Models.Lottery;
import brodev.hu.kinyeri.kinyeri.hu.Models.LotteryUser;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.LotteryRepository;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.LotteryUserRepository;
import brodev.hu.kinyeri.kinyeri.hu.Services.*;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;

import static brodev.hu.kinyeri.kinyeri.hu.Models.enums.TopStatisticType.ALL_TIME;

@Controller
@RequestMapping(value = "/kinyeri-admin")
@AllArgsConstructor
public class AdminPageController {

    private final UserService userService;

    private final TeamService teamService;

    private final RoleService roleService;

    private final PackageService packageService;

    private final BankDetailService bankDetailService;

    private final ReflinkService reflinkService;

    private final LotteryRepository lotteryRepository;

    private final LotteryUserRepository lotteryUserRepository;

    @GetMapping(value={ "/match-control"})
    public String matchControl(Model model){
        model.addAttribute("teams",teamService.findAllList());
        return "/page/match-control";
    }

    @GetMapping(value={ "/user-control"})
    public String userControl(Model model){
        model.addAttribute("roles",roleService.findAll());
        return "/page/user-control";
    }

    @GetMapping(value={ "/matchv2"})
    public String matchv2(Model model){
        return "/page/match-control-v2";
    }

    @RequestMapping(value = "/team-control", method =RequestMethod.GET)
    public String teamControl(Model model)
    {
        return "/page/team-control";
    }

    @RequestMapping(value = "/package-control", method =RequestMethod.GET)
    public String packageControl(Model model)
    {
        return "/page/package-control";
    }


    @GetMapping(value={ "/notification-control"})
    public String notificationControl(Model model){
        model.addAttribute("reflink",reflinkService.findOne());
        return "/page/notification-control";
    }

    @RequestMapping(value = "/payment-control", method =RequestMethod.GET)
    public String paymentControl(Model model)
    {
        model.addAttribute("users", userService.findAllList());
        model.addAttribute("packages", packageService.findAllList());
        return "/page/payment";
    }

    @RequestMapping(value = "/bank-detail", method =RequestMethod.GET)
    public String bankDetailControl(Model model)
    {
        model.addAttribute("bankDetail", bankDetailService.getOne());
        return "/page/bank-detail";
    }

    @RequestMapping(value = "/sorsolas", method =RequestMethod.GET)
    public String sorsolasControl(Model model)
    {
        return "/page/sorsolas";
    }



    @RequestMapping(value = "/sorsolas/new", method =RequestMethod.POST)
    @ResponseBody
    public JsonResponse sorsolasControlAdd(Model model, Principal principal)
    {
        List<User> users = userService.findTop10(ALL_TIME);

        Long userCounter = userService.getCurrentMonthActiveUsers();
        Double money =  (1000*userCounter)*0.7;

        Double[] calculatedMoney = {(money*0.3),(money*0.2),(money*0.15),(money*0.1),(money*0.1),(money*0.03),(money*0.03),(money*0.03),(money*0.03),(money*0.03)};

        Lottery lottery = new Lottery();
        lottery.setCreated(LocalDateTime.now());
        lottery.setUser(userService.findByUsername(principal.getName()));

        lotteryRepository.save(lottery);

        int i = 0;
        for (User user : users)
        {
            Double plusMoney = calculatedMoney[i];
            Integer currentBalance = user.getBalance() == null ? 0 : user.getBalance();
            user.setBalance(currentBalance+plusMoney.intValue());

            LotteryUser lotteryUser =  new LotteryUser();
            lotteryUser.setUser(user);
            lotteryUser.setLotteryId(lottery);
            lotteryUser.setMoney(plusMoney);
            lotteryUser.setPosition(i);
            lotteryUserRepository.save(lotteryUser);
            i=i+1;

        };


        return new JsonResponse("success","success");
    }

    @RequestMapping(value = "/sorsolas/get", method =RequestMethod.POST)
    @ResponseBody
    public DataTablesOutput<LotteryUser> sorsolasControlGet(@Valid @RequestBody DataTablesInput input)
    {
        return lotteryUserRepository.findAll(input);
    }

}
