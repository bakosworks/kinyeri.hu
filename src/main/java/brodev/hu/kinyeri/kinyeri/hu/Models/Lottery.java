package brodev.hu.kinyeri.kinyeri.hu.Models;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "lottery")
@Entity
@Data
public class Lottery {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "created")
    private LocalDateTime created;

    @ManyToOne
    @JoinColumn(name = "who")
    private User user;

}



