package brodev.hu.kinyeri.kinyeri.hu.Services;

import brodev.hu.kinyeri.kinyeri.hu.Models.PasswordResetToken;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.PasswordTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class PasswordResetTokenService {
    @Autowired
    PasswordTokenRepository passwordTokenRepository;

    public PasswordResetToken getPasswordResetTokenByToken(String token)
    {
        return passwordTokenRepository.findByToken(token);
    }

    public void savePasswordResetToken(PasswordResetToken passwordResetToken)
    {
        passwordTokenRepository.save(passwordResetToken);
    }

    public String validatePasswordResetToken(String token) {
        final PasswordResetToken passToken = passwordTokenRepository.findByToken(token);
        return !isTokenFound(passToken) ? "invalidToken" : isTokenExpired(passToken) ? "expired" : null;
    }

    private boolean isTokenFound(PasswordResetToken passToken) {
        return passToken != null;
    }

    private boolean isTokenExpired(PasswordResetToken passToken) {
        final Calendar cal = Calendar.getInstance();
        return passToken.getExpiryDate().before(cal.getTime());
    }
}
