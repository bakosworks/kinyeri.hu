package brodev.hu.kinyeri.kinyeri.hu.Services;

import brodev.hu.kinyeri.kinyeri.hu.Dto.ReflinkDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Models.Reflink;
import brodev.hu.kinyeri.kinyeri.hu.Models.Team;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.ReflinkRepository;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.TeamRepository;
import lombok.AllArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ReflinkService {

    private final ReflinkRepository reflinkRepository;

    public Reflink findOne() {
        List<Reflink> finded = reflinkRepository.findAll();

        if (finded.isEmpty())
        {
            return  new Reflink(-1l,"Reflink szükséges","Link szükséges","Link szükséges","Link szükséges");
        }
        else
        {
           return finded.get(0);
        }

    }

    public boolean save(ReflinkDto reflinkDto){
        try {

            if (reflinkDto.getId() == -1)
            {
                Reflink reflink = new Reflink(null,reflinkDto.getText(),reflinkDto.getLink(),reflinkDto.getStreamReflink(), reflinkDto.getMatchReflink());
                reflinkRepository.save(reflink);

            }
            else
            {
                Reflink dbReflink = reflinkRepository.findById(reflinkDto.getId()).get();
                dbReflink.setLink(reflinkDto.getLink());
                dbReflink.setText(reflinkDto.getText());
                dbReflink.setStreamReflink(reflinkDto.getStreamReflink());
                dbReflink.setMatchReflink(reflinkDto.getMatchReflink());
                reflinkRepository.save(dbReflink);
            }

            return true;
        }
        catch (Exception e)
        {
            return  false;
        }

    }

}
