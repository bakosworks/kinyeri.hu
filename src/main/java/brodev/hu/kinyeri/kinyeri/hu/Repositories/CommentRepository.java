
package brodev.hu.kinyeri.kinyeri.hu.Repositories;

import brodev.hu.kinyeri.kinyeri.hu.Models.Comment;
import brodev.hu.kinyeri.kinyeri.hu.Models.Match;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment,Long> {
    List<Comment> findTop10ByMatchIdAndIdLessThanAndUser_BannedAndDeletedByIsNullOrderByIdDesc(Match matchId, Long id,Boolean bool);
    List<Comment> findTop10ByMatchIdAndUser_BannedAndDeletedByIsNullOrderByIdDesc(Match matchId,Boolean bool);
    long countByMatchIdAndUser_BannedAndDeletedByIsNull(Match match,Boolean bool);
}

