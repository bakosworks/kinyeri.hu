package brodev.hu.kinyeri.kinyeri.hu.Models;

import brodev.hu.kinyeri.kinyeri.hu.Models.enums.PaymentStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "payments")
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "created_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "Europe/Budapest")
    private LocalDateTime createdTime;

    @Column(name = "activate_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "Europe/Budapest")
    private LocalDateTime activateTime;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private PaymentStatus status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "package_id")
    private Package packageId;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User userId;

    @Column(name = "discount")
    private Integer discount;
}
