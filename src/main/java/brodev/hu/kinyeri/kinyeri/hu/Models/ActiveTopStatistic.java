package brodev.hu.kinyeri.kinyeri.hu.Models;

import lombok.Data;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Data
@Table(name = "active_top_statistics")
@Immutable
public class ActiveTopStatistic extends BaseTopStatistic {

}
