package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Dto.BankDetailDto;
import brodev.hu.kinyeri.kinyeri.hu.Dto.MatchDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.BankDetail;
import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Models.Match;
import brodev.hu.kinyeri.kinyeri.hu.Services.BankDetailService;
import lombok.AllArgsConstructor;
import org.apache.catalina.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/bankDetail")
public class BankDetailController {

    private final ModelMapper modelMapper;

    private final BankDetailService bankDetailService;

    @PostMapping(value = "/edit", name ="Edit a BankDetail from DTO" )
    public JsonResponse editMatchByDTO(@RequestBody BankDetailDto bankDetailDto){
        try {
            BankDetail bankDetail = new BankDetail();
            bankDetail= modelMapper.map(bankDetailDto, BankDetail.class);

            bankDetailService.save(bankDetail);
            return new JsonResponse("Sikeres mentés!","success");
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"error");
        }
    }
}
