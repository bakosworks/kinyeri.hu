package brodev.hu.kinyeri.kinyeri.hu.Services;


import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import java.util.concurrent.Future;

@Service
public class AsyncEmailService {

    @Async
    public Future<String> sendEmail(Message msg){
        try {
            Transport.send(msg);
            return new AsyncResult<>("Email sent successfully");
        } catch (MessagingException e) {
            e.printStackTrace();
            return new AsyncResult<>("Error: "+e.getMessage());
        }
    }
}
