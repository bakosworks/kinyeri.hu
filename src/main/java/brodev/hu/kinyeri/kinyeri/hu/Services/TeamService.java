package brodev.hu.kinyeri.kinyeri.hu.Services;

import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Models.Team;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.TeamRepository;
import lombok.AllArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TeamService {

    private final TeamRepository teamRepository;

    public boolean existByName(String name) {
        return  teamRepository.existsByName(name);
    }

    public Team get(Long id) {
        return  teamRepository.getOne(id);

    }

    public JsonResponse delete(Long id) {
        try{
            teamRepository.deleteById(id);
            return new JsonResponse("Sikeres törlés", "success");
        }
        catch (DataIntegrityViolationException cve){
            return new JsonResponse("Ez a Csapatnév használatban van így nem törölhető! Töröld ki a mérkőzést!","error");
        }catch(EmptyResultDataAccessException e){
            return new JsonResponse("Ez a Csapatnév  már törölve van!","error");
        }catch(Exception e){
            return new JsonResponse("Sikertelen törlés "+e.getMessage(), "error");
        }

    }

    public void save(Team team) {
        teamRepository.save(team);
    }

    public DataTablesOutput<Team> findAll(DataTablesInput input) {
        return teamRepository.findAll(input);
    }

    public List<Team> findAllList() {
        return teamRepository.findAll();
    }

    public Team findOne(String team) {
        return teamRepository.findTeamByName(team);
    }

    public Team newTeam(String teamName) {
        Team team = new Team();
        team.setName(teamName);
        return teamRepository.save(team);
    }

    public Team findOneOrCreate(String teamName) {

        Optional<Team> teamOptional = teamRepository.findByName(teamName);

        if(teamOptional.isPresent())
        {
            return teamOptional.get();
        }
        else
        {
            Team newTeam = new Team();
            newTeam.setName(teamName);
            return teamRepository.save(newTeam);
        }

    }
}
