package brodev.hu.kinyeri.kinyeri.hu.Dto;

import brodev.hu.kinyeri.kinyeri.hu.Models.Team;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.MatchResult;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
public class MatchDto {

    private Long id;

    private String vteam;

    private String hteam;

    private Float oddsH;

    private Float oddsV;

    private Float oddsD;

    private Integer goalHQty;

    private Integer goalVQty;

    private MatchResult result;

    private LocalDateTime match;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" , timezone = "Europe/Budapest")
    private LocalDateTime insertTime;

    private String insertTimeString;

    private Team homeTeam;

    private Team visitorTeam;

    private Integer homeGoalQty;

    private Integer visitorGoalQty;

    private LocalDateTime matchTime;

    private MatchHVDDto matchHVDDto;

    private LocalDateTime closeTime;

    private String matchReflink;

    private Float under1500;
    private Float under2500;
    private Float over1500;
    private Float over2500;
    private Float over3500;
    private Float over4500;
    private Float over5500;
    private Float over6500;
    private Float bothTeamGolYes;
    private Float bothTeamGolNo;


}
