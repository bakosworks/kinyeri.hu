package brodev.hu.kinyeri.kinyeri.hu.Dto;

import lombok.Data;

@Data
public class MessageDto {
    private String text;
}
