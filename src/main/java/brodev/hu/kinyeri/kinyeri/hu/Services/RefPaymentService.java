package brodev.hu.kinyeri.kinyeri.hu.Services;

import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Models.RefPayment;
import brodev.hu.kinyeri.kinyeri.hu.Models.User;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.PayType;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.PaymentStatus;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.RefPaymentRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RefPaymentService {

    private final RefPaymentRepository refPaymentRepository;

    private final UserService userService;

    public DataTablesOutput<RefPayment> findAll(DataTablesInput input) {
        return refPaymentRepository.findAll(input);
    }

    public JsonResponse paymentDone(Long id, Principal principal) {
        Optional<RefPayment> optional=refPaymentRepository.findById(id);
        if (optional.isPresent()){
            RefPayment refPayment= optional.get();
            User user=userService.findByUsername(principal.getName());
            refPayment.setFinishTime(LocalDateTime.now());
            refPayment.setUserPayed(user);
            refPayment.setPaymentStatus(PaymentStatus.DONE);
            refPaymentRepository.save(refPayment);
            return new JsonResponse("success","success");
        }else{
            return new JsonResponse("Nem található a kérelem!","error");
        }
    }

    public JsonResponse paymentReq(String name, Integer qty, PayType payType) {
       User optional=userService.findByUsername(name);
        if (optional != null){

            if (qty<30000){
                return new JsonResponse("Kiutalás 30.000 Forinttól!","error");
            }

            if (optional.getBalance()<qty ){
                return new JsonResponse("Alacsonyabb az egyenleg a kért összegnél!","error");
            }

            if (refPaymentRepository.existsByFinishTimeIsNullAndReqUser(optional)){
                return new JsonResponse("Egy kifizetésed már folyamatban van, nem tudsz újat kezdeményezni!","error");
            }
            RefPayment refPayment=new RefPayment();
            refPayment.setReqTime(LocalDateTime.now());
            refPayment.setEmail(getProperEmailByPayType(payType,optional));
            refPayment.setPayType(payType);
            refPayment.setReqUser(optional);
            refPayment.setRefBalance(qty);
            refPayment.setPaymentStatus(PaymentStatus.IN_PROCESS);
            refPaymentRepository.save(refPayment);

            optional.setBalance(optional.getBalance()-qty);
            userService.save(optional);

            return new JsonResponse("success","success",optional.getBalance());
        }
        return new JsonResponse("Nem található a felhasználó!","error");
    }

    private String getProperEmailByPayType(PayType payType,User user) {

        switch (payType){
            case SKRILL:
                return user.getSkrillAddress();
            case PAYPAL:
                return user.getPaypalAddress();
            default:return null;
        }
    }

    public JsonResponse paymentRevert(Long id, Principal principal) {
        Optional<RefPayment> optional=refPaymentRepository.findById(id);
        if (optional.isPresent()){
            RefPayment refPayment= optional.get();
            if (!refPayment.getPaymentStatus().equals(PaymentStatus.IN_PROCESS)) {
                return new JsonResponse("Csak le nem zárt kérelmet lehet visszavonni!","error");
            }
            User user=userService.findByUsername(principal.getName());
            user.setBalance(user.getBalance()+refPayment.getRefBalance());
            refPayment.setUserReverted(user);
            refPayment.setFinishTime(LocalDateTime.now());
            refPayment.setPaymentStatus(PaymentStatus.DENIED);
            refPaymentRepository.save(refPayment);
            userService.save(user);
            return new JsonResponse("Sikeres visszavonás!","success",user.getBalance());
        }else{
            return new JsonResponse("Nem található a kérelem!","error");
        }
    }

    public DataTablesOutput<RefPayment> findAll(DataTablesInput input, Principal principal) {
        Specification<RefPayment> specification=new Specification<RefPayment>() {
            @Override
            public Predicate toPredicate(Root<RefPayment> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                return cb.and( cb.like(root.get("reqUser").get("username"), principal.getName()));
            }
        };
        return refPaymentRepository.findAll(input,specification);
    }
}
