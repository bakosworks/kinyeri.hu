package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Models.Match;
import brodev.hu.kinyeri.kinyeri.hu.Models.RefPayment;
import brodev.hu.kinyeri.kinyeri.hu.Models.enums.PayType;
import brodev.hu.kinyeri.kinyeri.hu.Services.RefPaymentService;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@AllArgsConstructor
@RequestMapping("/ref-payment")
public class RefPaymentController {

    private final RefPaymentService refPaymentService;


    @PostMapping(value = "/list", name = "Get all Matches to DataTable")
    public DataTablesOutput<RefPayment> getAllUser(@Valid @RequestBody DataTablesInput input) {
        return refPaymentService.findAll(input);
    }

    @PostMapping(value = "/list-user", name = "Get all Matches to DataTable")
    public DataTablesOutput<RefPayment> getAllUser(@Valid @RequestBody DataTablesInput input, Principal principal) {
        return refPaymentService.findAll(input,principal);
    }

    @PostMapping(value = "/done")
    public JsonResponse paymentDone(@RequestParam("id") Long id, Principal principal){
        return refPaymentService.paymentDone(id,principal);
    }

    @PostMapping(value = "/revert")
    public JsonResponse paymentRevert(@RequestParam("id") Long id, Principal principal){
        return refPaymentService.paymentRevert(id,principal);
    }

    @PostMapping(value = "/req")
    public JsonResponse paymentReq(Principal principal, @RequestParam("qty") Integer qty, @RequestParam("payType") PayType payType){
        return refPaymentService.paymentReq(principal.getName(),qty,payType);
    }
}
