package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Dto.NotificationDto;
import brodev.hu.kinyeri.kinyeri.hu.Dto.ReflinkDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Models.Notification;
import brodev.hu.kinyeri.kinyeri.hu.Services.NotificationService;
import brodev.hu.kinyeri.kinyeri.hu.Services.ReflinkService;
import brodev.hu.kinyeri.kinyeri.hu.Services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/reflink")
@AllArgsConstructor
public class ReflinkController {

    private final ReflinkService reflinkService;


    @PostMapping(value = "/save")
    public JsonResponse editAndSave(@RequestBody ReflinkDto reflinkDto)
    {
        if(reflinkService.save(reflinkDto))
        {
            return new JsonResponse("success","success");
        }
        else
        {
            return new JsonResponse("error","error");
        }
    }

}
