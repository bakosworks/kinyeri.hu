package brodev.hu.kinyeri.kinyeri.hu.Dto;

import lombok.Data;

@Data
public class MatchV2 {
    private MatchOfferHDV matchOffer;
    private Long id;
    private String away;
    private String home;
    private String start;
}
