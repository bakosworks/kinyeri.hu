package brodev.hu.kinyeri.kinyeri.hu.Repositories;

import brodev.hu.kinyeri.kinyeri.hu.Models.ActiveTopStatistic;
import org.springframework.stereotype.Repository;

@Repository
public interface ActiveTopStatisticRepository extends BaseTopStatisticsRepository<ActiveTopStatistic>{
}
