
package brodev.hu.kinyeri.kinyeri.hu.Services;


import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Models.Notification;
import brodev.hu.kinyeri.kinyeri.hu.Models.Tipp;
import brodev.hu.kinyeri.kinyeri.hu.Repositories.NotificationRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


@Service
public class NotificationService {

    @Value("${server.name}")
    String server;

    @Value("${oneSignal.rest_key}")
    String REST_API_KEY;

    @Value("${oneSignal.api_id}")
    String APP_ID;

    @Autowired
    private  NotificationRepository notificationRepository;

    public NotificationService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }


    public DataTablesOutput<Notification> findAll(DataTablesInput input) {

        Specification<Notification> additionalSpecification = new Specification<Notification>() {
            @Override
            public Predicate toPredicate(Root<Notification> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> predicates = new ArrayList<>();

                String sendTime = input.getColumn("sendTime").getSearch().getValue();
                String title = input.getColumn("title").getSearch().getValue();
                String message = input.getColumn("message").getSearch().getValue();
                String user = input.getColumn("user.username").getSearch().getValue();

                if(sendTime != ""){
                    LocalDateTime dateTime = LocalDateTime.parse(sendTime);
                    predicates.add(
                            cb.greaterThanOrEqualTo(root.get("sendTime"),dateTime)
                    );
                }
                if(message != ""){
                    predicates.add(
                            cb.like(root.get("message"),"%"+message+"%")
                    );
                }
                if(title != ""){
                    predicates.add(cb.like(root.get("title"),"%"+title+"%"));
                }
                if(user != ""){
                    predicates.add(
                            cb.like(root.join("user").get("username"),user)
                    );
                }


                return cb.and(predicates.toArray(new Predicate[0]));

            }
        };
        return notificationRepository.findAll(input,additionalSpecification);
    }

    public JsonResponse save(Notification notification,Long id) {
        try {
            notificationRepository.save(notification);
            sendMessageToAll(notification.getTitle(),notification.getMessage(),id);
            return new JsonResponse("Sikeres mentés és küldés!","success");
        }
        catch (Exception e)
        {
            return new JsonResponse(e.getMessage(),"error");
        }
    }

    public String getJsonBody(String title, String message,Long id)
    {

        return  id==null?"{"
                +   "\"app_id\": "+ APP_ID +","
                +   "\"included_segments\": [\"All\"],"
                +   "\"contents\": {\"hu\":\""+message+"\",\"en\":\""+message+"\"},"
                +   "\"headings\": {\"hu\":\""+title+"\",\"en\":\""+title+"\"},"
                +" \"content_available\":true"
                + "}"
                :"{"
                +   "\"app_id\": "+ APP_ID +","
                +   "\"included_segments\": [\"All\"],"
                +   "\"contents\": {\"hu\":\""+message+"\",\"en\":\""+message+"\"},"
                +   "\"headings\": {\"hu\":\""+title+"\",\"en\":\""+title+"\"},"
                +" \"content_available\":true,"
                +" \"url\":\""+server+"index/#"+id+"\""
                + "}";

    }
    public  void sendMessageToAll(String title, String message,Long id) {
        try {
            String jsonResponse;

            URL url = new URL("https://onesignal.com/api/v1/notifications");
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setUseCaches(false);
            con.setDoOutput(true);
            con.setDoInput(true);

            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.setRequestProperty("Authorization", "Basic "+REST_API_KEY);//REST API
            con.setRequestMethod("POST");

            String strJsonBody = getJsonBody(title,message,id);

            System.out.println("strJsonBody:\n" + strJsonBody);

            byte[] sendBytes = strJsonBody.getBytes("UTF-8");
            con.setFixedLengthStreamingMode(sendBytes.length);

            OutputStream outputStream = con.getOutputStream();
            outputStream.write(sendBytes);

            int httpResponse = con.getResponseCode();
            System.out.println("httpResponse: " + httpResponse);

            jsonResponse = mountResponseRequest(con, httpResponse);
            System.out.println("jsonResponse:\n" + jsonResponse);

        } catch(Throwable t) {
            t.printStackTrace();
        }
    }

    private static String mountResponseRequest(HttpURLConnection con, int httpResponse) throws IOException {
        String jsonResponse;
        if (httpResponse >= HttpURLConnection.HTTP_OK
                && httpResponse < HttpURLConnection.HTTP_BAD_REQUEST) {
            Scanner scanner = new Scanner(con.getInputStream(), "UTF-8");
            jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
            scanner.close();
        } else {
            Scanner scanner = new Scanner(con.getErrorStream(), "UTF-8");
            jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
            scanner.close();
        }
        return jsonResponse;
    }
}

