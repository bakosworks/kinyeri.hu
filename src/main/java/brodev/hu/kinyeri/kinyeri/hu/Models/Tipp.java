package brodev.hu.kinyeri.kinyeri.hu.Models;

import brodev.hu.kinyeri.kinyeri.hu.Models.enums.MatchResult;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "tipps")
public class Tipp implements Serializable {

    @EmbeddedId
    private TippId tippId;

    @Column(name = "result_tip")
    @Enumerated(EnumType.STRING)
    private MatchResult resultTip;

    @Column(name = "h_goal_qty_tipp")
    private Integer homeGoalQtyTipp;

    @Column(name = "v_goal_qty_tipp")
    private Integer visitorGoalQtyTipp;

    @Column(name = "goal_qty")
    private Integer goalQty;

    @Column(name = "final_points")
    private Integer finalPoints;

    @Column(name = "final_win_count")
    private Integer finalWinCount;

    @Column(name = "win_wining_team")
    private boolean winWinningTeam;

    @Column(name = "win_final_result")
    private boolean winFinalResult;

    @Column(name = "win_number_of_gols")
    private boolean winNumberOfGols;

    @Column(name = "win_which_team_goes")
    private boolean winWichTeamGoes;
}
