package brodev.hu.kinyeri.kinyeri.hu.Controllers;

import brodev.hu.kinyeri.kinyeri.hu.Dto.MatchDto;
import brodev.hu.kinyeri.kinyeri.hu.Dto.PaymentDto;
import brodev.hu.kinyeri.kinyeri.hu.Models.JsonResponse;
import brodev.hu.kinyeri.kinyeri.hu.Models.Package;
import brodev.hu.kinyeri.kinyeri.hu.Models.Payment;
import brodev.hu.kinyeri.kinyeri.hu.Services.PaymentService;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@AllArgsConstructor
@RequestMapping("/payment")
public class PaymentController {

    private final PaymentService paymentService;

    @Scheduled(cron = "0 0 1 * * ?")
    public void DayMinusz(){
        paymentService.dayMinusz();
    }

    @GetMapping("/get")
    public Payment getpayment(@RequestParam("id") Long id){
        return paymentService.getOne(id);
    }

    @PostMapping(value = "/list", name = "Get all Payments to DataTable")
    public DataTablesOutput<Payment> getAllPayment(@Valid @RequestBody DataTablesInput input) {
        return paymentService.findAll(input);
    }
    @PostMapping(value = "/listUser", name = "Get all Payments User to DataTable")
    public DataTablesOutput<Payment> getAllPaymentUser(@Valid @RequestBody DataTablesInput input, Principal principal) {
        return paymentService.findListUserPayment(input, principal);
    }



    @PostMapping("/paid")
    public JsonResponse paid(@RequestParam("id") Long id){
        return paymentService.paid(id);
    }

    @PostMapping("/delete")
    public JsonResponse delete(@RequestParam("id") Long id){
        return paymentService.delete(id);
    }

    @PostMapping("/add")
    public JsonResponse add (@RequestBody PaymentDto paymentDto, Principal principal) {
        return paymentService.add(paymentDto, principal);
    }

    @PostMapping("/addGift")
    public JsonResponse add (@RequestBody PaymentDto paymentDto) {
        return paymentService.addGift(paymentDto);
    }

    @PostMapping("/undo")
    public JsonResponse undo (@RequestParam("id") Long id){
        return paymentService.undo(id);
    }
}
