
package brodev.hu.kinyeri.kinyeri.hu.Models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Table(name = "users")
@Entity
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "banned")
    private Boolean banned;

    @Column(name = "username")
    private String username;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "country")
    private String country;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "city")
    private String city;

    @Column(name = "address")
    private String address;


    @Column(name = "membership")
    private String membership;

    @Column(name = "years_old")
    private Integer yearsOld;

    @Column(name = "active")
    private Boolean active;

    @ManyToMany(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    @Column(name = "tip_num")
    private Integer tipNum=0;

    @Column(name = "win_num")
    private Integer winNum=0;

    @Column(name = "points")
    private Integer points;

    @Formula("(SELECT COUNT(t.user) FROM tipps t WHERE t.final_points IS NOT NULL and t.user = id)")
    private Integer closedTips;

    @Formula(value = "coalesce(win_wining_team_qty / NULLIF((SELECT COUNT(t.user) FROM tipps t WHERE t.final_points IS NOT NULL and t.user = id), 0), 0)")
    private Double winWinningTeamWinRate;

    @Formula(value = "coalesce(win_final_result_qty / NULLIF((SELECT COUNT(t.user) FROM tipps t WHERE t.final_points IS NOT NULL and t.user = id), 0), 0)")
    private Double winFinalResultWinRate;

    @Formula(value = "coalesce(win_number_of_gols_qty / NULLIF((SELECT COUNT(t.user) FROM tipps t WHERE t.final_points IS NOT NULL and t.user = id), 0), 0)")
    private Double winNumberOfGolsWinRate;

    @Formula(value = "coalesce(win_which_team_goes_qty / NULLIF((SELECT COUNT(t.user) FROM tipps t WHERE t.final_points IS NOT NULL and t.user = id), 0), 0)")
    private Double winWichTeamGoesWinRate;

    @OneToMany(mappedBy = "tippId.user", fetch = FetchType.LAZY)
    @JsonBackReference
    private List<Tipp> tippList;

    @Column(name = "win_wining_team_qty")
    private Integer winWinningTeamQty=0;

    @Column(name = "win_final_result_qty")
    private Integer winFinalResultQty=0;

    @Column(name = "win_number_of_gols_qty")
    private Integer winNumberOfGolsQty=0;

    @Column(name = "win_which_team_goes_qty")
    private Integer winWichTeamGoesQty=0;

    @Column(name = "win_rate")
    private Integer winRate=0;

    @Column(name = "boosted_win_rate")
    private Integer boostedWinRate=0;

    @Column(name = "days_left_vip")
    private Integer daysLeftVip=0;

    @Column(name = "last_tip")
    private LocalDateTime lastTip;

    @Column(name ="email_notification")
    private boolean emailNotification;

    @Column(name = "ref_link")
    private String refLink;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ref_user")
    private User refUser;

    @Column(name = "balance")
    private Integer balance=0;

    @Column(name = "skrill_address")
    private String skrillAddress="";

    @Column(name = "paypal_address")
    private String paypalAddress="";
}



