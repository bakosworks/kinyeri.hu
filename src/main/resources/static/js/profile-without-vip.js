$(document).ready(function() {

    $.fn.DataTable.ext.pager.numbers_length = 5;

    var lang = {
        "sEmptyTable":     "Nincs rendelkezésre álló adat",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés ",
        "sZeroRecords":    "Nincs a keresésnek megfelelő találat",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };

    function convertTextToFloat(item)
    {
        if(item!=null)
        {
            let length = item.toString().length;

            if(length==3)
            {
                item+='0'
            }
            else if(length==1)
            {
                item+='.00'
            }
        }

        return item;
    }

    $('select').selectize({
        sortField: 'text'
    });

    var dtTable = $('table#tipps').DataTable({
        // 'responsive':{
        //     breakpoints: [
        //         {name: 'tippId.matchId.homeTeam.name', width: Infinity},
        //         {name: 'tippId.matchId.visitorTeam.name', width: Infinity},
        //         {name: 'tippId.matchId.matchTime', width: Infinity},
        //         {name: 'resultTip', width: Infinity},
        //         {name: 'homeGoalQtyTipp', width: Infinity},
        //         {name: 'goalQty', width: Infinity},
        //     ]
        // },
        'ajax': {
            'contentType': 'application/json',
            'url': '/tipp/list-tipps-hidden/'+$('#username').text(),
            'type': 'POST',
            'data': function (d) {
                return JSON.stringify(d);
            }
        },
        "pageLength": 10,
        'serverSide' : true,
        "processing": true,
        "order": [[ 2, "desc" ]],
        "scrollX": true,

        "language": lang,
        "stateSave": false,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Összes"]],
        "dom": '<"wrapper"ltp>',
        createdRow: function(row, data, dataIndex){
            // If name is "Ashton Cox"
            if(data.resultTip ==null){
                // Add COLSPAN attribute
                $('td:eq(3)', row).attr('colspan', 4);
                // Hide required number of columns
                // next to the cell with COLSPAN attribute
                $('td:eq(4)', row).css('display', 'none');
                $('td:eq(5)', row).css('display', 'none');
                $('td:eq(6)', row).css('display', 'none');
            }
        },
        columns: [
            {
                data:'tippId.matchId.homeTeam.name',

            },
            {
                data:'tippId.matchId.visitorTeam.name'
            },
            {
                data:'tippId.matchId.matchTime'
            },
            {
                data:'resultTip',
                createdCell: function (td, cellData, rowData, row, col) {
                    if (rowData.tippId.matchId.result != null) {
                        if (rowData.resultTip == rowData.tippId.matchId.result) {
                            $(td).css('background', 'green')
                        } else {
                            $(td).css('background', 'red')
                        }
                    }else{
                        $(td).addClass('text-center');
                        $(td).addClass('td-fake-blur');
                    }
                },
                render: function (data, type, row, td) {
                    if(row.resultTip ==null)
                    {
                        return '<div class="w-100 justify-content-between d-inline-block">' +
                            '<span>Előfizetés szükséges a tippek megtekintéséhez</span>' +
                            '<a href="/premium" class="text-decoration-none ml-5 pl-2 pr-2 kinyeri-orange-button" target="_blank">Előfizetések megtekintése</a></div>'
                    }
                    else
                    {

                        let result = row.resultTip;

                        if(result=="H")
                        {
                            result+= ' <span class="odds-text-profile">'+convertTextToFloat(row.tippId.matchId.oddsH)+'</span>';
                        }
                        else if(result=="D")
                        {
                            result+= ' <span class="odds-text-profile">'+convertTextToFloat(row.tippId.matchId.oddsD)+'</span>';
                        }
                        else
                        {
                            result+= ' <span class="odds-text-profile">'+convertTextToFloat(row.tippId.matchId.oddsV)+'</span>';
                        }

                        return result;
                    }
                }
            },
            {
                data:'visitorGoalQtyTipp',
                render: function (data, type, row) {
                    if(row.resultTip ==null)
                    {
                        return '-'
                    }else{

                        if(row.visitorGoalQtyTipp>0 && row.homeGoalQtyTipp>0)
                        {
                            goalText = "Igen";
                            if(row.tippId.matchId.unibetOdds != null)
                            {
                                if(row.tippId.matchId.unibetOdds.bothTeamYes != null)
                                {
                                    goalText+=" <span class='odds-text-profile'>"+convertTextToFloat(row.tippId.matchId.unibetOdds.bothTeamYes)+"</span>";
                                }
                            }
                        }
                        else
                        {
                            goalText = "Nem";
                            if(row.tippId.matchId.unibetOdds != null)
                            {
                                if(row.tippId.matchId.unibetOdds.bothTeamYes != null)
                                {
                                    goalText+=" <span class='odds-text-profile'>"+convertTextToFloat(row.tippId.matchId.unibetOdds.bothTeamNo)+"</span>";
                                }
                            }
                        }
                        return goalText;

                    }
                },
                createdCell: function (td, cellData, rowData) {
                    if (rowData.tippId.matchId.visitorGoalQty != null && rowData.tippId.matchId.homeGoalQty != null  ) {
                        if (rowData.winWichTeamGoes) {
                            $(td).css('background', 'green')
                        } else {
                            $(td).css('background', 'red')
                        }
                    }
                }
            },
            {
                data:'goalQty',
                render: function (data, type, row) {
                    if(row.resultTip ==null)
                    {
                        return '-'
                    }else{
                        return row.goalQty
                    }
                },
                createdCell: function (td, cellData, rowData,) {
                    if (rowData.tippId.matchId.visitorGoalQty != null && rowData.tippId.matchId.homeGoalQty != null  ) {
                        if (rowData.goalQty == (rowData.tippId.matchId.visitorGoalQty + rowData.tippId.matchId.homeGoalQty)) {
                            $(td).css('background', 'green')
                        } else {
                            $(td).css('background', 'red')
                        }
                    }else{
                        $(td).css('filter', 'blur(1.1rem)')
                    }
                }
            },
            {
                data:'homeGoalQtyTipp',
                render: function (data, type, row) {
                    if(row.resultTip ==null)
                    {
                        return '-'
                    }else{
                        return row.homeGoalQtyTipp +" - " +row.visitorGoalQtyTipp
                    }
                },
                createdCell: function (td, cellData, rowData,) {
                    if (rowData.tippId.matchId.visitorGoalQty != null && rowData.tippId.matchId.homeGoalQty != null  ) {
                        if (rowData.visitorGoalQtyTipp == rowData.tippId.matchId.visitorGoalQty && rowData.homeGoalQtyTipp == rowData.tippId.matchId.homeGoalQty) {
                            $(td).css('background', 'green')
                        } else {
                            $(td).css('background', 'red')
                        }
                    }else{
                        $(td).css('filter', 'blur(1.1rem)')
                    }
                }
            }

        ]
    });

});


$(document).find("#tipps thead th:first-child, #table1 td:first-child").addClass('text-white');
