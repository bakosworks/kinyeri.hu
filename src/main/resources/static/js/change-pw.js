$(document).ready(function(){



    $(document).on('submit',"#pwToken",function (event) {

        let form = document.forms['pwToken'];
        let formData = new FormData(form);
            if ($('#input_password').val() == $('#input_password_again').val())
            {
                $.ajax({
                    method:'POST',
                    url: '/user/change-pw-by-token',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(data){
                        if(data.type=='success')
                        {
                            swal({
                                title: "Sikeres",
                                text: ""+data.msg,
                                icon: "success",
                                closeOnClickOutside:false,
                            });
                            $('#pwToken').trigger('reset');
                        }
                        else if(data.type=='error')
                        {
                            swal({
                                title: "Hiba!",
                                text: ""+data.msg,
                                icon: "warning",
                                closeOnClickOutside:false,
                            });
                        }
                    },
                    error: function (error) {
                        swal({
                            title: "Hiba lépett fel",
                            text: ""+error.responseJSON['message'],
                            icon: "warning",
                            closeOnClickOutside:false,
                            dangerMode: true,
                        }).then(function(isConfirm) {
                            if (isConfirm) {

                            }
                        });
                    }
                });
            }
            else
            {
                swal({
                    title: "Hiba!",
                    text: "A két jelszó nem egyezik!",
                    icon: "warning",
                    closeOnClickOutside:false,
                });
            }

        event.preventDefault();
    });


});