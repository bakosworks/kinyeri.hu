
var stompClient = null;

function setConnected(connected) {
    if (connected) {
        $(".kinyeri-chat").show();
    }
    else {
        $(".kinyeri-chat").hide();
    }
}

function showCommentMessages(text, username, id, matchId, history,commentQty,userId) {
    const commentQtyElement=$("#commentQty-"+matchId).find(".kinyeri-comment-qty");
    if (commentQty===undefined){
        let qty=commentQtyElement.text();
        if (qty===""){
            commentQtyElement.text("1");
        }else{
            commentQtyElement.text((parseInt(qty)+1));
        }
    }else{
        commentQtyElement.text(commentQty);
    }
    commentQtyElement.addClass('uk-badge ml-1 mb-1');
    const link="/profil/"+username;
    let lUID="";
    if ($('#hideMe').text()==userId){
        lUID="<i class=\"kinyeri-delete-comment ml-3 fas fa-trash\"></i>";
    }
    const comment="<p class='kiny-com kinyeri-user-"+userId+"' data-id="+id+" id="+id+">" +
        "<a class='badge badge-primary text-wrap text-decoration-none' target='_blank' href="+link+">"+username+": </a>" +
        "<span class='ml-1'>" + text + "</span>" +lUID+
        "</p>";
    if (history){
        $("#comment-texts-"+matchId+"").prepend(comment);
    }else{
        $("#comment-texts-"+matchId+"").append(comment);
    }
}

function showStatistics(matchId,wrH,wrV,wrD,goalOver,goalTippHome,goalTippVisitor,max,fullStatistic) {


    if(goalOver!=undefined && goalOver !=null)
    {
        if(goalOver == 0)
        {
            $("#" + matchId + "-goalOver").text("Várható gólok száma: 1.5 Alatt");

                $('#'+matchId+'-goalOverOdds').text(fullStatistic.under1500);
        }
        else   if(goalOver == 1)
        {
            $("#" + matchId + "-goalOver").text("Várható gólok száma: 2.5 Alatt");

                $('#'+matchId+'-goalOverOdds').text(fullStatistic.under2500);
        }
        else   if(goalOver == 2)
        {
            $("#" + matchId + "-goalOver").text("Várható gólok száma: 2.5 Alatt");

                $('#'+matchId+'-goalOverOdds').text(fullStatistic.under2500);
        }
        else   if(goalOver == 3)
        {
            $("#" + matchId + "-goalOver").text("Várható gólok száma: 2.5 Felett");

                $('#'+matchId+'-goalOverOdds').text(fullStatistic.over2500);
        }
        else   if(goalOver == 4)
        {
            $("#" + matchId + "-goalOver").text("Várható gólok száma: 3.5 Felett");

                $('#'+matchId+'-goalOverOdds').text(fullStatistic.over3500);
        }
        else   if(goalOver == 5)
        {
            $("#" + matchId + "-goalOver").text("Várható gólok száma: 3.5 Felett");

                $('#'+matchId+'-goalOverOdds').text(fullStatistic.over3500);
        }
        else   if(goalOver ==6)
        {
            $("#" + matchId + "-goalOver").text("Várható gólok száma: 4.5 Felett");

                $('#'+matchId+'-goalOverOdds').text(fullStatistic.over4500);
        }
        else   if(goalOver ==7)
        {
            $("#" + matchId + "-goalOver").text("Várható gólok száma: 5.5 Felett");

                $('#'+matchId+'-goalOverOdds').text(fullStatistic.over5500);

        }
        else   if(goalOver >=8)
        {
            $("#" + matchId + "-goalOver").text("Várható gólok száma: 6.5 Felett");

                $('#'+matchId+'-goalOverOdds').text(fullStatistic.over6500);
        }

    }
    // $("#" + matchId + "-goalTippHome").text("Várt végeredmény: "+goalTippHome)
    $("#" + matchId + "-goalHomeVsVisitor").text(goalTippHome+" - "+goalTippVisitor)

    if (goalTippHome>0 && goalTippVisitor>0){
        $("#" + matchId + "-bothTeam").html("<span class=\"\">Mindkét csapat szerez gólt: Igen</span>");
        $("#" + matchId + "-bothTeamOdds").text(fullStatistic.bothTeamYes);


    }else{
        $("#" + matchId + "-bothTeam").html("<span class=\"\">Mindkét csapat szerez gólt: Nem</span>");
        $("#" + matchId + "-bothTeamOdds").text(fullStatistic.bothTeamNo);

    }
    let s="Várható végkimenetel: ";
    let i=0;

    let hdvOdds = 0.00;

    if (wrH!=0){
        if (wrH==max) {
            $("#" + matchId + "-home")
                .text(wrH + "%")
                .closest(".content")
                .children()
                .first()
                .addClass("kinyeri-yellow-cube");
            s=s+"Hazai, ";
            hdvOdds = fullStatistic.oddsH;
            i++;
        }else{
            $("#" + matchId + "-home")
                .text(wrH + "%")
                .closest(".content")
                .children()
                .first()
                .removeClass("kinyeri-yellow-cube")
        }
    }else{
        $( "#"+matchId+"-home" ).text("0%")
            .closest(".content")
            .children()
            .first()
            .removeClass("kinyeri-yellow-cube")
    }
    if (wrD!=0){
        if (wrD==max) {
            $("#" + matchId + "-draw")
                .text(wrD + "%")
                .closest(".content")
                .children()
                .first()
                .addClass("kinyeri-yellow-cube");
            s=s+"Döntetlen, ";
            hdvOdds = fullStatistic.oddsD;
            i++;
        }else{
            $("#" + matchId + "-draw")
                .text(wrD + "%")
                .closest(".content")
                .children()
                .first()
                .removeClass("kinyeri-yellow-cube")
        }
    }else{
        $( "#"+matchId+"-draw" ).text("0%")
            .closest(".content")
            .children()
            .first()
            .removeClass("kinyeri-yellow-cube")
    }
    if (wrV!=0){
        if (wrV==max) {
            $("#" + matchId + "-visitor")
                .text(wrV + "%")
                .closest(".content")
                .children()
                .first()
                .addClass("kinyeri-yellow-cube");
            s=s+"Vendég, ";
            hdvOdds = fullStatistic.oddsV;
            i++;
        }else{
            $("#" + matchId + "-visitor")
                .text(wrV + "%")
                .closest(".content")
                .children()
                .first()
                .removeClass("kinyeri-yellow-cube")
        }
    }else{
        $( "#"+matchId+"-visitor" ).text("0%")
            .closest(".content")
            .children()
            .first()
            .removeClass("kinyeri-yellow-cube")
    }
    switch (i){
        case 1:
            $("#" + matchId + "-expectedResultDiv").html("<div id=\""+matchId+"-expectedResultDiv\" class=\" text-center\"><span th:id="+matchId+"-expectedResult\" class=\" font-weight-bolder\">"+s.substring(0,s.length-2)+" </span><span class='odds-button text-white'>"+hdvOdds+"</span></div>");
            break;
        case 2:
            $("#" + matchId + "-expectedResultDiv").html("<div id=\""+matchId+"-expectedResultDiv\" class=\" text-center\"><span th:id="+matchId+"-expectedResult\" class=\" font-weight-bolder\">Várható végkimenetel: Kétesélyes</span></div>");
            break;
        case 3:
            $("#" + matchId + "-expectedResultDiv").html("<div id=\""+matchId+"-expectedResultDiv\" class=\" text-center\"><span th:id="+matchId+"-expectedResult\" class=\" font-weight-bolder\">Kiszámíthatatlan a mérkőzés kimentele</span></div>");
            break;
    }
}

function deleteComment(id,matchId) {
    $("#comment-texts-"+matchId+"").find("#"+id).html("");
    const commentQtyElement=$("#commentQty-"+matchId).find(".kinyeri-comment-qty");

    let qty=commentQtyElement.text();

    commentQtyElement.text((parseInt(qty)-1));


}

function deleteChatMessage(id) {
    $("#chatMessages").find("#"+id).html("");
}

function userBanned(id) {

    $(".kinyeri-user-"+id).each(function () {
        $(this).html("");
    });
}

function connect() {
    var socket = new SockJS('/global-chat');
    stompClient = Stomp.over(socket);
    stompClient.debug = null
    stompClient.connect({}, function (frame) {
        stompClient.subscribe('/topic/live-chat', function (message) {
            if (message.body!==null) {
                let msg = JSON.parse(message.body);
                showChatMessages(msg.text, msg.user.username, msg.id, false, msg.user.id);
                if ($(".chat").scrollTop() < ($(".chat").prop("scrollHeight") - 200)) {
                    $(".chat").scrollTop($("#chatMessages").height());
                }
            }
        });
        stompClient.subscribe('/topic/comment/*', function (comment) {
            if (comment.body!==null) {
                comment = JSON.parse(comment.body);
                showCommentMessages(comment.text, comment.user.username, comment.id, comment.matchId.id, false,undefined, comment.user.id);
            }
        });
        stompClient.subscribe('/topic/live-statistics', function (statistics) {
            statistics=JSON.parse(statistics.body);
            showStatistics(statistics.matchId,statistics.home,statistics.visitor,statistics.draw,statistics.goalOver,statistics.goalTippHome,statistics.goalTippVisitor,statistics.maxWinRate,statistics);
        });
        stompClient.subscribe('/topic/comment-delete', function (com) {
            const split=com.body.split(";")
            deleteComment(split[0],split[1]);
        });
        stompClient.subscribe('/topic/chat-delete', function (id) {
            deleteChatMessage(id.body);
        });
        stompClient.subscribe('/topic/user-ban', function (id) {
            userBanned(id.body);
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
}

function sendMessage() {
    stompClient.send("/app/send-message", {}, JSON.stringify({
        'text': $("#textField").val()
    }));
    $("#textField").val("");
    $(".chat").animate({ scrollTop: $('.chat').prop("scrollHeight")}, 1000);
}

function showChatMessages(message,user,id,history,userId) {
    let lUID="";
    if ($('#hideMe').text()==userId){
        lUID="<i class=\"kinyeri-delete-message ml-3 fas fa-trash\"></i>";
    }
    const link="/profil/"+user;
    const messageTag="<p class='kiny-chat kinyeri-user-"+userId+"' data-id="+id+" id="+id+"><a class='badge badge-primary text-wrap text-decoration-none' target='_blank' href="+link+" >"+user+": </a><span class='ml-1'>" + message + "</span>"+lUID+"</p>";
    if (history){
        $("#chatMessages").prepend(messageTag);
    }else{
        $("#chatMessages").append(messageTag);
    }
}

function sendComment(id) {

    if($("#commentText"+id).val().trim()!=""){

        stompClient.send("/app/comment/"+id, {}, JSON.stringify({
            'text': $("#commentText"+id).val()
        }));
        $("#commentText"+id).val("");
    }
    // $(".chat").animate({ scrollTop: $('.chat').prop("scrollHeight")}, 1000);
}

function loadCommentHistory(matchId) {
    matchId=matchId.split('-')[1];
    const commentId=$("#comment-texts-"+matchId+" p:first-child").attr('id');
    let valami;
    if (commentId===undefined){
        valami=-111;
    }else{
        valami=commentId;
    }
    $.ajax({
        url: "/comments/get-comments/"+matchId+"/"+valami,
        method: "GET",
        contentType: 'application/json',
        data: {},
        success: function (response) {
            if (response!=null){
                response.comments.forEach(comment =>{
                    showCommentMessages(comment.text,comment.user.username,comment.id,comment.matchId.id,true,response.commentQty,comment.user.id);
                })
            }
        }
    });
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $('.kinyeri-comment-title').on('click', function (event) {
        event.stopPropagation();
        event.stopImmediatePropagation();
        if($(this).next().is(':visible')){
            $(this).find('.fa-angle-down').toggleClass('fa-angle-down fa-angle-left');
            $(this).next().hide();
        }else{
            $(this).find('.fa-angle-left').toggleClass('fa-angle-left fa-angle-down');
            $(this).next().show();
        };
    });
    $( "#send" ).click(function() {
        if ($("#textField").val()===""){}else{
            sendMessage();
        }
    });
    $( "#kinyeri-history-button" ).click(function() { getHistoryMessages(); });
    $( ".kinyeri-comment-history-button" ).click(function() { loadCommentHistory(
        $(this).closest('.comment').attr('id')
    ); });
    $( ".kinyeri-comment-btn" ).click(function() {


            sendComment($(this).closest('.comment').attr('id').split('-')[1]);

    });
});
// $(function () {
//     $('.kinyeri-chat-title').on('click', function () {
//         if($('.kinyeri-chat').is(':visible')){
//             $('.kinyeri-chat-container').removeClass('chat-visible');
//             $(".kinyeri-chat").hide();
//         }else{
//             $('.kinyeri-chat-container').addClass('chat-visible');
//             $(".kinyeri-chat").show();
//         };
//     });
// });
function getHistoryMessages() {
    const id=$("#chat p:first-child").attr('id');
    let valami;
    if (id===undefined){
        valami=-111;
    }else{
        valami=id;
    }
    $.ajax({
        url: "/messages/get-messages/"+valami,
        method: "GET",
        contentType: 'application/json',
        data: {},
        success: function (response) {
            if (response!=null && response != ""){
                response.forEach(msg =>{
                    showChatMessages(msg.text,msg.user.username,msg.id,true,msg.user.id);
                })
            }
        }
    });
};
function openForm() {
    $('#pcChatButton').hide();
    document.getElementById("myForm").style.display = "block";
    $(".chat").scrollTop($("#chatMessages").height());
}

function closeForm() {
    $('#pcChatButton').show();
    document.getElementById("myForm").style.display = "none";
}
$( document ).ready(function() {
    connect();
    getHistoryMessages();
    $( ".kinyeri-comment-history-button" ).each(function() {loadCommentHistory(
        $(this).closest('.comment').attr('id')
    )});
    $(".kinyeri-chat").hide();
    $(".kinyeri-comment-section").each(function () {
        $(this).hide();
    })
});
$(document).on('click','.kinyeri-delete-comment',function () {
    const id=$(this).parent().attr('id');
    swal({
        title: "Biztos törlöd a kommentet?",
        text: "",
        icon: "warning",
        closeOnClickOutside:false,
        buttons:['Mégsem','Igen']
    }).then(function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                method: 'PUT',
                url : '/comments/delete/'+id,
                contentType: 'application/json',
                success : function () {
                    swal({
                        title: 'Sikeres',
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                }
            });
        }
    });
});
$(document).on('click','.kinyeri-delete-message',function () {
    const id=$(this).parent().attr('id');
    swal({
        title: "Biztos törlöd az üzenetet?",
        text: "",
        icon: "warning",
        closeOnClickOutside:false,
        buttons:['Mégsem','Igen']
    }).then(function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                method: 'PUT',
                url : '/messages/delete/'+id,
                contentType: 'application/json',
                success : function () {
                    swal({
                        title: 'Sikeres',
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                }
            });
        }
    });
});