$(document).ready(function() {

    var lang = {
        "sEmptyTable":     "Nincs rendelkezésre álló adat",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés ",
        "sZeroRecords":    "Nincs a keresésnek megfelelő találat",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };

    var dtTable = $('table#packages').DataTable({
        'ajax': {
            'contentType': 'application/json',
            'url': '/package/list',
            'type': 'POST',
            'data': function (d) {
                return JSON.stringify(d);
            }
        },
        "pageLength": 10,
        'serverSide' : true,
        "processing": true,
        "scrollX": true,

        "language": lang,
        "stateSave": false,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Összes"]],
        "dom": '<"wrapper"ltip>',
        columns: [
            {
                data:'name'
            },{
                data:'interval'
            },{
                data:'color',
                render: data => {
                    if (data==''){
                        return ('<span>simple</span>')
                    }else{
                        return ('<span>'+data+'</span>')
                    }
                }
            },{
                data:'price'
            },{
                data:'details'
            },{
                data:'active'
            },{
                data:'activeTill'
            },
            {
                data: 'id',
                render:function(data,type,row){
                    return '<span class="ml-2 btn edit"  uk-icon="pencil" data-id='+data+'></span>';
                }
            }

        ]
    });
    $('#add').submit(function(event) {

        $.ajax({
            url: "/package/add",
            method: "POST",
            contentType: 'application/json',
            data:  JSON.stringify({
                "name": $("#name").val(),
                "interval": $("#interval").val(),
                "color": $("#color").val(),
                "price": $("#price").val(),
                "details": $("#details").val(),
                "active": $("#active").val()=='true',
                "activeTill": $("#activeTill").val(),
            }),
            success: function (response) {
                if (response.type == "success"){
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                    $('#addModal').modal('hide');
                    document.getElementById("add").reset();
                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                }
            }
        });
        event.preventDefault();
    });

    $(document).on('click','.delete',function () {

        var id = $(this).data('id');
        swal({
            title: "Biztos törlöd a csomagot?",
            text: "",
            icon: "warning",
            closeOnClickOutside:false,
            dangerMode: true,
            buttons:['Mégsem','Törlés']
        }).then(function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    method: 'DELETE',
                    url : '/package/delete/'+id,
                    processData: false,
                    contentType: false,
                    destroy: true,
                    success : function (data) {
                        if(data.type=='success')
                        {
                            swal({
                                title: data.msg,
                                text: '',
                                icon: "success",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                        else if(data.type=='error')
                        {
                            swal({
                                title: data.msg,
                                text: '',
                                icon: "error",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                    }
                });
            }
        });
    });

    function doSearchOnTable(rowId, control){

        if ($(control).val() == "") {
            dtTable
                .column(rowId)
                .search("")
                .draw();
        } else {
            dtTable
                .column(rowId)
                .search($(control).val())
                .draw();
        }
    }

    $("body").on("change","#search_name",function () {

        doSearchOnTable(0, this);
    });

    $("body").on("change","#search_intervalInDays",function () {

        doSearchOnTable(1, this);
    });

    $("body").on("change","#search_color",function () {

        doSearchOnTable(2, this);
    });

    $("body").on("change","#search_price",function () {

        doSearchOnTable(3, this);
    });

    $("body").on("change","#search_details",function () {

        doSearchOnTable(4, this);
    });
    $("body").on("change","#search_active",function () {
        if ($(this).val() == "") {
            dtTable
                .column(5)
                .search('')
                .draw();
        } else {
            dtTable
                .column(5)
                .search($(this).val()=='true')
                .draw();
        }
    });
    $("body").on("change","#search_activeTill",function () {

        doSearchOnTable(6, this);
    });
    $('#edit').submit(function(event) {

        $.ajax({
            url: "/package/edit",
            method: "PUT",
            contentType: 'application/json',
            data: JSON.stringify({
                "id":  $("#editId").val(),
                "name": $("#editName").val(),
                "interval": $("#editInterval").val(),
                "color": $("#editColor").val(),
                "price": $("#editPrice").val(),
                "details": $("#editDetails").val(),
                "active": $("#editActive").val()=='true',
                "activeTill": $("#editActiveTill").val(),
            }),
            success: function (response) {
                if (response.type == "success"){
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                    $('#editModal').modal('hide')
                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                }
            }
        });
        event.preventDefault();
    });

    $(document).on('click',".edit",function () {
        $('#editModal').modal('show')
        self = $(this);
        $.ajax({
            url: "/package/get",
            method: "GET",
            data: {"id": self.data("id")},
            success: function (data) {
                $("#editId").val(data.id);
                $("#editName").val(data.name);
                $("#editInterval").val(data.interval),
                $("#editColor").val(data.color),
                $("#editPrice").val(data.price),
                $("#editDetails").val(data.details),
                $("#editActive").val(data.active+''),
                $("#editActiveTill").val(data.activeTill)
            }
        })
    });
});