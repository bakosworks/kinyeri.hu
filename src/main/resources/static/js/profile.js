$(document).ready(function() {

    $.fn.DataTable.ext.pager.numbers_length = 5;

    var lang = {
        "sEmptyTable":     "Nincs rendelkezésre álló adat",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés ",
        "sZeroRecords":    "Nincs a keresésnek megfelelő találat",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };

    $('select').selectize({
        sortField: 'text'
    });

    function convertTextToFloat(item)
    {
        if(item!=null)
        {
            let length = item.toString().length;

            if(length==3)
            {
                item+='0'
            }
            else if(length==1)
            {
                item+='.00'
            }
        }

        return item;
    }

    var dtTable = $('table#tipps').DataTable({
        // 'responsive':{
        //     breakpoints: [
        //         {name: 'tippId.matchId.homeTeam.name', width: Infinity},
        //         {name: 'tippId.matchId.visitorTeam.name', width: Infinity},
        //         {name: 'tippId.matchId.matchTime', width: Infinity},
        //         {name: 'resultTip', width: Infinity},
        //         {name: 'homeGoalQtyTipp', width: Infinity},
        //         {name: 'goalQty', width: Infinity},
        //     ]
        // },
        'ajax': {
            'contentType': 'application/json',
            'url': '/tipp/list-tipps/'+$('#username').text(),
            'type': 'POST',
            'data': function (d) {
                return JSON.stringify(d);
            }
        },
        "pageLength": 10,
        'serverSide' : true,
        "processing": true,
        "order": [[ 2, "desc" ]],
        "scrollX": true,

        "language": lang,
        "stateSave": false,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Összes"]],
        "dom": '<"wrapper"ltp>',
        columns: [
            {
                data:'tippId.matchId.homeTeam.name'

            },
            {
                data:'tippId.matchId.visitorTeam.name'
            },
            {
                data:'tippId.matchId.matchTime'
            },
            {
                data:'resultTip',
                createdCell: function (td, cellData, rowData, row, col) {
                    if (rowData.tippId.matchId.result != null) {
                        if (rowData.resultTip == rowData.tippId.matchId.result) {
                            $(td).css('background', 'green')
                        } else {
                            $(td).css('background', 'red')
                        }
                    }
                },
                render: function (data, type, row, td) {
                    if(row.resultTip ==null)
                    {
                        return '-'
                    }else{

                        let result = row.resultTip;

                        if(result=="H")
                        {
                            result+= ' <span class="odds-text-profile">'+convertTextToFloat(row.tippId.matchId.oddsH)+'</span>';
                        }
                        else if(result=="D")
                        {
                            result+= ' <span class="odds-text-profile">'+convertTextToFloat(row.tippId.matchId.oddsD)+'</span>';
                        }
                        else
                        {
                            result+= ' <span class="odds-text-profile">'+convertTextToFloat(row.tippId.matchId.oddsV)+'</span>';
                        }

                        return result;
                    }
                }
            },
            {
                data:'goalQty',
                render: function (data, type, row) {
                    if(row.resultTip ==null)
                    {
                        return '-'
                    }else{

                        if(row.visitorGoalQtyTipp>0 && row.homeGoalQtyTipp>0)
                        {
                            goalText = "Igen";
                            if(row.tippId.matchId.unibetOdds != null)
                            {
                                if(row.tippId.matchId.unibetOdds.bothTeamYes != null)
                                {
                                    goalText+=" <span class='odds-text-profile'>"+convertTextToFloat(row.tippId.matchId.unibetOdds.bothTeamYes)+"</span>";
                                }
                            }
                        }
                        else
                        {
                            goalText = "Nem";
                            if(row.tippId.matchId.unibetOdds != null)
                            {
                                if(row.tippId.matchId.unibetOdds.bothTeamYes != null)
                                {
                                    goalText+=" <span class='odds-text-profile'>"+convertTextToFloat(row.tippId.matchId.unibetOdds.bothTeamNo)+"</span>";
                                }
                            }
                        }
                        return goalText;

                    }
                },
                createdCell: function (td, cellData, rowData) {
                    if (rowData.tippId.matchId.visitorGoalQty != null && rowData.tippId.matchId.homeGoalQty != null  ) {

                        let tipp=rowData.visitorGoalQtyTipp>0 && rowData.homeGoalQtyTipp>0;
                        let result=rowData.tippId.matchId.visitorGoalQty>0 && rowData.tippId.matchId.homeGoalQty>0;

                        if (tipp===result) {
                            $(td).css('background', 'green')
                        } else {
                            $(td).css('background', 'red')
                        }
                    }
                }
            },
            {
                data:'goalQty',
                render: function (data, type, row) {
                    if(row.resultTip ==null)
                    {
                        return '-'
                    }else{

                        let goalText = row.goalQty;
                        let goalOver = row.goalQty;
                        let alma = false;

                        if(row.tippId.matchId.unibetOdds != null && alma == true)
                        {
                            if(goalOver == 0)
                            {
                                goalText+= ' <span class="odds-text-profile">'+convertTextToFloat(row.tippId.matchId.unibetOdds.under1500)+'</span>';
                            }
                            else   if(goalOver == 1 || goalOver == 2)
                            {
                                goalText+= ' <span class="odds-text-profile">'+convertTextToFloat(row.tippId.matchId.unibetOdds.under2500)+'</span>';
                            }
                            else   if(goalOver == 3)
                            {
                                goalText+= '<span class="odds-text-profile">'+convertTextToFloat(row.tippId.matchId.unibetOdds.over2500)+'</span>';
                            }
                            else   if(goalOver == 4)
                            {
                                goalText+= '<span class="odds-text-profile">'+convertTextToFloat(row.tippId.matchId.unibetOdds.over3500)+'</span>';
                            }
                            else   if(goalOver == 5)
                            {
                                goalText+= '<span class="odds-text-profile">'+convertTextToFloat(row.tippId.matchId.unibetOdds.over3500)+'</span>';
                            }
                            else   if(goalOver ==6)
                            {1
                                goalText+= '<span class="odds-text-profile">'+convertTextToFloat(row.tippId.matchId.unibetOdds.over4500)+'</span>';
                            }
                            else   if(goalOver ==7)
                            {
                                goalText+= '<span class="odds-text-profile">'+convertTextToFloat(row.tippId.matchId.unibetOdds.over5500)+'</span>';
                            }
                            else   if(goalOver >=8)
                            {
                                goalText+= '<span class="odds-text-profile">'+convertTextToFloat(row.tippId.matchId.unibetOdds.over6500)+'</span>';
                            }
                        }

                        return goalText;
                    }
                },
                createdCell: function (td, cellData, rowData,) {
                    if (rowData.tippId.matchId.visitorGoalQty != null && rowData.tippId.matchId.homeGoalQty != null  ) {
                        if (rowData.goalQty == (rowData.tippId.matchId.visitorGoalQty + rowData.tippId.matchId.homeGoalQty)) {
                            $(td).css('background', 'green')
                        } else {
                            $(td).css('background', 'red')
                        }
                    }
                }
            },
            {
                data:'homeGoalQtyTipp',
                render: function (data, type, row) {
                    if(row.resultTip ==null)
                    {
                        return '-'
                    }else{
                        return row.homeGoalQtyTipp +" - " +row.visitorGoalQtyTipp
                    }
                },
                createdCell: function (td, cellData, rowData,) {
                    if (rowData.tippId.matchId.visitorGoalQty != null && rowData.tippId.matchId.homeGoalQty != null  ) {
                        if (rowData.visitorGoalQtyTipp == rowData.tippId.matchId.visitorGoalQty && rowData.homeGoalQtyTipp == rowData.tippId.matchId.homeGoalQty) {
                            $(td).css('background', 'green')
                        } else {
                            $(td).css('background', 'red')
                        }
                    }
                }
            }

        ]
    });

});


$(document).find("#tipps thead th:first-child, #table1 td:first-child").addClass('text-white');
