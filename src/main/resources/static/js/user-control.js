$(document).ready(function() {

    $.fn.DataTable.ext.pager.numbers_length = 5;

    var lang = {
        "sEmptyTable":     "Nincs rendelkezésre álló adat",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés ",
        "sZeroRecords":    "Nincs a keresésnek megfelelő találat",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };

    var dtTable = $('table#users').DataTable({
        'ajax': {
            'contentType': 'application/json',
            'url': '/user/list',
            'type': 'POST',
            'data': function (d) {
                return JSON.stringify(d);
            }
        },
        "pageLength": 10,
        'serverSide' : true,
        "processing": true,
        "scrollX": true,
        "order": [[ 0, "desc" ]],
        "language": lang,
        "stateSave": false,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Összes"]],
        "dom": '<"wrapper"ltip>',
        columns: [
            {
                data:'id'
            },
            {
                data:'username'
            },
            {
                data: 'email'
            },
            {
                data: 'active',
                render:function(data,type,row){
                    if(data==1)
                    {
                        return 'Aktív';
                    }
                    else
                    {
                        return 'Inaktív';
                    }
                }
            },
            {
                data: 'roles',
                render:function(data,type,row){
                    let role = '';
                    for (item of data)
                    {
                        role=role+item.role+' ';
                    }
                    return role;
                }
            },
            {
                data: 'id',
                render:function(data,type,row){
                    return '<span uk-icon="trash" class="ml-2 btn  delete" data-id='+data+'></span>' +
                        '<span class="ml-2 btn edit"  uk-icon="pencil" data-id='+data+'></span><span class="ml-2 btn ban" uk-icon="ban" data-id='+data+'></span>';
                }
            }

        ]
    });
    $('#add').submit(function(event) {

        let roles = [];
        let user = $('#USER').is(':checked') ?  {role_id:$('#USER').data('id'),role:'USER'} : undefined;
        let admin = $('#ADMIN').is(':checked') ?  {role_id:$('#ADMIN').data('id'),role:'ADMIN'} : undefined;
        let vip = $('#VIP').is(':checked') ?  {role_id:$('#VIP').data('id'),role:'VIP'} : undefined;
        if(user!=undefined)
        {
            roles.push(user);
        }
        if(admin!=undefined)
        {
            roles.push(admin);
        }
        if(vip!=undefined)
        {
            roles.push(vip);
        }

        $.ajax({
            url: "/user/add",
            method: "POST",
            contentType: 'application/json',
            data:  JSON.stringify({
                "username": $("#username").val(),
                "password": $("#password").val(),
                "password2": $("#password2").val(),
                "email": $("#email").val(),
                "roles":roles
            }),
            success: function (response) {
                if (response.type == "success"){
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    document.getElementById("add").reset();
                    dtTable.ajax.reload();
                    $('#addModal').modal('hide')
                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                }
            }
        });
        event.preventDefault();
    });

    $(document).on('click','.delete',function () {

        var id = $(this).data('id');
        swal({
            title: "Biztos törlöd a felhasználót?",
            text: "",
            icon: "warning",
            closeOnClickOutside:false,
            dangerMode: true,
            buttons:['Mégsem','Törlés']
        }).then(function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    method: 'DELETE',
                    url : '/user/delete/'+id,
                    processData: false,
                    contentType: false,
                    destroy: true,
                    success : function (data) {
                        if(data.type=='success')
                        {
                            swal({
                                title: data.msg,
                                text: '',
                                icon: "success",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                        else if(data.type=='error')
                        {
                            swal({
                                title: data.msg,
                                text: '',
                                icon: "error",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                    }
                });
            }
        });
    });
    $(document).on('click','.ban',function () {

        var id = $(this).data('id');
        swal({
            title: "Biztos megváltoztatod a státuszt?",
            text: "",
            icon: "warning",
            closeOnClickOutside:false,
            dangerMode: true,
            buttons:['Mégsem','Módosítás']
        }).then(function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    method: 'POST',
                    url : '/user/activate/'+id,
                    processData: false,
                    contentType: false,
                    destroy: true,
                    success : function (data) {
                        if(data.type=='success')
                        {
                            swal({
                                title: data.msg,
                                text: '',
                                icon: "success",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                        else if(data.type=='error')
                        {
                            swal({
                                title: data.msg,
                                text: '',
                                icon: "error",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                    }
                });
            }
        });
    });

    function doSearchOnTable(rowId, control){

        if ($(control).val() == "") {
            dtTable
                .column(rowId)
                .search("")
                .draw();
        } else {
            dtTable
                .column(rowId)
                .search($(control).val())
                .draw();
        }
    }

    $("body").on("change","#search_id",function () {

        doSearchOnTable(0, this);
    });
    $("body").on("change","#search_username",function () {

        doSearchOnTable(1, this);
    });
    $("body").on("change","#search_email",function () {

        doSearchOnTable(2, this);
    });

    $('#edit').submit(function(event) {
        let roles = [];
        let user = $('#edit_USER').is(':checked') ?  {role_id:$('#edit_USER').data('id'),role:'USER'} : undefined;
        let admin = $('#edit_ADMIN').is(':checked') ?  {role_id:$('#edit_ADMIN').data('id'),role:'ADMIN'} : undefined;
        let vip = $('#edit_VIP').is(':checked') ?  {role_id:$('#edit_VIP').data('id'),role:'VIP'} : undefined;
        if(user!=undefined)
        {
            roles.push(user);
        }
        if(admin!=undefined)
        {
            roles.push(admin);
        }
        if(vip!=undefined)
        {
            roles.push(vip);
        }
        event.preventDefault();
        $.ajax({
            url: "/user/edit",
            method: "POST",
            contentType: 'application/json',
            data: JSON.stringify({
                "id":  $("#editId").val(),
                "username":  $("#editUsername").val(),
                "password": $("#editPassword").val(),
                "password2": $("#editPassword2").val(),
                "email": $("#editEmail").val(),
                "roles":roles,
            }),
            success: function (response) {
                if (response.type == "success"){
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                    $('#editModal').modal('hide')
                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                }
            }
        });
    });

    $(document).on('click',".edit",function () {
        $('#editModal').modal('show')
        self = $(this);
        $.ajax({
            url: "/user/get",
            method: "GET",
            data: {"id": self.data("id")},
            success: function (data) {
                $("#editId").val(data.id);
                $("#editUsername").val(data.username);
                $("#editEmail").val(data.email);

                for(item of data.roles)
                {
                    if(item.role=='ADMIN')
                    {
                        $('#edit_ADMIN').prop('checked', true);
                    }
                    else if(item.role=='USER')
                    {
                        $('#edit_USER').prop('checked', true);
                    }
                }
            }
        })
    });
});