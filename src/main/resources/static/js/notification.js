$(document).ready(function() {

    var lang = {
        "sEmptyTable":     "Nincs rendelkezésre álló adat",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés ",
        "sZeroRecords":    "Nincs a keresésnek megfelelő találat",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };

    var dtTable = $('table#notifications').DataTable({
        'ajax': {
            'contentType': 'application/json',
            'url': '/notification/list',
            'type': 'POST',
            'data': function (d) {
                return JSON.stringify(d);
            }
        },
        "pageLength": 10,
        'serverSide' : true,
        "processing": true,
        "scrollX": true,
        "order": [[ 0, "desc" ]],
        "language": lang,
        "stateSave": false,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Összes"]],
        "dom": '<"wrapper"ltip>',
        columns: [
            {
                data:'id'
            },
            {
                data:'title'
            },
            {
                data: 'message'
            },
            {
                data: 'user.username'
            },
            {
                data: 'sendTime'
            }
        ]
    });

    function doSearchOnTable(rowId, control){

        if ($(control).val() == "") {
            dtTable
                .column(rowId)
                .search("")
                .draw();
        } else {
            dtTable
                .column(rowId)
                .search($(control).val())
                .draw();
        }
    }

    $("body").on("change","#search_title",function () {

        doSearchOnTable(1, this);
    });
    $("body").on("change","#search_message",function () {

        doSearchOnTable(2, this);
    });
    $("body").on("change","#search_who",function () {

        doSearchOnTable(3, this);
    });
    $("body").on("change","#search_when",function () {

        doSearchOnTable(4, this);
    });

    $('#reflinkEdit').submit(function (event){

        $.ajax({
            url: "/reflink/save",
            method: "POST",
            contentType: 'application/json',
            data:  JSON.stringify({
                "id":$("#reflinkId").text(),
                "text": $("#buttonText").val(),
                "link": $("#reflinkText").val(),
                "streamReflink": $("#streamReflink").val(),
                "matchReflink": $("#matchReflink").val()
            }),
            success: function (response) {
                if (response.type == "success"){
                    swal({
                        title: "Sikeres mentés",
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    $('#reflinkModal').modal('hide');

                } else if(response.type =="error") {
                    swal({
                        title: "Sikertelen mentés!",
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });

                }
            }
        });

        event.preventDefault();

    });

    $('#add').submit(function(event) {


        $.ajax({
            url: "/notification/add",
            method: "POST",
            contentType: 'application/json',
            data:  JSON.stringify({
                "title": $("#title").val(),
                "message": $("#message").val()
            }),
            success: function (response) {
                if (response.type == "success"){
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    document.getElementById("add").reset();
                    dtTable.ajax.reload();
                    $('#addModal').modal('hide');
                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                }
            }
        });
        event.preventDefault();
    });


    $(document).on('click','#morgenMail', function (){


        // swal({
        //     title: 'Are you sure?',
        //     text: "You won't be able to revert this!",
        //     icon: 'warning',
        //     showCancelButton: true,
        //     confirmButtonColor: '#3085d6',
        //     cancelButtonColor: '#d33',
        //     confirmButtonText: 'Yes, delete it!'
        // }).then((result) => {
        //     if (result.isConfirmed) {
        //         Swal.fire(
        //             'Deleted!',
        //             'Your file has been deleted.',
        //             'success'
        //         )
        //     }
        // })


        swal({
            title: "Biztos kiküldöd a reggeli mailek?",
            text: "",
            icon: "warning",
            closeOnClickOutside:false,
            dangerMode: true,
            buttons:['Mégsem','Küldés']
        }).then(function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    method: 'POST',
                    url : '/user/send-mail',
                    processData: false,
                    contentType: false,
                    destroy: true,
                    success : function (data) {
                        if(data.type=='success')
                        {
                            swal({
                                title: data.msg,
                                text: '',
                                icon: "success",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                        else if(data.type=='error')
                        {
                            swal({
                                title: data.msg,
                                text: '',
                                icon: "error",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                    }
                });
            }
            else
            {

            }
        });

    });

    $(document).on('click','.delete',function () {

        var id = $(this).data('id');
        swal({
            title: "Biztos törlöd a felhasználót?",
            text: "",
            icon: "warning",
            closeOnClickOutside:false,
            dangerMode: true,
            buttons:['Mégsem','Törlés']
        }).then(function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    method: 'DELETE',
                    url : '/user/delete/'+id,
                    processData: false,
                    contentType: false,
                    destroy: true,
                    success : function (data) {
                        if(data.type=='success')
                        {
                            swal({
                                title: data.msg,
                                text: '',
                                icon: "success",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                        else if(data.type=='error')
                        {
                            swal({
                                title: data.msg,
                                text: '',
                                icon: "error",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                    }
                });
            }
        });
    });
    $(document).on('click','.ban',function () {

        var id = $(this).data('id');
        swal({
            title: "Biztos megváltoztatod a státuszt?",
            text: "",
            icon: "warning",
            closeOnClickOutside:false,
            dangerMode: true,
            buttons:['Mégsem','Módosítás']
        }).then(function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    method: 'POST',
                    url : '/user/activate/'+id,
                    processData: false,
                    contentType: false,
                    destroy: true,
                    success : function (data) {
                        if(data.type=='success')
                        {
                            swal({
                                title: data.msg,
                                text: '',
                                icon: "success",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                        else if(data.type=='error')
                        {
                            swal({
                                title: data.msg,
                                text: '',
                                icon: "error",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                    }
                });
            }
        });
    });





    $('#edit').submit(function(event) {
        let roles = [];
        let user = $('#edit_USER').is(':checked') ?  {role_id:$('#edit_USER').data('id'),role:'USER'} : undefined;
        let admin = $('#edit_ADMIN').is(':checked') ?  {role_id:$('#edit_ADMIN').data('id'),role:'ADMIN'} : undefined;
        if(user!=undefined)
        {
            roles.push(user);
        }
        if(admin!=undefined)
        {
            roles.push(admin);
        }
        event.preventDefault();
        $.ajax({
            url: "/user/edit",
            method: "POST",
            contentType: 'application/json',
            data: JSON.stringify({
                "id":  $("#editId").val(),
                "username":  $("#editUsername").val(),
                "password": $("#editPassword").val(),
                "password2": $("#editPassword2").val(),
                "email": $("#editEmail").val(),
                "roles":roles,
            }),
            success: function (response) {
                if (response.type == "success"){
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                    $('#editModal').modal('hide')
                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                }
            }
        });
    });

    $(document).on('click',".edit",function () {
        $('#editModal').modal('show')
        self = $(this);
        $.ajax({
            url: "/user/get",
            method: "GET",
            data: {"id": self.data("id")},
            success: function (data) {
                $("#editId").val(data.id);
                $("#editUsername").val(data.username);
                $("#editEmail").val(data.email);

                for(item of data.roles)
                {
                    if(item.role=='ADMIN')
                    {
                        $('#edit_ADMIN').prop('checked', true);
                    }
                    else if(item.role=='USER')
                    {
                        $('#edit_USER').prop('checked', true);
                    }
                }
            }
        })
    });
});