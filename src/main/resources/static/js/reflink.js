
const nextButton = $('#nextButton');
const mailForm = $('#mail');
const registrationDiv = $('#registrationDiv');
const regForm = $('#reflinkRegistration');


$(document).ready(function () {

    $.fn.DataTable.ext.pager.numbers_length = 5;

    mailForm.on('submit',function (event) {

        event.preventDefault();

        $.ajax({
            url: "/user/check-mail",
            method: "POST",
            data: {"mail": $('#email').val(),
            "id":$('#reflinkOwner').text()},
            success: function (response) {
                if (response.type == "success"){

                    nextButton.removeClass('d-flex');
                    nextButton.addClass('d-none');

                    registrationDiv.removeClass('d-none');
                    registrationDiv.addClass('d-flex');

                    $('#email').attr('readonly', true);

                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                }
            }

        });

    });

    regForm.on('submit',function (event) {

        event.preventDefault();

        let form = document.forms['reflinkRegistration'];
        let formData = new FormData(form);

        formData.append("email",$('#email').val());


        $.ajax({
            url: "/user/reflink-reg",
            method: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                if (response.type == "success"){

                        window.location.href = "/premium";

                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                }
            }

        });

    });
   

})
