$(document).ready(function(){

    function disableAllAndLoad()
    {
        $('#loader').addClass("loader");
        $('#loading').addClass("loading");
    };
    function enableAll()
    {
        $('#loader').removeClass("loader");
        $('#loading').removeClass("loading");

    };

    $(document).on('submit',"#forgotPw",function (event) {

        let form = document.forms['forgotPw'];
        let formData = new FormData(form);

            disableAllAndLoad();
            $.ajax({
                method:'POST',
                url: '/user/forgotPassword',
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                    if(data.type=='success')
                    {
                        swal({
                            title: ""+data.msg,
                            text: "",
                            icon: "success",
                            closeOnClickOutside:false,
                        });
                        enableAll();
                        $('#forgotPw').trigger('reset');
                    }
                    else if(data.type=='error')
                    {
                        swal({
                            title: "Hiba!",
                            text: ""+data.msg,
                            icon: "warning",
                            closeOnClickOutside:false,
                        });
                        enableAll();
                    }
                },
                error: function (error) {
                    swal({
                        title: "Hiba lépett fel",
                        text: ""+error.responseJSON['message'],
                        icon: "warning",
                        closeOnClickOutside:false,
                        dangerMode: true,
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            enableAll();
                        }
                    });
                }
            });

        event.preventDefault();
    });
});











