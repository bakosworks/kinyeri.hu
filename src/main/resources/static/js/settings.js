var useragentid = null;

document.addEventListener('DOMContentLoaded', (event) => {
    if(window.location.href.indexOf('?notif') != -1) {
        $('#notification-settings').click();
    }
});

document.addEventListener('DOMContentLoaded', (event) => {
    if(window.location.href.indexOf('?altalanos') != -1) {
        $('#profile-settings').click();
    }
});

$(document).ready(function() {

    $.fn.DataTable.ext.pager.numbers_length = 5;

    var lang = {
        "sEmptyTable":     "Nincs rendelkezésre álló adat",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés ",
        "sZeroRecords":    "Nincs a keresésnek megfelelő találat",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };

    var dtTable = $('table#usersPayments').DataTable({
        'ajax': {
            'contentType': 'application/json',
            'url': '/payment/listUser',
            'type': 'POST',
            'data': function (d) {
                return JSON.stringify(d);
            }
        },
        "pageLength": 10,
        'serverSide' : true,
        "processing": true,
        "scrollX": true,
        "order": [[ 0, "desc" ]],
        "language": lang,
        "stateSave": false,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Összes"]],
        "dom": '<"wrapper"ltp>',
        columns: [
            {
                data:'packageId.name',
                render:function(data,type,row){

                    if(row.discount!=null)
                    {
                        return '<span>'+row.packageId.name+' '+ (row.packageId.price*0.7)+' Ft<span>';
                    }
                    else
                    {
                        return '<span>'+row.packageId.name+' '+ row.packageId.price+' Ft <span>';

                    }


                }

            },
            {
                data:'createdTime'
            },
            {
                data: 'activateTime',
                render:function(data,type,row){
                    if(data==null)
                    {
                        return ' ';
                    }
                    else
                    {
                        return data;
                    }
                }
            },
            {
                data: 'status',
                render:function(data,type,row){
                    if(data== "IN_PROCESS"){
                        return '<button type="button" class="btn btn-danger " >Fizetésre vár</button>'
                    }else if(data== "DONE"){
                        return '<button type="button" class="btn btn-success " >Fizetve</button>'
                    }else{
                        return '';
                    }
                }

            },
            {
                data: 'id',
                render:function(data,type,row){
                    if(row.status== "IN_PROCESS"){
                        return '<button type="button" data-id='+data+' class="btn btn-warning pay" >Befizetés</button>'
                    }else if(row.status== "DONE"){
                        return '<button type="button" data-id='+data+' class="btn btn-success " >Befizetve</button>'
                    }else{
                        return '';
                    }

                }
            }

        ]
    });


    $(document).on('click',".pay",function () {

        self = $(this);
        $.ajax({
            url: "/payment/get",
            method: "GET",
            data: {"id": self.data("id")},
            success: function (data) {
                $("#package").text(data.packageId.name);

                if(data.discount!=null)
                {
                    $('#packagePrice').text(data.packageId.price*0.7+' Ft');
                }
                else
                {
                    $('#packagePrice').text(data.packageId.price+' Ft');
                }

                $("#email").text(data.userId.email);
                $("#address").text(data.userId.country+" "+data.userId.zipCode+" "+data.userId.city+" "+data.userId.address);
            }
        });
        $('#payModal').modal('show');
    });

    $(document).on('click',".activatedEmail",function () {
        self = $(this);
        swal({
            title: "Feliratkozol a levelél értesítésre?",
            text: "",
            icon: "warning",
            closeOnClickOutside:false,
            dangerMode: true,
            buttons:['Mégsem','Feliratkozás']
        }).then(function(isConfirm) {
            if(isConfirm){
                $.ajax({
                    url: "/user/subscribe",
                    method: "POST",
                    data: {"id": self.data("id")},
                    success: function (response) {
                        if (response.type == "success") {
                            $('#activated').addClass("d-none");
                            $('#disable').removeClass("d-none");
                            swal({
                                title: response.msg,
                                text: '',
                                icon: "success",
                                closeOnClickOutside: false,
                                dangerMode: true
                            });
                        } else if (response.type == "error") {
                            swal({
                                title: response.msg,
                                text: '',
                                icon: "error",
                                closeOnClickOutside: false,
                                dangerMode: true
                            });
                        }
                    }
                })
            }
        });
    });

    $(document).on('click',".disabledEmail",function () {
        self = $(this);
        swal({
            title: "Biztos leiratkozol a levelél értesítésről?",
            text: "",
            icon: "warning",
            closeOnClickOutside:false,
            dangerMode: true,
            buttons:['Mégsem','Leiratkozás']
        }).then(function(isConfirm) {
            if(isConfirm){
                $.ajax({
                    url: "/user/unSubscribe",
                    method: "POST",
                    data: {"id": self.data("id")},
                    success: function (response) {
                        if (response.type == "success") {
                            $('#disable').addClass("d-none");
                            $('#activated').removeClass("d-none");

                            swal({
                                title: response.msg,
                                text: '',
                                icon: "success",
                                closeOnClickOutside: false,
                                dangerMode: true
                            });
                        } else if (response.type == "error") {
                            swal({
                                title: response.msg,
                                text: '',
                                icon: "error",
                                closeOnClickOutside: false,
                                dangerMode: true
                            });
                        }
                    }
                })
            }
        });
    });


    $('#personalForm').submit(function(event) {
        event.preventDefault();

        var stringSplitName = $('#name').val().trim().split(' ');
        if (stringSplitName.length >= 2)
        {
            $.ajax({
                url: "/user/change-info",
                method: "POST",
                contentType: 'application/json',
                data:  JSON.stringify({
                    "mail": $("#mail").val(),
                    "city": $("#city").val(),
                    "street": $("#street").val(),
                    "name": $("#name").val()
                }),
                success: function (response) {
                    if (response.type == "success"){
                        swal({
                            title: response.msg,
                            text: '',
                            icon: "success",
                            closeOnClickOutside:false,
                            dangerMode: true
                        });
                    } else if(response.type =="error") {
                        swal({
                            title: response.msg,
                            text: '',
                            icon: "error",
                            closeOnClickOutside:false,
                            dangerMode: true
                        });

                    }
                }
            });
        }
        else
        {
            swal({
                title: "Hiba",
                text: 'A neved szóközzel kell elválasztani!',
                icon: "error",
                closeOnClickOutside:false,
                dangerMode: true
            });
        }


    });


       //Firstly this will check user id
        OneSignal.push(function() {
        OneSignal.getUserId().then(function(userId) {
            if(userId == null){
                document.getElementById('unsubscribe').style.display = 'none';
            }
            else{
                useragentid = userId;
                document.getElementById('unsubscribe').style.display = '';
                OneSignal.push(["getNotificationPermission", function(permission){
                }]);
                OneSignal.isPushNotificationsEnabled(function(isEnabled) {
                    if (isEnabled){
                        document.getElementById('unsubscribe').style.display = '';
                        document.getElementById('subscribe').style.display = 'none';
                    }
                    else{
                        document.getElementById('unsubscribe').style.display = 'none';
                        document.getElementById('subscribe').style.display = '';
                    }
                });
            }
        });
    });
        //Secondly this will check when subscription changed
        OneSignal.push(function() {
        OneSignal.on('subscriptionChange', function (isSubscribed) {
            if(isSubscribed==true){
                OneSignal.getUserId().then(function(userId) {
                    useragentid = userId;
                }).then(function(){
                    OneSignalUserSubscription(useragentid);
                });
                document.getElementById('unsubscribe').style.display = '';
                document.getElementById('subscribe').style.display = 'none';
            }
            else if(isSubscribed==false){
                OneSignal.getUserId().then(function(userId) {
                    useragentid = userId;
                });
                document.getElementById('unsubscribe').style.display = 'none';
                document.getElementById('subscribe').style.display = '';
            }
            else{
                console.log('Unable to process the request');
            }
        });
    });

        ///* Bakos's@script *//
        //Check user enabled notifications or not
    OneSignal.push(function() {
        OneSignal.isPushNotificationsEnabled().then(function(isEnabled) {
            if (isEnabled)
                $('#notifEnable').removeClass('d-none');
            else
                $('#notifDisable').removeClass('d-none');

        });

    });
        ///*My script*//



});

function subscribeOneSignal(){
    if(useragentid !=null){
        OneSignal.setSubscription(true);
    }
    else{
        OneSignal.push(function() {
            OneSignal.showNativePrompt();
        });
    }
}
function unSubscribeOneSignal(){
    OneSignal.setSubscription(false);
}
function showNativePrompt(){
    OneSignal.push(function() {
        OneSignal.showNativePrompt();
    });

}
