    $(document).ready(function() {

    $.fn.DataTable.ext.pager.numbers_length = 5;

    $(document).on('click','#buy',function () {

        var id = $(this).data('package');
        $.ajax({
            url: "/package/get",
            method: "GET",
            data: {"id": id},
            success: function (data) {

                $("#package").text(data.name);

                swal({
                    title: "Biztos megvásárolod a "+data.name +" csomagot?",
                    text: "",
                    icon: "warning",
                    closeOnClickOutside:false,
                    buttons:['Mégsem','Igen']
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            method: 'POST',
                            url : '/payment/add/',
                            contentType: 'application/json',
                            data:  JSON.stringify({
                                "packageId": id,
                            }),
                            success : function (data) {
                                if(data.type=='success')
                                {
                                    $("#package").text(data.obj.name);

                                    if(data.obj.discount!=null)
                                    {
                                        $('#packagePrice').text(data.obj.packageId.price*0.7+' Ft');
                                    }
                                    else
                                    {
                                        $('#packagePrice').text(data.obj.packageId.price+' Ft');
                                    }

                                    swal({
                                        title: data.msg,
                                        text: '',
                                        icon: "success",
                                        closeOnClickOutside:false,
                                        dangerMode: true
                                    });
                                    $('#payModal').modal('show');

                                }
                                else if(data.type=='error')
                                {
                                    swal({
                                        title: data.msg,
                                        text: '',
                                        icon: "error",
                                        closeOnClickOutside:false,
                                        dangerMode: true
                                    });
                                }
                            }
                        });
                    }
                });
            }
        })

        event.preventDefault();
    });

});