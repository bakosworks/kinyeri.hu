$(document).ready(function() {

    $('#edit').submit(function(event) {
        $.ajax({
            url: "/bankDetail/edit",
            method: "POST",
            contentType: 'application/json',
            data: JSON.stringify({
                "id":  $("#id").val(),
                "name": $("#name").val(),
                "iban": $("#iban").val(),
                "bankNumber": $("#bankNumber").val(),
                "bankName": $("#bankName").val(),
                "phone": $("#phone").val(),
                "swiftCode": $("#swiftCode").val()

            }),
            success: function (response) {
                if (response.type == "success"){
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                }
            }

        });

        event.preventDefault();
    });


});