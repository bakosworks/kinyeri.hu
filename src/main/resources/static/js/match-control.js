$(document).ready(function() {

    $.fn.DataTable.ext.pager.numbers_length = 5;

    var lang = {
        "sEmptyTable":     "Nincs rendelkezésre álló adat",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés ",
        "sZeroRecords":    "Nincs a keresésnek megfelelő találat",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };


    let fasz=$('select').selectize({
        sortField: 'text',
        create:function (input, callback){
            $.ajax({
                url: '/team/add-by-select',
                type: 'POST',
                contentType: 'application/json',
                data:JSON.stringify({
                    "name":input
                }),
                success: function (result) {
                    if (result){
                        fasz[0].selectize.clearOptions();
                        fasz[0].selectize.addOption(result);
                        fasz[1].selectize.clearOptions();
                        fasz[1].selectize.addOption(result);
                        callback({text:input,value:input});
                    }
                }
            });
        },
        render: {
            option_create: function(data, escape) {
                return '<div class="create"><strong>' + escape(data.input) + ' hozzáadása</strong>&hellip;</div>';
            }
        },
    });


    var dtTable = $('table#matchs').DataTable({
        'ajax': {
            'contentType': 'application/json',
            'url': '/match/list',
            'type': 'POST',
            'data': function (d) {
                return JSON.stringify(d);
            }
        },
        "pageLength": 25,
        'serverSide' : true,
        "processing": true,
        "scrollX": true,
        "order": [[ 2, "desc" ]],
        "language": lang,
        "stateSave": false,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Összes"]],
        "dom": '<"wrapper"ltip>',
        columns: [
            {
                data:'homeTeam.name'
            },
            {
                data:'visitorTeam.name'
            },

            {
                data:'matchTime'
            },
            {
                data:'oddsH'
            },
            {
                data:'oddsD'
            },
            {
                data:'oddsV'
            },
            {
                data:'homeGoalQty',
                render: function (data, type, row) {
                    if(row.homeGoalQty ==null)
                    {
                        return '-'
                    }else{
                        return row.homeGoalQty
                    }

                }
            },
            {
                data:'visitorGoalQty',
                render: function (data, type, row) {
                    if(row.visitorGoalQty ==null)
                    {
                        return '-'
                    }else{
                        return row.visitorGoalQty
                    }

                }
            },
            {
                data:'result',
                render: function (data, type, row) {
                    if(row.result ==null)
                    {
                        return '-'
                    }else{
                        return row.result
                    }

                }

            },
            {
                data: 'id',
                render:function(data,type,row){
                    return '<span uk-icon="trash" class="ml-2 btn text-danger  delete" data-id='+data+'></span>' +
                        '<span class="ml-2 btn edit text-warning"  uk-icon="pencil" data-id='+data+'></span>' +
                        '<span class="ml-2 btn editResult text-success"  uk-icon="lock" data-id='+data+'></span>';
                }
            }

        ]
    });
    $('#add').submit(function(event) {
        event.preventDefault();
        $.ajax({
            url: "/match/add",
            method: "POST",
            contentType: 'application/json',
            data:  JSON.stringify({
                "hteam": $("#addHTeam  option:selected").text(),
                "vteam": $("#addVTeam  option:selected").text(),
                "oddsH": $("#HOdds").val(),
                "oddsD": $("#DOdds").val(),
                "oddsV": $("#VOdds").val(),
                "match": $("#match").val(),
                "matchReflink": $('#matchReflink').val()
            }),
            success: function (response) {
                if (response.type == "success"){
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                    $('#addModal').modal('hide');
                    document.getElementById("add").reset();
                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                }
            }
        });
        event.preventDefault();
    });

    $(document).on('click','.delete',function () {

        var id = $(this).data('id');
        swal({
            title: "Biztos törlöd a mérközést?",
            text: "",
            icon: "warning",
            closeOnClickOutside:false,
            dangerMode: true,
            buttons:['Mégsem','Törlés']
        }).then(function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    method: 'DELETE',
                    url : '/match/delete/'+id,
                    processData: false,
                    contentType: false,
                    destroy: true,
                    success : function (data) {
                        if(data.type=='success')
                        {
                            swal({
                                title: data.msg,
                                text: '',
                                icon: "success",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                        else if(data.type=='error')
                        {
                            swal({
                                title: data.msg,
                                text: '',
                                icon: "error",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                    }
                });
            }
        });
        event.preventDefault();
    });

    function doSearchOnTable(rowId, control){

        if ($(control).val() == "") {
            dtTable
                .column(rowId)
                .search("")
                .draw();
        } else {
            dtTable
                .column(rowId)
                .search($(control).val())
                .draw();
        }
    }

    $("body").on("change","#search_h_team",function () {

        doSearchOnTable(0, this);
    });
    $("body").on("change","#search_v_team",function () {

        doSearchOnTable(1, this);
    });

    $('#edit').submit(function(event) {

        $.ajax({
            url: "/match/edit",
            method: "POST",
            data: {
                "id":  $("#edit_matchId").val(),
                "hteam": $("#editHTeam  option:selected").text(),
                "vteam": $("#editVTeam  option:selected").text(),
                "oddsH": $("#editHOdds").val(),
                "oddsD": $("#editDOdds").val(),
                "oddsV": $("#editVOdds").val(),
                "match": $("#editMatch").val(),
                "reflink":$('#editMatchReflink').val(),
                "matchDate":$('#editMatch').val()
            },
            success: function (response) {
                if (response.type == "success"){
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                    $('#editModal').modal('hide')
                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                }
            }
        });
        event.preventDefault();
    });

    $(document).on('click',".edit",function () {
        $('#editModal').modal('show')
        self = $(this);
        $.ajax({
            url: "/match/get",
            method: "GET",
            data: {"id": self.data("id")},
            success: function (data) {
                $("#edit_matchId").val(data.id);
                $('#editHTeam').data('selectize').setValue(data.hteam);
                $('#editVTeam').data('selectize').setValue(data.vteam);
                $("#editHOdds").val(data.oddsH);
                $("#editDOdds").val(data.oddsD);
                $("#editVOdds").val(data.oddsV);
                $("#editMatch").val(data.match);
                $('#editMatchReflink').val(data.matchReflink);
            }
        })
    });

    $(document).on('click',".editResult",function () {
        $('#editResultModal').modal('show')
        self = $(this);
        $.ajax({
             url: "/match/get",
            method: "GET",
            data: {"id": self.data("id")},
            success: function (data) {
                $("#matchIdResult").val(data.id);
                $('#TeamHResult').data('selectize').setValue(data.hteam);
                $('#TeamVResult').data('selectize').setValue(data.vteam);
                $("#OddsHResult").val(data.oddsH);
                $("#OddsDResult").val(data.oddsD);
                $("#OddsVResult").val(data.oddsV);
                $("#MatchResult").val(data.match);
                $("#editHomeGoalResult").val(data.goalHQty);
                $("#editVisitorGoalResult").val(data.goalVQty);
                $('#editMatchResult').data('selectize').setValue(data.result);

            }
        })
    });

    $('#editResult').submit(function(event) {

        $.ajax({
            url: "/match/editResult",
            method: "POST",
            contentType: 'application/json',
            data: JSON.stringify({
                "id":  $("#matchIdResult").val(),
                "hteam": $("#TeamHResult  option:selected").text(),
                "vteam": $("#TeamVResult  option:selected").text(),
                "oddsH": $("#OddsHResult").val(),
                "oddsD": $("#OddsDResult").val(),
                "oddsV": $("#OddsVResult").val(),
                "match": $("#MatchResult").val(),
                "result": $("#editMatchResult").val(),
                "goalHQty": $("#editHomeGoalResult").val(),
                "goalVQty": $("#editVisitorGoalResult").val(),
                "matchReflink" : $('#editMatchReflink').val()
            }),
            success: function (response) {
                if (response.type == "success"){
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                }
            }
        });
        event.preventDefault();
    });
});