$(document).ready(function() {

    $.fn.DataTable.ext.pager.numbers_length = 5;

    var lang = {
        "sEmptyTable":     "Nem igényeltél még előfizetést",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés ",
        "sZeroRecords":    "Nem igényeltél még előfizetést",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };




    $(document).on('click',".pay",function () {
        $('#payModal').modal('show')
        self = $(this);
        $.ajax({
            url: "/payment/get",
            method: "GET",
            data: {"id": self.data("id")},
            success: function (data) {
                $("#package").text(data.packageId.name);
                $("#email").text(data.userId.email);
                $("#address").text(data.userId.country+" "+data.userId.zipCode+" "+data.userId.city+" "+data.userId.address);
            }
        })
    });



});