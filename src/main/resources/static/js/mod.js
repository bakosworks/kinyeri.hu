$(document).ready(function (){

    $(document).on('click','.kinyeri-username',function () {
        let current=this;
        const id=$(this).attr('id');
        if (!$(current).hasClass("banned")){
            swal({
                title: "Biztos bannolod a felhasználót?",
                text: "",
                icon: "warning",
                closeOnClickOutside:false,
                buttons:['Mégsem','Igen']
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        method: 'POST',
                        url : '/user/ban/'+id,
                        contentType: 'application/json',
                        success : function (data) {
                            if(data.type=='success')
                            {
                                swal({
                                    title: data.msg,
                                    text: '',
                                    icon: "success",
                                    closeOnClickOutside:false,
                                    dangerMode: true
                                });
                                $(current).addClass("banned");

                            }
                            else if(data.type=='error')
                            {
                                swal({
                                    title: data.msg,
                                    text: '',
                                    icon: "error",
                                    closeOnClickOutside:false,
                                    dangerMode: true
                                });
                            }
                        }
                    });
                }
            });

        }else{
            swal({
                title: "Biztos visszavonod a bant?",
                text: "",
                icon: "warning",
                closeOnClickOutside:false,
                buttons:['Mégsem','Igen']
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        method: 'POST',
                        url : '/user/unban/'+id,
                        contentType: 'application/json',
                        success : function (data) {
                            if(data.type=='success')
                            {
                                swal({
                                    title: data.msg,
                                    text: '',
                                    icon: "success",
                                    closeOnClickOutside:false,
                                    dangerMode: true
                                });
                                $(current).removeClass("banned");

                            }
                            else if(data.type=='error')
                            {
                                swal({
                                    title: data.msg,
                                    text: '',
                                    icon: "error",
                                    closeOnClickOutside:false,
                                    dangerMode: true
                                });
                            }
                        }
                    });
                }
            });
        }

    });




});