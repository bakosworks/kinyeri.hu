const active=$('#active-all-time');
const type=$('#type');
const statisticBody=$('#pc-content');
const phoneBody = $('#phone-content');
const rewards= ["300.000","200.000","150.000","100.000","100.000","30.000","30.000","30.000","30.000","30.000"];

let countOfVoter = $('#voteCountSpan').text();
let money = (1000*countOfVoter)*0.7;
let calculatedMoney =[(money*0.3),(money*0.2),(money*0.15),(money*0.1),(money*0.1),(money*0.03),(money*0.03),(money*0.03),(money*0.03),(money*0.03)];

$(document).ready(function () {



    $.fn.DataTable.ext.pager.numbers_length = 5;

    getData();

    $("body").on("change","#active-all-time",function () {
        getData();
    });

    $("body").on("change","#type",function () {
        getData();
    });


    let date=new Date();
    jQuery($('#kinyeri-countdown')).countdown(new Date(date.getFullYear(), date.getMonth() + 1, 1),function(event) {
        $(this).html(event.strftime('%D nap %H óra %M perc %S mp'));
    });

})
function getData() {
    let activeOrAllTime = active.val();
    let typeVal = type.val();
    let typeEnum;
    if (typeVal===undefined){
        typeEnum=activeOrAllTime;
    }else{
        typeEnum=typeVal+activeOrAllTime;
    }
    $.ajax({
        url: "/top-statistic/get-statistic/"+typeEnum,
        method: "GET",
        success: function (data) {


            if(data.length == 0)
            {
                statisticBody.html("<div class='m-3 w-100 text-center'><p class='text-dark p-3 h3'>Nincs megjelenítendő adat</p></div>");
                phoneBody.html("<div class='m-3 w-100 text-center'><p class='text-dark h3'>Nincs megjelenítendő adat</p></div>");

            }
            else
            {
                let reward="";
                if(window.matchMedia("(max-width: 767px)").matches){
                    let phtml = "";
                    phoneBody.html(phtml);

                    let rowId = 0;

                    data.forEach((element,ind) => {
                        if(typeEnum==='ALL_TIME'){
                            reward="<span class='d-flex ml-3 mb-1'> Jutalom: " +rewards[ind]+" Ft</span>";

                            let realValue = currency(calculatedMoney[ind], { symbol: "", separator: ".", precision:0 }).format();

                            reward=reward+"<span class='d-flex ml-3 text-secondary'> Vigaszdíj: " +realValue+" Ft</span>";
                        }

                        let sHtml = "";
                        if(element.best!=null)
                        {

                           element.best.forEach( element => {
                             sHtml = sHtml.concat("<div class='col-md-12'>"+element+"<span class='kinyeri-best'>&nbsp;</span></div>");
                          });
                        }

                        let fireIcon = "";

                        if(ind<=2 && typeEnum==='ALL_TIME')
                        {
                            fireIcon = "<i class=\"fas fa-fire kinyeri-orange\"></i>";
                        }


                        var mobile = " <div class=\"item-head mb-2 rounded w-100 bg-white text-center\" data-toggle=\"collapse\" data-target=#h_" + rowId + " aria-expanded=\"false\">\n" +
                            "                <div class='d-flex justify-content-between'>" +
                            "" +
                            "<div class='my-auto text-left'><a class=\"bg-white rounded-top text-center bg-white ml-3 text-dark text-decoration-none  pb-2\"  data-toggle=\"collapse\" data-target=#h_" + rowId + " aria-expanded=\"false\" aria-controls=\"multiCollapseExample2\">"+element.user.username+" "+fireIcon+"<a class='fas fa-external-link-alt text-dark ml-3' href='/profil/"+element.user.username+"'></a></a>"+reward+"</div><div class='pt-2 pb-2'><a class=\"fas fa-chevron-down mr-2\"></a></div></div>" +                            "                <div class=\"col p-0 m-0 bg-white border-top rounded-bottom\">" +

                            "                    <div class=\"collapse multi-collapse\" id=" + "h_"+rowId +
                            "                        <div class=\"row p-0 m-0\">" +
                            "                            <div class=\"col-md-12 m-0 mt-2 pb-2 border-bottom text-center\">" +
                            "                                <span class='mb-2'>Tippek száma</span>" +
                            "                                <div class=\"tippNumb\">" +element.user.tipNum+
                            "                                </div>" +
                            "                            </div>" +
                            "                            <div class=\"col-md-12 m-0 pb-2 mt-2 border-bottom text-center\">" +
                            "                                <span class='mb-2'>Összesített rálátás</span>" +
                            "                                <div class=\"winRate mt-2\">" +element.user.boostedWinRate +"%<span class='kinyeri-winrate'>&nbsp;</span>"+
                            "                                </div>" +
                            "                            </div>" +
                            "                            <div class=\"col-md-12 m-0 pb-2 mt-2 border-bottom text-center\">" +
                            "                                <span class='mb-2'>Amiben a legjobb</span>" +
                            "                                <div class=\"row m-0 p-0 mt-2\">" +sHtml+""+
                            "                                </div>" +
                            "                            </div>" +
                            "                            <div class=\"col-md-12 m-0 mt-2 pb-2 border-bottom text-center\">" +
                            "                                <span class='mb-2'>Diagram</span>" +
                            " <div class=\"chart-container-phone\">" +
                            "                            <canvas class=\"diagram-canvas mt-2\" id=all-time-"+element.user.id+" ></canvas>" +
                            "                            </div>" +
                            "                            </div>" +
                            "                            <div class=\"col-md-12 m-0 pb-3 mt-2 mb-2 border-bottom text-center\">" +
                            "                                <span>Friss tippek</span>" +
                            "                                <div class=\"newTips mt-2\">"+
                            "                <a href='/profil/"+element.user.username+"' class='btn btn-success btn-success-custom text-decoration-none text-white' >"+element.pending+"</a>\n" +
                            "                                    " +
                            "                                </div>" +
                            "                            </div>" +
                            "                        </div>" +
                            "                    </div>" +
                            "                </div>" +
                            "            </div>"

                            phoneBody.append(mobile);
                            generateChart(element);

                           rowId++;

                    });

                }
                else
                {

                    let html="";
                    statisticBody.html(html);
                    data.forEach((element, ind) =>{

                        let sHtml="";
                        element.best.forEach( element => {
                            sHtml = sHtml.concat(element+", ");
                        });
                        if (sHtml.length>2){
                           sHtml = sHtml.substring(0,sHtml.length-2);
                        }
                        let fireIcon = "";

                        if(ind<=2)
                        {
                            fireIcon = "<i class=\"fas fa-fire kinyeri-orange mr-2\"></i>";
                        }
                        if(typeEnum==='ALL_TIME'){
                            reward=fireIcon+"<span  class='d-flex'> Jutalom: " +rewards[ind]+" Ft</span>";

                            let realValue = currency(calculatedMoney[ind], { symbol: "", separator: ".", precision:0 }).format();

                            reward=reward+"<span class='d-flex text-secondary'> Vigaszdíj: " +realValue+" Ft</span>";
                        }

                        html=
                            "<div class='kinyeri-ranking row p-0 m-0 pt-2 pb-2 mb-2'>"+
                                         "<div class=\"col-md-2 text-left my-auto\">\n" +

                            "            <span class='font-weight-bolder'>"+(ind+1)+". </span><span class='ml-2'><a class='text-decoration-none' href='/profil/"+element.user.username+"'>"+element.user.username+"</a></span>\n"+reward+
                            "            </div>\n" +
                            "            <div class=\"col-md-1 my-auto text-center\">\n" +
                            "                <span class=''>"+element.user.tipNum+"</span>\n" +
                            "            </div>\n" +
                            "            <div class=\"col-md-2 my-auto text-center\">\n" +
                            "                <span class=''>"+element.user.boostedWinRate+"%</span>\n" +"<span class='kinyeri-winrate'>&nbsp;</span>"+
                            "            </div>\n" +
                            "            <div class=\"col-md-2 my-auto text-center\">\n" +
                            "                <span>"+sHtml+"</span>" +"<span class='kinyeri-best'>&nbsp;</span>"+
                            "            </div>\n" +
                            "            <div class=\"col-md-4 my-auto \">\n" +
                            "                            <div class=\"chart-container\">" +
                            "                            <canvas class=\"diagram-canvas\" id=all-time-"+element.user.id+" ></canvas>" +
                            "                            </div>" +
                            "            </div>\n" +
                            "            <div class=\"col-md-1 my-auto text-center\">\n" +
                            "                <a href='/profil/"+element.user.username+"' class='btn btn-success btn-success-custom text-decoration-none text-white' >"+element.pending+"</a>\n" +
                            "            </div>"+
                            "</div>";

                        statisticBody.append(html);
                        generateChart(element);
                    });
                }




            }


        }
    })

}

function generateChart(stat) {
    let ctx = document.getElementById('all-time-'+stat.user.id);
    let myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels:stat.labels,

            datasets:

                stat.datasets


        },
        options: {
            maintainAspectRatio:false,
            legend:{
                display:false
            },
            tooltip:{
                enabled:false,

            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        suggestedMin: 50,
                        display: false,
                        suggestedMax: 110
                    }
                }]
            },
            animation:{
                duration: 1,
                onComplete: function() {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;

                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function(dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function(bar, index) {
                            if (dataset.data[index] > 0) {
                                var data = dataset.data[index];
                                ctx.fillText(data, bar._model.x, bar._model.y);
                            }
                        });
                    });
                }
            }
        }
    });
}