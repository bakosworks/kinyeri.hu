
const copyButtonUsername = $('#copyButtonUsername');
const copyButtonGeneric = $('#copyButtonGeneric');
const requestPayment = $('#requestPayment');
const currentBalance = $('#currentBalance');
const balanceInfo = $('#balanceInfo');
const formRequestPayment = $('#formRequestPayment');
const requestBalance = $('#requestBalance');
const requestPaymentAlert = $('#requestPaymentAlert');
const addPaymentAddress = $('#addPaymentAddress');
const newAddressForm = $('#newAddress');
const editAddress = $('#editAddress');
const editPaymentAddress = $('#editPaymentAddress');
const editSkrillAddress = $('#editSkrillAddress');
const editPaypalAddress = $('#editPaypalAddress');
const editNewAddress = $('#editNewAddress');



$(document).ready(function () {

    $.fn.DataTable.ext.pager.numbers_length = 5;

    copyButtonUsername.on('click',function (event) {

        $('#toolTipUsername').show(200);

        var copyText = document.getElementById("reflinkUsername");
        var textArea = document.createElement("textarea");
        textArea.value = copyText.textContent;
        document.body.appendChild(textArea);
        textArea.select();
        document.execCommand("Copy");
        textArea.remove();

        var tooltip = document.getElementById("toolTipUsername");

    });
    copyButtonGeneric.on('click',function (event) {

        $('#toolTipGeneric').show(200);

        var copyText = document.getElementById("reflinkGeneric");
        var textArea = document.createElement("textarea");
        textArea.value = copyText.textContent;
        document.body.appendChild(textArea);
        textArea.select();
        document.execCommand("Copy");
        textArea.remove();

        var tooltip = document.getElementById("toolTipGeneric");




    });

    copyButtonGeneric.on('mouseout',function (event) {

        $('#toolTipGeneric').hide(300);
    });

    copyButtonUsername.on('mouseout',function (event) {

        $('#toolTipUsername').hide(300);
    });



    requestPayment.on("click",function (event){

        $("#requestMail").html("");

        $.ajax({
            url: "/user/get-by-name",
            method: "POST",
            contentType: 'application/json',
            success: function (response) {
                if (response != null){

                    if (response.skrillAddress!=null && response.skrillAddress!="")
                    {
                        var skrill = document.createElement('option');
                        skrill.value = 'SKRILL';
                        skrill.innerHTML = 'Skrill: '+response.skrillAddress;

                        $('#requestMail').append(skrill);
                    }


                    if (response.paypalAddress!=null && response.paypalAddress!="")
                    {
                        var paypal = document.createElement('option');
                        paypal.value = 'PAYPAL';
                        paypal.innerHTML = 'PayPal: '+response.paypalAddress;

                        $('#requestMail').append(paypal);
                    }





                    currentBalance.text("");
                    currentBalance.text(balanceInfo.text());
                    $('#requestPaymentModal').modal('show');

                }else {
                    swal({
                        title: 'Server hiba',
                        text: 'Hiba a címek lekérdezése során!',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                }
            }

        });





    });



    addPaymentAddress.on("click",function (event) {

        $('#addNewAddress').modal('show');

    });

    function modifyPaymentAddress(skrill,paypal,event) {

        event.preventDefault();

        $.ajax({
            url: "/user/payment-addresses",
            method: "POST",
            data: {
                "paypal":  paypal,
                "skrill": skrill,
            },
            success: function (response) {
                if (response.type == "success"){
                    swal({
                        title: 'Beállítások',
                        text: 'Sikeres mentés',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                } else if(response.type =="error") {
                    swal({
                        title: 'Beállítások',
                        text: 'Hiba mentés során',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                }
            }

        });

    }

    newAddressForm.on("submit",function (event){

        modifyPaymentAddress($("#skrillAddress").val(),$("#paypalAddress").val(),event);


    });

    editAddress.on("submit",function (event){

        event.preventDefault();

        modifyPaymentAddress($("#editSkrillAddress").val(),$("#editPaypalAddress").val(),event);

    });


    editPaymentAddress.on('click',function (event)
    {
        $.ajax({
            url: "/user/get-by-name",
            method: "POST",
            contentType: 'application/json',
            success: function (response) {
                if (response != null){


                    editSkrillAddress.val(response.skrillAddress);


                    editPaypalAddress.val(response.paypalAddress);
                    editNewAddress.modal('show');

                }else {
                    swal({
                        title: 'Server hiba',
                        text: 'Hiba a címek lekérdezése során!',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                }
            }

        });
    });

    var lang = {
        "sEmptyTable":     "Nincs rendelkezésre álló adat",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés ",
        "sZeroRecords":    "Nincs a keresésnek megfelelő találat",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };

    var userRequestPayments = $('table#userRequestPayments').DataTable({
        'ajax': {
            'contentType': 'application/json',
            'url': '/ref-payment/list-user',
            'type': 'POST',
            'data': function (d) {
                return JSON.stringify(d);
            }
        },
        "pageLength": 10,
        'serverSide' : true,
        "processing": true,
        "scrollX": true,
        "order": [[ 5, "desc" ]],
        "language": lang,
        "stateSave": false,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Összes"]],
        "dom": '<"wrapper"ltip>',
        columns: [
            {
                data:'reqTime'
            },
            {
                data:'email'
            },
            {
                data:'refBalance',
                render:function(data,type,row){
                    return data+' Ft';
                }

            },
            {
                data: 'payType'
            },
            {
                data: 'paymentStatus',
                render:function(data,type,row){

                    if (data=="IN_PROCESS")
                    {
                        return 'Folyamatban';
                    }
                    else if(data=="DENIED" && row.userReverted!= null &&  row.userReverted.username == row.reqUser.username)
                    {
                        return 'Visszavonva';
                    }
                    else if (data=="DENIED")
                    {
                        return 'Elutasítva';
                    }
                    else if (data=="DONE")
                    {
                        return 'Kifizetve';
                    }
                    else
                    {
                        return  '';
                    }
                }
            },
            {
                data: 'finishTime',
                render:function(data,type,row){

                    if (data!=null)
                    {
                        return data;
                    }
                    else
                    {
                        return'';
                    }
                }

            },
            {
                data: 'id',
                orderable: false,
                render:function(data,type,row){
                    if(row.paymentStatus== "IN_PROCESS"){
                        return '<button type="button" data-id="'+data+'" class="btn revert-payment text-white btn-warning pay">Visszavonás</button>'
                    }
                    else
                    {
                        return '';
                    }
                }
            }

        ]
    });

    formRequestPayment.on("submit",function (event){

        event.preventDefault();

        requestPaymentAlert.removeClass('d-inline-block');


            $.ajax({
                url: "/ref-payment/req",
                method: "POST",
                data: {
                    "qty":  requestBalance.val(),
                    "payType": $("#requestMail  option:selected").val()
                },
                success: function (response) {
                    if (response.type == "success"){
                        swal({
                            title: 'Kifizetés',
                            text: 'Sikeres kifizetés igénylése',
                            icon: "success",
                            closeOnClickOutside:false,
                            dangerMode: true
                        });
                        userRequestPayments.ajax.reload();
                        $('#balanceInfo').text(response.obj+' Ft');

                    } else if(response.type =="error") {
                        swal({
                            title: 'Kifizetés',
                            text: response.msg,
                            icon: "error",
                            closeOnClickOutside:false,
                            dangerMode: true
                        });
                    }
                }

            });



    });

    $(document).on('click',".revert-payment",function () {

        self = $(this);

        swal({
            title: "Biztos visszavonod a kérelmet?",
            text: "",
            icon: "warning",
            closeOnClickOutside:false,
            dangerMode: true,
            buttons:['Mégse','Igen']
        }).then(function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: "/ref-payment/revert",
                    method: "POST",
                    data: {"id": self.data("id")},
                    success: function (response) {
                        if (response.type == "success"){

                            swal({
                                title: 'Visszavonás',
                                text: 'Kifizetési kérelem visszavonva!',
                                icon: "success",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            userRequestPayments.ajax.reload();
                            $('#balanceInfo').text(response.obj+' Ft');

                        }else {
                            swal({
                                title: 'Server hiba',
                                text: 'Sikertelen módosítás!',
                                icon: "error",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                        }
                    }

                });
            }
        });



    });

    function myFunction() {
        var copyText = document.getElementById("myInput");
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");

        var tooltip = document.getElementById("myTooltip");
        tooltip.innerHTML = "Copied: " + copyText.value;
    }

    function outFunc() {
        var tooltip = document.getElementById("myTooltip");
        tooltip.innerHTML = "Copy to clipboard";
    }





   

});
