$(document).ready(function() {

    const pullMatches = $('#pullMatches');
    const loadMatches = $('#loadMatches');
    const matchContainer = $('#match-container');

    const applicationId = 'c8d4cd6d';
    const applicationKey = 'de1ce8ce0872f3356ddc1413c3d11a49';


    $.fn.DataTable.ext.pager.numbers_length = 5;

    var lang = {
        "sEmptyTable":     "Nincs rendelkezésre álló adat",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés ",
        "sZeroRecords":    "Nincs a keresésnek megfelelő találat",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };


    let fasz=$('select').selectize({
        sortField: 'text',
        create:function (input, callback){
            $.ajax({
                url: '/team/add-by-select',
                type: 'POST',
                contentType: 'application/json',
                data:JSON.stringify({
                    "name":input
                }),
                success: function (result) {
                    if (result){
                        fasz[0].selectize.clearOptions();
                        fasz[0].selectize.addOption(result);
                        fasz[1].selectize.clearOptions();
                        fasz[1].selectize.addOption(result);
                        callback({text:input,value:input});
                    }
                }
            });
        },
        render: {
            option_create: function(data, escape) {
                return '<div class="create"><strong>' + escape(data.input) + ' hozzáadása</strong>&hellip;</div>';
            }
        },
    });



    $('#add').submit(function(event) {
        event.preventDefault();
        $.ajax({
            url: "/match/add",
            method: "POST",
            contentType: 'application/json',
            data:  JSON.stringify({
                "hteam": $("#addHTeam  option:selected").text(),
                "vteam": $("#addVTeam  option:selected").text(),
                "oddsH": $("#HOdds").val(),
                "oddsD": $("#DOdds").val(),
                "oddsV": $("#VOdds").val(),
                "match": $("#match").val(),
                "matchReflink": $('#matchReflink').val()
            }),
            success: function (response) {
                if (response.type == "success"){
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                    $('#addModal').modal('hide');
                    document.getElementById("add").reset();
                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                }
            }
        });
        event.preventDefault();
    });

    $(document).on('click','.delete',function () {

        var id = $(this).data('id');
        swal({
            title: "Biztos törlöd a mérközést?",
            text: "",
            icon: "warning",
            closeOnClickOutside:false,
            dangerMode: true,
            buttons:['Mégsem','Törlés']
        }).then(function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    method: 'DELETE',
                    url : '/match/delete/'+id,
                    processData: false,
                    contentType: false,
                    destroy: true,
                    success : function (data) {
                        if(data.type=='success')
                        {
                            swal({
                                title: data.msg,
                                text: '',
                                icon: "success",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                        else if(data.type=='error')
                        {
                            swal({
                                title: data.msg,
                                text: '',
                                icon: "error",
                                closeOnClickOutside:false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                    }
                });
            }
        });
        event.preventDefault();
    });

    function doSearchOnTable(rowId, control){

        if ($(control).val() == "") {
            dtTable
                .column(rowId)
                .search("")
                .draw();
        } else {
            dtTable
                .column(rowId)
                .search($(control).val())
                .draw();
        }
    }

    $("body").on("change","#search_h_team",function () {

        doSearchOnTable(0, this);
    });
    $("body").on("change","#search_v_team",function () {

        doSearchOnTable(1, this);
    });

    $('#edit').submit(function(event) {

        $.ajax({
            url: "/match/edit",
            method: "POST",
            data: {
                "id":  $("#edit_matchId").val(),
                "hteam": $("#editHTeam  option:selected").text(),
                "vteam": $("#editVTeam  option:selected").text(),
                "oddsH": $("#editHOdds").val(),
                "oddsD": $("#editDOdds").val(),
                "oddsV": $("#editVOdds").val(),
                "match": $("#editMatch").val(),
                "reflink":$('#editMatchReflink').val(),
                "matchDate":$('#editMatch').val()
            },
            success: function (response) {
                if (response.type == "success"){
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                    $('#editModal').modal('hide')
                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                }
            }
        });
        event.preventDefault();
    });

    $(document).on('click',".edit",function () {
        $('#editModal').modal('show')
        self = $(this);
        $.ajax({
            url: "/match/get",
            method: "GET",
            data: {"id": self.data("id")},
            success: function (data) {
                $("#edit_matchId").val(data.id);
                $('#editHTeam').data('selectize').setValue(data.hteam);
                $('#editVTeam').data('selectize').setValue(data.vteam);
                $("#editHOdds").val(data.oddsH);
                $("#editDOdds").val(data.oddsD);
                $("#editVOdds").val(data.oddsV);
                $("#editMatch").val(data.match);
                $('#editMatchReflink').val(data.matchReflink);
            }
        })
    });

    $(document).on('click',".editResult",function () {
        $('#editResultModal').modal('show')
        self = $(this);
        $.ajax({
             url: "/match/get",
            method: "GET",
            data: {"id": self.data("id")},
            success: function (data) {
                $("#matchIdResult").val(data.id);
                $('#TeamHResult').data('selectize').setValue(data.hteam);
                $('#TeamVResult').data('selectize').setValue(data.vteam);
                $("#OddsHResult").val(data.oddsH);
                $("#OddsDResult").val(data.oddsD);
                $("#OddsVResult").val(data.oddsV);
                $("#MatchResult").val(data.match);
                $("#editHomeGoalResult").val(data.goalHQty);
                $("#editVisitorGoalResult").val(data.goalVQty);
                $('#editMatchResult').data('selectize').setValue(data.result);

            }
        })
    });

    $('#editResult').submit(function(event) {

        $.ajax({
            url: "/match/editResult",
            method: "POST",
            contentType: 'application/json',
            data: JSON.stringify({
                "id":  $("#matchIdResult").val(),
                "hteam": $("#TeamHResult  option:selected").text(),
                "vteam": $("#TeamVResult  option:selected").text(),
                "oddsH": $("#OddsHResult").val(),
                "oddsD": $("#OddsDResult").val(),
                "oddsV": $("#OddsVResult").val(),
                "match": $("#MatchResult").val(),
                "result": $("#editMatchResult").val(),
                "goalHQty": $("#editHomeGoalResult").val(),
                "goalVQty": $("#editVisitorGoalResult").val(),
                "matchReflink" : $('#editMatchReflink').val()
            }),
            success: function (response) {
                if (response.type == "success"){
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                }
            }
        });
        event.preventDefault();
    });

    let kindredResponse;
    let matchCount;

    var LocalDate = JSJoda.LocalDate;

    var today = LocalDate.now().toString();
    var tomorrow = LocalDate.now().plusDays(1).toString();
    var tomorrowPlus = LocalDate.now().plusDays(2).toString();


    pullMatches.on('click',function (event) {

        pullMatches.addClass('d-none');
        loadMatches.removeClass('d-none');


        matchCount=0;

        $.ajax({
            method: 'GET',
            url: window.location.protocol+'//api.unicdn.net/v1/feeds/sportsbookv2/event/livecalendar.json?app_id=c8d4cd6d&app_key=de1ce8ce0872f3356ddc1413c3d11a49&local=hu_HU&site=hu1.unibet.com&from='+today+'&to='+tomorrowPlus+'&rangeStart=0&rangeSize=100000&ongoing=true&streamed=false&externaleventprovider=EP_BETRADAR'
            ,
            success: function (response) {

                kindredResponse = response.events;
                loadMatches.addClass('d-none');

                kindredResponse.forEach(
                    event =>
                    {
                        let eventRealStart = JSJoda.ZonedDateTime.parse(event.start).plusHours(2).toLocalDate().toString();
                        if (event.sport=="FOOTBALL" && eventRealStart.includes(tomorrow) && event.homeName!=undefined && event.awayName!=undefined && !event.homeName.includes("Esport") && !event.awayName.includes("Esport") )
                        {
                            matchCount++;
                            let realEventTime = JSJoda.ZonedDateTime.parse(event.start).plusHours(2).toString();
                            let eventTime =  realEventTime.replaceAll('-','.').replace('T',' ').replace('Z','');
                            matchContainer.append("<input type='checkbox' class='' id='"+event.id+"' data-id='"+event.id+"' data-home='"+event.homeName+"' data-away='"+event.awayName+"' data-start='"+event.start+"'/><label class='col-12 card bg-white rounded m-0 p-0 mb-3 pt-1 pb-1 whatever' for='"+event.id+"'><span class='h3 ml-2'><span class='h3 ml-2'>"+event.homeName+"</span> <span class='text-danger'>-</span> <span class='h3'>"+event.awayName+"</span><span class='h3 float-right h-50 m-0 mr-3 '>"+eventTime+"</span></span></label>")

                        }
                    }
                )

                if(matchCount>0)
                {
                    $('#match-count').text("Összesen "+matchCount+" darab meccs holnapra.");
                }
                else
                {
                    $('#match-count').text("Nem sikerült meccseket lekérni!");
                }
                $('#matchCount').removeClass('d-none');
                $('#matchReset').removeClass('d-none');


            }

        });
    });
    $('#matchReset').on('click',function (event) {

        $('input[type=checkbox]').each(function()
        {
            this.checked = false;
        });

    });
    $('#matchCount').on('click',function (event){

        $('#matches-header').addClass('div-disabled');

        document.getElementsByTagName('body')[0].style.overflow = 'hidden';
        $('#matches-save-spinner').removeClass('d-none');

        setTimeout(function(event){

                let matches = [];
                let errorLog = "";

                $('input[type=checkbox]').each(function()
                {
                    let home = "";
                    let away = "";

                    if (this.checked==true)
                    {
                        try {
                            let id = $(this).data("id");
                            home = $(this).data("home");
                            away = $(this).data("away");
                            let start = $(this).data("start");
                            let H ="";
                            let D = "";
                            let V = "";
                            let matchOffer;
                            let over2500,under2500,under1500,over1500,over3500,over4500,over5500,over6500,bothTheamHasGoalYes,bothTheamHasGoalNo = "";


                            $.ajax({async:false,type:'GET',url:window.location.protocol+'//api.unicdn.net/v1/feeds/sportsbookv2/betoffer/event/'+id+'.json?app_id=c8d4cd6d&app_key=de1ce8ce0872f3356ddc1413c3d11a49&includeparticipants=false&outComeSortBy=lexical&outComeSortDir=desc',
                                success: function (response) {

                                    matchOffer = response;

                                }});


                            matchOffer.betOffers.forEach(
                                betOffer =>
                                {
                                    if(betOffer.betOfferType.englishName=="Yes/No" && betOffer.criterion.englishLabel == "Both Teams To Score" )
                                    {
                                        betOffer.outcomes.forEach(HDV =>
                                        {
                                            if(HDV.englishLabel=="Yes")
                                            {
                                                bothTheamHasGoalYes=HDV.oddsFractional;
                                            }
                                            else if(HDV.englishLabel=="No")
                                            {
                                                bothTheamHasGoalNo=HDV.oddsFractional;
                                            }
                                        });
                                    }
                                    else if(betOffer.betOfferType.englishName=="Match" && betOffer.criterion.englishLabel == "Full Time")
                                    {
                                        betOffer.outcomes.forEach(HDV =>
                                        {
                                            if(HDV.englishLabel=="1")
                                            {
                                                H=HDV.oddsFractional;
                                            }
                                            else if(HDV.englishLabel=="X")
                                            {
                                                D=HDV.oddsFractional;
                                            }
                                            else if(HDV.englishLabel=="2")
                                            {
                                                V=HDV.oddsFractional;
                                            }
                                        });
                                    }
                                    else if(betOffer.betOfferType.englishName=="Over/Under" && betOffer.criterion.englishLabel == "Total Goals")
                                    {
                                        betOffer.outcomes.forEach(HDV =>
                                        {
                                            if(HDV.englishLabel=="Over")
                                            {
                                                if(HDV.line == 1500)
                                                {
                                                    over1500=HDV.oddsFractional;
                                                }
                                                if(HDV.line == 2500)
                                                {
                                                    over2500=HDV.oddsFractional;
                                                }
                                                if(HDV.line == 3500)
                                                {
                                                    over3500=HDV.oddsFractional;
                                                }
                                                if(HDV.line == 4500)
                                                {
                                                    over4500=HDV.oddsFractional;
                                                }
                                                if(HDV.line == 5500)
                                                {
                                                    over5500=HDV.oddsFractional;
                                                }
                                                if(HDV.line == 6500)
                                                {
                                                    over6500=HDV.oddsFractional;
                                                }


                                            }
                                            else if(HDV.englishLabel=="Under" )
                                            {
                                                if(HDV.line == 1500)
                                                {
                                                    under1500=HDV.oddsFractional;
                                                }
                                                if(HDV.line == 2500)
                                                {
                                                    under2500=HDV.oddsFractional;
                                                }
                                            }

                                        });
                                    }
                                }
                            )

                            let match = {
                                'matchOffer':{
                                    'home':H,
                                    'draw':D,
                                    'away':V,
                                    'under1500':under1500,
                                    'under2500':under2500,
                                    'over1500':over1500,
                                    'over2500':over2500,
                                    'over3500':over3500,
                                    'over4500':over4500,
                                    'over5500':over5500,
                                    'over6500':over6500,
                                    'bothTeamHasGoalYes':bothTheamHasGoalYes,
                                    'bothTeamHasGoalNo':bothTheamHasGoalNo
                                },
                                'id':id,
                                'home':home,
                                'away':away,
                                'start':start,

                            };

                            console.log(match);

                            matches.push(match);
                        }
                        catch(err) {

                            console.log('Meccs kihagyva '+home+' vs '+away);

                            errorLog=errorLog+'\n Mérkőzés nem lett felvéve!  '+home+' vs. '+away;


                        }



                    }

                });

                if(errorLog!='')
                {
                    swal({
                        title: 'Kindred hiba!',
                        text: errorLog,
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                }



                $.ajax({
                    type:'POST',
                    url:'/match/v2/add',
                    contentType: 'application/json',
                    data:JSON.stringify(matches),
                    success: function (response) {

                        swal({
                            title: '',
                            text: 'Összesen '+response.obj+' db mérkőzés felvéve!',
                            icon: "success",
                            closeOnClickOutside:false,
                            dangerMode: true
                        });

                        pullMatches.removeClass('d-none');
                        loadMatches.addClass('d-none');
                        matchContainer.text('');
                        $('#matchCount').addClass('d-none');
                        $('#matchReset').addClass('d-none');
                        $('#match-count').text('');

                        $('#matches-header').removeClass('div-disabled');
                        document.getElementsByTagName('body')[0].style.overflow = 'visible';
                        $('#matches-save-spinner').addClass('d-none');


                    }
                });

            }

        , 500)



    });
});