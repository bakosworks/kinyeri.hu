
function saveTipp(postData,form,formId) {
    let active= form.find(".button")

    $.ajax({
        url: "/tipp/tipp-submit",
        method: "POST",
        contentType: 'application/json',
        data: JSON.stringify(postData),
        success : function (data) {
            if(data.type=='success')
            {
                var item = $("#form-"+formId).find(".mySvg")[0];
                if(item!=undefined)
                {
                    ($("#form-"+formId).find(".mySvg")[0].parentNode).removeChild($("#form-"+formId).find(".mySvg")[0]);
                }


                if (active){
                    active.toggleClass("button");
                    switch (postData["radioName"]) {
                        case "h":
                            $("#"+formId+"-home").closest(".contentv2").addClass("button");
                            $("#"+formId+"-home").closest(".contentv2").append("<svg class='mySvg' viewBox='0 0 500 150' preserveAspectRatio='none'><path fill='none' d='M325,18C228.7-8.3,118.5,8.3,78,21C22.4,38.4,4.6,54.6,5.6,77.6c1.4,32.4,52.2,54,142.6,63.7 c66.2,7.1,212.2,7.5,273.5-8.3c64.4-16.6,104.3-57.6,33.8-98.2C386.7-4.9,179.4-1.4,126.3,20.7' /></svg>");
                            break
                        case "v":
                            $("#"+formId+"-visitor").closest(".contentv2").addClass("button");
                            $("#"+formId+"-visitor").closest(".contentv2").append("<svg class='mySvg' viewBox='0 0 500 150' preserveAspectRatio='none'><path fill='none' d='M325,18C228.7-8.3,118.5,8.3,78,21C22.4,38.4,4.6,54.6,5.6,77.6c1.4,32.4,52.2,54,142.6,63.7 c66.2,7.1,212.2,7.5,273.5-8.3c64.4-16.6,104.3-57.6,33.8-98.2C386.7-4.9,179.4-1.4,126.3,20.7' /></svg>");
                            break
                        case "d":
                            $("#"+formId+"-draw").closest(".contentv2").addClass("button");
                            $("#"+formId+"-draw").closest(".contentv2").append("<svg class='mySvg' viewBox='0 0 500 150' preserveAspectRatio='none'><path fill='none' d='M325,18C228.7-8.3,118.5,8.3,78,21C22.4,38.4,4.6,54.6,5.6,77.6c1.4,32.4,52.2,54,142.6,63.7 c66.2,7.1,212.2,7.5,273.5-8.3c64.4-16.6,104.3-57.6,33.8-98.2C386.7-4.9,179.4-1.4,126.3,20.7' /></svg>");
                            break
                    }
                }
                swal({
                    title: data.msg,
                    text: '',
                    icon: "success",
                    closeOnClickOutside:false,
                    dangerMode: true
                });
            }
            else if(data.type=='error')
            {
                swal({
                    title: data.msg,
                    text: '',
                    icon: "error",
                    closeOnClickOutside:false,
                    dangerMode: true
                });
            }
        }
    });
}
function objectifyForm(formArray) {
    let returnArray = {};
    for (let i = 0; i < formArray.length; i++){
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}

$(document).ready(function (){
   $(".kinyeri-tipp-form").on("submit", function (){

       const form=$(this).closest("form");
       const formId=form.attr("id");
       let data=form.serializeArray();


       let matchId=formId.slice(5,formId.length);

       console.log(data);



       let homeQty = 0;
       let visitorQty = 0;
       let HVD = "notFound";

       data.forEach(function (obj) {

           if(obj.name=="homeQty")
           {
              homeQty =  obj.value == "" ? 0 : obj.value;
           }
           if(obj.name=="visitorQty")
           {
               visitorQty =  obj.value == "" ? 0 : obj.value;
           }
           if(obj.name=="radioName")
           {
                HVD = obj.value;
           }
       });



       if(HVD=="notFound")
       {
           let result = homeQty == visitorQty ? 'd' : homeQty > visitorQty ? 'h' : 'v';

           data.push({
               "name":"radioName",
               "value":result+""
           });
       }
       else
       {
           data.forEach(function (obj) {

               if(obj.name=="radioName")
               {
                   obj.value = homeQty == visitorQty ? 'd' : homeQty > visitorQty ? 'h' : 'v';
               }

           });

       }

       data.push({
           "name":"matchId",
           "value":matchId+""
       });

       if(data.length != 4){

           swal({
               title: 'Hibás mentés! Töltse ki az összes mezőt!',
               text: '',
               icon: "error",
               closeOnClickOutside:false,
               dangerMode: true
           });

       }
       else
       {

               saveTipp(objectifyForm(data),form,matchId);

       }
   });
});