$(document).ready(function() {

    $.fn.DataTable.ext.pager.numbers_length = 5;

    var lang = {
        "sEmptyTable":     "Nincs rendelkezésre álló adat",
        "sInfo":           "Találatok: _START_ - _END_ Összesen: _TOTAL_",
        "sInfoEmpty":      "Nulla találat",
        "sInfoFiltered":   "(_MAX_ összes rekord közül szűrve)",
        "sInfoPostFix":    "",
        "sInfoThousands":  " ",
        "sLengthMenu":     "_MENU_ oldalanként",
        "sLoadingRecords": "Betöltés...",
        "sProcessing":     "Feldolgozás...",
        "sSearch":         "Keresés ",
        "sZeroRecords":    "Nincs a keresésnek megfelelő találat",
        "oPaginate": {
            "sFirst":    "Első",
            "sPrevious": "Előző",
            "sNext":     "Következő",
            "sLast":     "Utolsó"
        },
        "oAria": {
            "sSortAscending":  ": aktiválja a növekvő rendezéshez",
            "sSortDescending": ": aktiválja a csökkenő rendezéshez"
        }
    };

    var userRequestPayments = $('table#userRequestPayments').DataTable({
        'ajax': {
            'contentType': 'application/json',
            'url': '/ref-payment/list',
            'type': 'POST',
            'data': function (d) {
                return JSON.stringify(d);
            }
        },
        "pageLength": 10,
        'serverSide' : true,
        "processing": true,
        "scrollX": true,
        "order": [[ 0, "desc" ]],
        "language": lang,
        "stateSave": false,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Összes"]],
        "dom": '<"wrapper"ltip>',
        columns: [
            {
                data:'id'
            },
            {
                data:'reqUser.username'
            },
            {
                data:'email',
                render:function(data,type,row){

                        return data+' '+row.payType;
                }
            },
            {
                data:'refBalance',
                render:function(data,type,row){

                        return  data+' Ft';

                }
            },
            {
                data: 'reqTime'
            },
            {
                data: 'finishTime',
                render:function(data,type,row){

                    if (data!=null)
                    {
                        return data;
                    }
                    else
                    {
                        return'';
                    }
                    }

            },
            {
                data: 'userPayed',
                render:function(data,type,row){
                    if(data!=null)
                    {

                        return data.username;

                    }
                    else
                    {
                        return '';
                    }
                }

            },
            {
                data: 'paymentStatus',
                render:function(data,type,row){

                    if (data=="IN_PROCESS")
                    {
                        return 'Folyamatban';
                    }
                    else if(data=="DENIED" && row.userReverted!= null &&  row.userReverted.username == row.reqUser.username)
                    {
                        return 'Visszavonva';
                    }
                    else if (data=="DENIED")
                    {
                        return 'Elutasítva'+row.userReverted.username;
                    }
                    else if (data=="DONE")
                    {
                        return 'Kifizetve';
                    }
                    else
                    {
                        return  '';
                    }
                }

            },
            {
                data: 'id',
                orderable: false,
                render:function(data,type,row){
                    if(row.paymentStatus== "IN_PROCESS"){
                        return '<button type="button" data-id="'+data+'" class="btn btn-warning ref-pay">Kifizet</button>'+
                        '<button type="button" data-id="'+data+'" class="btn btn-info revert ml-2">Elutasít</button>';
                    }else{
                        return '';
                    }
                }
            }

        ]
    });

    var dtTable = $('table#usersPayments').DataTable({
        'ajax': {
            'contentType': 'application/json',
            'url': '/payment/list',
            'type': 'POST',
            'data': function (d) {
                return JSON.stringify(d);
            }
        },
        "pageLength": 10,
        'serverSide' : true,
        "processing": true,
        "scrollX": true,
        "order": [[ 5, "desc" ]],
        "language": lang,
        "stateSave": false,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Összes"]],
        "dom": '<"wrapper"ltip>',
        columns: [
            {
                data: 'userId.username',
                render:function(data,type,row){
                    return '<span uk-icon="menu" class="ml-2 btn details-control" uk-tooltip="title: További adatok"></span>'+row.userId.username
                }
            },
            {
                data:'packageId.name',
                render:function(data,type,row){

                    if(row.discount!=null)
                    {
                        return '<span>'+row.packageId.name+' '+ (row.packageId.price*0.7)+' Ft [Kedvezményes]<span>';
                    }
                    else
                    {
                        return '<span>'+row.packageId.name+' '+ row.packageId.price+' Ft <span>';

                    }


                }
            },
            {
                data:'createdTime'
            },
            {
                data: 'activateTime',
                render:function(data,type,row){
                    if(data==null)
                    {
                        return ' ';
                    }
                    else
                    {
                        return data;
                    }
                }
            },
            {
                data: 'status',
                render:function(data,type,row){
                    if(data== "IN_PROCESS"){
                        return '<button type="button" class="btn btn-danger " >Fizetésre vár</button>'
                    }else if(data== "DONE"){
                        return '<button type="button" class="btn btn-success " >Fizetve</button>'
                    }else{
                        return '';
                    }
                }

            },
            {
                data: 'id',
                render:function(data,type,row){
                    if(row.status== "IN_PROCESS"){
                        return '<button type="button" data-id='+data+' class="btn btn-warning pay">Aktíválás</button>'
                    }else if(row.status== "DONE"){
                        return '<button type="button" data-id='+data+' class="btn btn-success">Aktív</button>'+
                            '<button type="button" data-id='+data+' class="btn btn-info revert ml-2">Visszavonás</button>'
                    }else{
                        return '';
                    }



                }
            }

        ]
    });

    $(document).on('click', ".details-control", function() {
        var tr = $(this).closest('tr');
        var row = dtTable.row( tr );

        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }

    });

    function format ( d ) {
        return '<table id="dataTable" class="table table-bordered" style="width: 100%">'+
            '<tr class="thead-dark">' +
            '<th>Teljes név</th>'+
            '<th>Email</th>'+
            '<th>Ország</th>'+
            '<th>Irányítószám</th>'+
            '<th>Város</th>'+
            '<th>Cím</th>'+
            '</tr>'+
            '<tr class="thead-dark">' +
            '<td>'+d.userId.firstName+' '+d.userId.lastName+'</td>'+
            '<td>'+d.userId.email+'</td>'+
            '<td>'+d.userId.country+'</td>'+
            '<td>'+d.userId.zipCode+'</td>'+
            '<td>'+d.userId.city+'</td>'+
            '<td>'+d.userId.address+'</td>'+
            '</tr>'+
            '</table>';
    };

    $('#add').submit(function(event) {

        $.ajax({
            url: "/payment/addGift",
            method: "POST",
            contentType: 'application/json',
            data:  JSON.stringify({
                "userId": $("#addUsername").val(),
                "packageId": $("#addPackage").val(),
            }),
            success: function (response) {
                if (response.type == "success"){
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "success",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                    $('#addModal').modal('hide')
                    document.getElementById("add").reset();
                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                    dtTable.ajax.reload();
                }
            }
        });
        event.preventDefault();
    });


    $(document).on('click',".pay",function () {

        $('#payModal').modal('show');

        self = $(this);
        swal({
            title: "Biztos Aktiválod?",
            text: "",
            icon: "warning",
            closeOnClickOutside:false,
            dangerMode: true,
            buttons:['Mégsem','Aktíválás']
        }).then(function(isConfirm) {
            if(isConfirm){
            $.ajax({
                url: "/payment/paid",
                method: "POST",
                data: {"id": self.data("id")},
                success: function (response) {
                    if (response.type == "success") {
                        swal({
                            title: "Sikeresen mentve!",
                            text: '',
                            icon: "success",
                            closeOnClickOutside: false,
                            dangerMode: true
                        });
                        dtTable.ajax.reload();
                        $('#editResultModal').modal('hide')
                    } else if (response.type == "error") {
                        swal({
                            title: response.msg,
                            text: '',
                            icon: "error",
                            closeOnClickOutside: false,
                            dangerMode: true
                        });
                        dtTable.ajax.reload();
                    }
                }
            })
        }
    });
    });

    $(document).on('click',".revert",function () {


        self = $(this);
        swal({
            title: "Biztos elutasítod?",
            text: "",
            icon: "warning",
            closeOnClickOutside:false,
            dangerMode: true,
            buttons:['Mégsem','Elutasít']
        }).then(function(isConfirm) {
            if(isConfirm){
            $.ajax({
                url: "/ref-payment/revert",
                method: "POST",
                data: {"id": self.data("id")},
                success: function (response) {
                    if (response.type == "success") {
                        swal({
                            title: response.msg,
                            text: '',
                            icon: "success",
                            closeOnClickOutside: false,
                            dangerMode: true
                        });
                        userRequestPayments.ajax.reload();
                    } else if (response.type == "error") {
                        swal({
                            title: response.msg,
                            text: '',
                            icon: "error",
                            closeOnClickOutside: false,
                            dangerMode: true
                        });
                        userRequestPayments.ajax.reload();
                    }
                }
            })
        }
    });
    });


    $(document).on('click',".ref-pay",function () {


        self = $(this);
        swal({
            title: "Biztos készre állítod?",
            text: "",
            icon: "warning",
            closeOnClickOutside:false,
            dangerMode: true,
            buttons:['Mégsem','Kifizetve']
        }).then(function(isConfirm) {
            if(isConfirm){
                $.ajax({
                    url: "/ref-payment/done",
                    method: "POST",
                    data: {"id": self.data("id")},
                    success: function (response) {
                        if (response.type == "success") {
                            swal({
                                title: response.msg,
                                text: '',
                                icon: "success",
                                closeOnClickOutside: false,
                                dangerMode: true
                            });
                            userRequestPayments.ajax.reload();
                        } else if (response.type == "error") {
                            swal({
                                title: response.msg,
                                text: '',
                                icon: "error",
                                closeOnClickOutside: false,
                                dangerMode: true
                            });
                            userRequestPayments.ajax.reload();
                        }
                    }
                })
            }
        });
    });


    $(document).on('click',".undo",function () {
        $('#payModal').modal('show')
        self = $(this);
        swal({
            title: "Biztos Visszavonod?",
            text: "",
            icon: "warning",
            closeOnClickOutside:false,
            dangerMode: true,
            buttons:['Mégsem','Visszavonás']
        }).then(function(isConfirm) {
            if(isConfirm) {
                $.ajax({
                    url: "/payment/undo",
                    method: "POST",
                    data: {"id": self.data("id")},
                    success: function (response) {
                        if (response.type == "success") {
                            swal({
                                title: response.msg,
                                text: '',
                                icon: "success",
                                closeOnClickOutside: false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                            $('#editResultModal').modal('hide')
                        } else if (response.type == "error") {
                            swal({
                                title: response.msg,
                                text: '',
                                icon: "error",
                                closeOnClickOutside: false,
                                dangerMode: true
                            });
                            dtTable.ajax.reload();
                        }
                    }
                })
            }
        });
    });




});