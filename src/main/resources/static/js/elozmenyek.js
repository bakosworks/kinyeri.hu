
const nextButton = $('#nextButton');
const mailForm = $('#mail');
const registrationDiv = $('#registrationDiv');
const regForm = $('#reflinkRegistration');

const spinner =  $('#spinner');
const parentDiv = $('#parentDiv');
const noMore = $('#noMore');
const loadMore = $('#loadMore');


$(document).ready(function () {
    
    mailForm.on('submit',function (event) {

        event.preventDefault();

        $.ajax({
            url: "/user/check-mail",
            method: "POST",
            data: {"mail": $('#email').val(),
            "id":$('#reflinkOwner').text()},
            success: function (response) {
                if (response.type == "success"){

                    nextButton.removeClass('d-flex');
                    nextButton.addClass('d-none');

                    registrationDiv.removeClass('d-none');
                    registrationDiv.addClass('d-flex');

                    $('#email').attr('readonly', true);

                } else if(response.type =="error") {
                    swal({
                        title: response.msg,
                        text: '',
                        icon: "error",
                        closeOnClickOutside:false,
                        dangerMode: true
                    });
                }
            }

        });

    });

    regForm.on('submit',function (event) {

        event.preventDefault();

        let form = document.forms['reflinkRegistration'];
        let formData = new FormData(form);

        formData.append("email",$('#email').val());




    });

    let pageNumber = 0;
    let endOfPage = false;

    $(document).on('click','#loadMore', function() {

            if(!endOfPage)
            {
                spinner.removeClass("d-none");

                $.ajax({
                    url: "/match/findAllByResultIsNotNull",
                    data:{"pageNumber":pageNumber},
                    method: "GET",
                    success: function (response) {

                        pageNumber=response.pageNumber;
                        console.log(response);

                        if(response.list.length != 0)
                        {
                            appendToContainer(response.list);
                            console.log(response);
                        }
                        else
                        {
                            spinner.addClass("d-none");
                            loadMore.addClass("d-none");
                            noMore.removeClass("d-none");
                            endOfPage = true;

                        }
                    }

                });
            }

    });

    function appendIfStringNot3Long(item)
    {
        const oddsText = item.toString();

        return oddsText.length == 3 ? oddsText+'0' : oddsText;
    }

    function appendToContainer(items) {


        for(item of items)
        {

            let ggNo = item.match.unibetOdds == null ? '' : item.match.unibetOdds.bothTeamNo  == null ?  ''  : appendIfStringNot3Long(item.match.unibetOdds.bothTeamNo);
            let ggYes = item.match.unibetOdds == null ? '' : item.match.unibetOdds.bothTeamYes  == null ?  ''  : appendIfStringNot3Long(item.match.unibetOdds.bothTeamYes);

            let gOver2500 = item.match.unibetOdds == null ? '' : item.match.unibetOdds.over2500 == null ? '' : appendIfStringNot3Long(item.match.unibetOdds.over2500);
            let gOver3500 = item.match.unibetOdds == null ? '' : item.match.unibetOdds.over3500 == null ? '' : appendIfStringNot3Long(item.match.unibetOdds.over3500);
            let gOver4500 = item.match.unibetOdds == null ? '' : item.match.unibetOdds.over4500 == null ? '' : appendIfStringNot3Long(item.match.unibetOdds.over4500);
            let gOver5500 = item.match.unibetOdds == null ? '' : item.match.unibetOdds.over5500 == null ? '' : appendIfStringNot3Long(item.match.unibetOdds.over5500);
            let gOver6500 = item.match.unibetOdds == null ? '' : item.match.unibetOdds.over6500 == null ? '' : appendIfStringNot3Long(item.match.unibetOdds.over6500);
            let gUnder1500 = item.match.unibetOdds == null ? '' : item.match.unibetOdds.under1500 == null ? '' : appendIfStringNot3Long(item.match.unibetOdds.under1500);
            let gUnder2500 = item.match.unibetOdds == null ? '' : item.match.unibetOdds.under2500 == null ? '' : appendIfStringNot3Long(item.match.unibetOdds.under2500);



            let xxxVHD = item.match.goalTippVisitor > item.match.goalTippHome  ? 'V' : (item.match.goalTippVisitor < item.match.goalTippHome  ? 'H' : 'D');
            let VHD = item.match.goalTippVisitor > item.match.goalTippHome  ? 'V <span class="odds-button text-white">'+appendIfStringNot3Long(item.match.oddsV)+'</span>' : (item.match.goalTippVisitor < item.match.goalTippHome  ? 'H <span class="odds-button">'+appendIfStringNot3Long(item.match.oddsH)+'</span>' : 'D <span class="odds-button">'+appendIfStringNot3Long(item.match.oddsD)+'</span>');


            let VHDresult = item.match.result == xxxVHD ? "<div class='d-inline-block'>"+VHD+" <span><i class=\"fas fa-check kinyeri-green\"></i></span></div>" : "<div class='d-inline-block'>"+VHD+" <span><i class=\"fas fa-times kinyeri-red\"></i></span></div>";


       let goalOver = item.match.goalOver;
       let goalText = "";

        if(goalOver == 0)
        {
            goalText = '1.5 Alatt <span class="odds-button text-white">'+gUnder1500+'</span>';
        }
        else   if(goalOver == 1)
        {
            goalText = '2.5 Alatt <span class="odds-button text-white">'+gUnder2500+'</span>';
        }
        else   if(goalOver == 2)
        {
            goalText = '2.5 Alatt <span class="odds-button text-white">'+gUnder2500+'</span>';
        }
        else   if(goalOver == 3)
        {
            goalText = '2.5 Felett <span class="odds-button text-white">'+gOver2500+'</span>';
        }
        else   if(goalOver == 4)
        {
            goalText = '3.5 Felett <span class="odds-button text-white">'+gOver3500+'</span>';
        }
        else   if(goalOver == 5)
        {
            goalText = '3.5 Felett <span class="odds-button text-white">'+gOver3500+'</span>';
        }
        else   if(goalOver ==6)
        {
            goalText = '4.5 Felett <span class="odds-button text-white">'+gOver4500+'</span>';
        }
        else   if(goalOver ==7)
        {
            goalText = '5.5 Felett <span class="odds-button text-white">'+gOver5500+'</span>';
        }
        else   if(goalOver >=8)
        {
            goalText = '6.5 Felett <span class="odds-button text-white">'+gOver6500+'</span>';
        }


           let goalOverWon = item.matchHVDDto.goalOverWon == true ? "<span><i class=\"fas fa-check kinyeri-green\"></i></span>" : "<span><i class=\"fas fa-times kinyeri-red\"></i></span>"
           let GG = item.matchHVDDto.goalTippHome>0 && item.matchHVDDto.goalTippVisitor>0 ? 'Igen' : 'Nem ';


           let realGG =  item.matchHVDDto.goalTippVisitor>0 && item.matchHVDDto.goalTippHome >0;
           let realGGResult = item.matchHVDDto.bothTeamGoalWon;
           let realGGText = "";

           if (realGG==realGGResult)
           {
               realGGText = GG+'<i class=\"fas fa-check kinyeri-green\"></i>';
           }
           else
           {
               realGGText = GG+'<i class=\"fas fa-times kinyeri-red\"></i>';
           }

           let matchResult =   item.match.goalTippHome == item.match.homeGoalQty && item.match.goalTippVisitor == item.match.visitorGoalQty ? "<i class=\"fas fa-check kinyeri-green\"></i>"  : "<i class=\"fas fa-times kinyeri-red\"></i></span>";
           let matchTime =  item.match.matchTime.split(' ')[0].replaceAll('-','.');


       let ggText = "";
       let ggResultIcon = "";

            if(item.match.goalTippVisitor>0 && item.match.goalTippHome>0)
            {
                ggText = '<span>Igen</span><span><span class="odds-button text-white">'+ggYes+'</span>';
            }
            else
            {
                ggText = '<span>Nem</span><span><span class="odds-button text-white">'+ggNo+'</span>';
            }

            if(item.matchHVDDto.bothTeamGoalWon)
            {
                ggResultIcon =  ' <i class="fas fa-check kinyeri-green"></i></span>';
            }
            else
            {
                ggResultIcon =  ' <i class="fas fa-times kinyeri-red"></i></span>';
            }



           html= " <div class=\"justify-content-center d-flex\">" +
            "            <div class=\"col-md-7  mb-2 mt-2\">\n" +
            "\n" +
               "                <div class=\"row p-0 m-0 d-md-none\">\n" +
               "                    <div class=\"col-md-7\">\n" +
               "                        <div class=\"row p-0 m-0\">\n" +
               "                            <span class=\"col-12 m-0 p-0\">"+matchTime+" "+item.match.homeGoalQty+" - "+item.match.visitorGoalQty+"</span>\n" +
               "                        </div>\n" +
               "                    </div>\n" +
               "                </div>"+
            "                <div class=\"row p-0 m-0\">\n" +
            "                    <div class=\"col-md-7\">\n" +
            "                        <div class=\"row p-0 m-0\">\n" +
            "                            <span class=\"col-12 m-0 p-0\">"+item.match.homeTeam.name+"</span>\n" +
            "                            <span class=\"col-12 m-0 p-0\">"+item.match.visitorTeam.name+"</span>\n" +
            "                        </div>\n" +
            "                    </div>\n" +
            "                    <div class=\"col-md-5 d-none d-md-inline-block text-right \">\n" +
            "                        <div class=\"row m-0 p-0\">\n" +
            "                            <span class=\"col-12  m-0 p-0\">"+matchTime+"</span>\n" +
            "                            <span class=\"col-12  m-0 p-0\">"+item.match.homeGoalQty+" - "+item.match.visitorGoalQty+"</span>\n" +
            "                        </div>\n" +
            "                    </div>\n" +
            "                </div>\n" +
            "                <div class=\"row p-0 m-0\">" +
            "                     <span class=\"col-12 mb-1\">Nyertes csapat: " +VHDresult + "</span>" +
            "                    <span class=\"col-12 mb-1\">Gólszám:" +
            "                                                                <span>"+goalText+"</span>\n" +
            "\n" +goalOverWon+

            "                    </span>\n" +
            "                    <span class=\"col-12 mb-1\">G/G:\n" +
            "\n" +ggText+ggResultIcon+
            "                    </span>\n" +
            "                </div>\n" +
            "\n" +
            "                    </span>\n" +
            "                    <span class=\"col-12 mb-1\"> Pontos kimenetel:\n" +
            "                        <span>"+item.matchHVDDto.goalTippHome+" - "+item.matchHVDDto.goalTippVisitor+"</span> " + matchResult+
            "\n" +
            "\n" +
            "                    </span>\n" +
            "                <hr/>\n" +
            "\n" +
            "            </div>\n" +
            "\n" +
            "        </div>";
            parentDiv.append(html);

        }
        spinner.addClass("d-none");
        
    }



})
