DROP TABLE IF EXISTS tmp_user;
DROP TABLE IF EXISTS user_reflink;
ALTER TABLE users ADD COLUMN ref_link varchar(50);
ALTER TABLE users ADD COLUMN ref_user bigint;
ALTER TABLE users ADD
    CONSTRAINT `fk_ref_user`
    FOREIGN KEY (`ref_user`) REFERENCES `users` (`id`);