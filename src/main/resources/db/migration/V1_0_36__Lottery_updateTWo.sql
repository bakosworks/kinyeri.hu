CREATE TABLE lottery (
                                 `id` BIGINT NOT NULL,
                                 `who` BIGINT NULL,
                                 `created` DATETIME NULL,
                                 PRIMARY KEY (`id`));

CREATE TABLE lottery_user (
                                      `id` BIGINT NOT NULL,
                                      `lottery` BIGINT NULL,
                                      `user` BIGINT NULL,
                                      `position` INT NULL,
                                      PRIMARY KEY (`id`));

