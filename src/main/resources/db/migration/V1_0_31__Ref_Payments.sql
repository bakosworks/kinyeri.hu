ALTER TABLE users ADD COLUMN balance int;

CREATE TABLE IF NOT EXISTS ref_payments(
    id BIGINT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    req_user bigint not null,
    user_payed bigint,
    req_time timestamp not null,
    finish_time timestamp,
        CONSTRAINT fk_req_user
        FOREIGN KEY (req_user) REFERENCES users (id),
        CONSTRAINT fk_user_payed
        FOREIGN KEY (req_user) REFERENCES users (id)
)