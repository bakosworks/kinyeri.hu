CREATE table reflink(
    reflink_id BIGINT NOT NULL AUTO_INCREMENT,
    text Varchar(255) NOT NULL,
    link VARCHAR(255) NOT NULL,
    PRIMARY KEY (reflink_id)
);
