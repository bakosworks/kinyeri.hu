ALTER TABLE `users` ADD COLUMN country varchar(255);
ALTER TABLE `users` ADD COLUMN zip_code varchar(255);
ALTER TABLE `users` ADD COLUMN city varchar(255);
ALTER TABLE `users` ADD COLUMN address varchar(255);
ALTER TABLE `users` ADD COLUMN membership varchar(255);

CREATE TABLE packages(
    id BIGINT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name varchar(255) NOT NULL,
    interval_in_days integer NOT NULL,
    color varchar(255) NOT NULL,
    price double NOT NULL,
    details varchar(255) NOT NULL,
    active boolean NOT NULL,
    active_till timestamp
);

CREATE TABLE payments(
    id BIGINT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    created_time timestamp NOT NULL,
    activate_time timestamp,
    status varchar(255) NOT NULL,
    package_id BIGINT(11) NOT NULL,
    active_till timestamp,
    user_id BIGINT(11) NOT NULL,
    CONSTRAINT `payment_to_user_fk`
        FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
    CONSTRAINT `payment_to_package_fk`
        FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`)
);

ALTER TABLE users
    ADD COLUMN win_wining_team_qty int(12),
    ADD COLUMN win_final_result_qty int(12),
    ADD COLUMN win_number_of_gols_qty int(12),
    ADD COLUMN win_which_team_goes_qty int(12),
    ADD COLUMN boosted_win_rate int(12),
    ADD COLUMN win_rate int(12);

ALTER TABLE tipps
    ADD COLUMN win_wining_team BOOLEAN,
    ADD COLUMN win_final_result BOOLEAN,
    ADD COLUMN win_number_of_gols BOOLEAN,
    ADD COLUMN win_which_team_goes BOOLEAN;

