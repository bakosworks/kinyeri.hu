




ALTER TABLE reflink
    ADD COLUMN `match_reflink` VARCHAR(255) NULL DEFAULT 'https://b1.trickyrock.com/redirect.aspx?pid=71653391&bid=36561&redirectURL=https://hu1.unibet.com/betting/sports/filter/football/matches' AFTER `link`;

ALTER TABLE matches
    ADD COLUMN `match_reflink` VARCHAR(255) NULL AFTER `insert_time`;
