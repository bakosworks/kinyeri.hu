ALTER TABLE users
    ADD COLUMN `skrill_address` VARCHAR(255) NULL DEFAULT '' AFTER `balance`,
    ADD COLUMN `paypal_address` VARCHAR(255) NULL DEFAULT '' AFTER `skrill_address`;
