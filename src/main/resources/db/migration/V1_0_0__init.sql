
CREATE TABLE  `users` (
                          `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
                          username varchar(255) NOT NULL,
                          first_name varchar(255) NOT NULL,
                          last_name varchar(255) NOT NULL,
                          password varchar(255) NOT NULL,
                          email varchar(255) NOT NULL,
                          default_password boolean NOT NULL,
                          active boolean NOT NULL,
                          PRIMARY KEY (`id`)
);

CREATE TABLE  `roles` (
                          `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
                          role varchar(255) NOT NULL,
                          PRIMARY KEY (`id`)
);

Create TABLE user_role(
                          `user_id` BIGINT(11) NOT NULL,
                          `role_id`  BIGINT(11) NOT NULL,
                          PRIMARY KEY (`user_id`, `role_id`),
                          CONSTRAINT `user_to_user_fk`
                              FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
                          CONSTRAINT `role_to_role_fk`
                              FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
);



REPLACE INTO `roles` VALUES (1,'ADMIN');
REPLACE INTO `roles` VALUES (2,'USER');

INSERT INTO `users` (`id`, `username`, `first_name`, `last_name`,`default_password`,`password`, `email`, `active`) VALUES ('3', 'admin', 'a', 'a','1', '$2a$10$sYkRkWeZ3nut/YiNfcGgaup2TveNJfFf2NyOwOGmZKlQ39reE/QfW', 'a@gmail.com', '1');
INSERT INTO `user_role` (`user_id`, `role_id`) VALUES ('3', '1');
INSERT INTO `user_role` (`user_id`, `role_id`) VALUES ('3', '2');