CREATE TABLE unibet_odds (
                                           `line_id` BIGINT NOT NULL AUTO_INCREMENT,
                                           `over_2500` FLOAT NULL,
                                           `over_3500` FLOAT NULL,
                                           `over_4500` FLOAT NULL,
                                           `over_5500` FLOAT NULL,
                                           `over_6500` FLOAT NULL,
                                           `over_1500` FLOAT NULL,
                                           `under_1500` FLOAT NULL,
                                           `under_2500` FLOAT NULL,
                                           `both_team_yes` FLOAT NULL,
                                           `both_team_no` FLOAT NULL,
                                           PRIMARY KEY (`line_id`));

ALTER TABLE matches ADD COLUMN `unibet_odds` BIGINT NULL AFTER `draw`;
