ALTER TABLE ref_payments
    ADD COLUMN email varchar(255),
    ADD COLUMN pay_type varchar(255),
    ADD COLUMN payment_status varchar(255),
    ADD COLUMN user_reverted bigint;
ALTER TABLE ref_payments ADD CONSTRAINT fk_user_reverted
        FOREIGN KEY (user_reverted) REFERENCES users (id);