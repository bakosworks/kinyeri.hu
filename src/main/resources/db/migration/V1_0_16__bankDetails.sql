
CREATE TABLE bank_details(
`id` BIGINT(11) NOT NULL AUTO_INCREMENT,
      name varchar (255),
      iban varchar (255),
      bank_number varchar (255),
      swift_code varchar (255),
      bank_name varchar (255),
      phone varchar(255),
       PRIMARY KEY (`id`)
);