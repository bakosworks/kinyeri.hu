DROP VIEW IF EXISTS all_time_top_statistics;
create view all_time_top_statistics as
SELECT users.id
FROM users
         LEFT JOIN tipps t ON users.id = t.user
         LEFT JOIN matches m on t.match_id = m.id
where (m.match_time >= DATE(NOW()) - INTERVAL 7 DAY ) AND users.tip_num>49 AND match_time > curdate() - interval (dayofmonth(curdate()) - 1) day - interval 4 month
        group by users.id LIMIT 10