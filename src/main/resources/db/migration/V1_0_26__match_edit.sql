
ALTER TABLE matches
    ADD COLUMN goal_over DOUBLE PRECISION,
    ADD COLUMN goal_tipp_visitor bigint,
    ADD COLUMN goal_tipp_home bigint,
    ADD COLUMN max_win_rate bigint,
    ADD COLUMN home bigint,
    ADD COLUMN visitor bigint,
    ADD COLUMN draw bigint;
