alter table users add column banned boolean;
alter table comments add column delete_time timestamp;
alter table comments add column deleted_by bigint;
alter table comments add constraint fk_comments_deleted_by FOREIGN KEY (deleted_by) REFERENCES users(id);
alter table messages add column delete_time timestamp;
alter table messages add column deleted_by bigint;
alter table messages add constraint fk_messages_deleted_by FOREIGN KEY (deleted_by) REFERENCES users(id);
INSERT INTO `roles` VALUES (4,'MOD');