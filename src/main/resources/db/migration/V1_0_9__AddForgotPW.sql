CREATE TABLE `table_token` (
  `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) DEFAULT NULL,
   `user_id` BIGINT(11) NOT NULL,
   `exp` date NOT NULL,
   `status` INT(11) NULL,
  PRIMARY KEY (`id`)
);