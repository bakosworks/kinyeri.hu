create table notification
(
    id        BIGINT AUTO_INCREMENT,
    user      BIGINT       NOT NULL,
    send_time DATETIME     NOT NULL,
    title     VARCHAR(255) NOT NULL,
    message   VARCHAR(255) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user) REFERENCES users (id)
);