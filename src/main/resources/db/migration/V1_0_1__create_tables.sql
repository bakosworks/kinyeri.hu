CREATE TABLE teams(
    id BIGINT(11)  AUTO_INCREMENT PRIMARY KEY,
    name varchar (255)
);


CREATE TABLE matches(
    id BIGINT(11)  AUTO_INCREMENT PRIMARY KEY,
    v_team BIGINT(11),
    h_team BIGINT(11),
    odds_h float,
    odds_v float,
    odds_d float,
    h_goal_qty int(6),
    v_goal_qty int(6),
    result varchar (255),
    `match` DATETIME,
    insert_time  DATETIME,
    CONSTRAINT `FK_v_team` FOREIGN KEY (`v_team`) REFERENCES `teams`(`id`),
    CONSTRAINT `FK_h_team` FOREIGN KEY (`h_team`) REFERENCES `teams`(`id`)
);

CREATE TABLE tipps(
    user BIGINT(11),
    `match` BIGINT(11),
    result_tip varchar (255),
    h_goal_qty_tipp int(6),
    v_goal_qty_tipp int(6),
    goal_qty int(6),
    final_points int(6),
    PRIMARY KEY(`user`,`match`),
    CONSTRAINT `FK_tipps_match` FOREIGN KEY (`match`) REFERENCES `matches`(`id`),
    CONSTRAINT `FK_tipps_user` FOREIGN KEY (`user`) REFERENCES `users`(`id`)
);

CREATE TABLE comments(
    id BIGINT(11)  AUTO_INCREMENT PRIMARY KEY,
    `match` BIGINT(11),
    user BIGINT(11),
    text varchar (255),
    insert_time DATETIME,
    CONSTRAINT `FK_comments_match` FOREIGN KEY (`match`) REFERENCES `matches`(`id`),
    CONSTRAINT `FK_comments_user` FOREIGN KEY (`user`) REFERENCES `users`(`id`)
);

CREATE TABLE messages(
    id BIGINT(11)  AUTO_INCREMENT PRIMARY KEY,
    user BIGINT(11),
    text varchar (255),
    insert_time DATETIME,
    CONSTRAINT `FK_global_chat` FOREIGN KEY (`user`) REFERENCES `users`(`id`)
);
